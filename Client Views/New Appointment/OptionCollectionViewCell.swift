//
//  OptionCollectionViewCell.swift
//  MyFigaro
//
//  Created by Albert Kwaśkiewicz on 2/24/20.
//  Copyright © 2020 fernando rosa. All rights reserved.
//

import UIKit

class OptionCollectionViewCell: UICollectionViewCell {
    
    let optionWidth: CGFloat = 110
    let optionHeight: CGFloat = 172
    
//    var type: OptionType
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupOptionView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let optionView = OptionView()
    
    func setupOptionView() {
        addSubview(optionView)
        
        optionView.frame = CGRect(x: 20, y: 0, width: optionWidth, height: optionHeight)
    }
    
    enum OptionType {
        case service, barber
    }
}

