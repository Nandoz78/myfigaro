//
//  OptionView.swift
//  MyFigaro
//
//  Created by Albert Kwaśkiewicz on 2/24/20.
//  Copyright © 2020 fernando rosa. All rights reserved.
//

import UIKit


class OptionView: UIView {
    
    let optionWidth: CGFloat = 110
    let optionHeight: CGFloat = 172
    var isSelected:Bool = false
    
    let optionImage: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "Beard")
        iv.contentMode = .scaleAspectFill
        iv.backgroundColor = #colorLiteral(red: 0.968627451, green: 0.968627451, blue: 0.968627451, alpha: 1)
        return iv
    }()
    
    let optionName: UILabel = {
        let label = UILabel()
        label.text = "Option"
        label.font = UIFont.systemFont(ofSize: 20, weight: .regular)
        label.textColor = .black
        label.textAlignment = .center
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.5
        return label
    }()
    
    let optionDescription: UILabel = {
        let label = UILabel()
        label.text = "Description"
        label.font = UIFont.systemFont(ofSize: 15, weight: .regular)
        label.textColor = .black
        label.textAlignment = .center
        label.numberOfLines = 0
//        label.lineBreakMode = .byCharWrapping
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupOptionView()
        deselected()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupOptionView() {
        
        addSubview(optionImage)
        addSubview(optionName)
        addSubview(optionDescription)
        
        optionImage.frame = CGRect(x: 0, y: 10, width: 110, height: 110)
        
        roundCorners(of: optionImage, by: 25)
        createShadow(for: optionImage)
        
        optionName.frame = CGRect(x: (frame.width - optionName.frame.width)/2, y: (optionImage.frame.minY + optionImage.frame.height + 15), width: optionImage.frame.width, height: optionName.font.pointSize)
        optionDescription.frame = CGRect(x: 0, y: (optionImage.frame.minY + optionImage.frame.height + 38), width: optionImage.frame.width, height: 40)
        
    }
    
    func deselected() {
        optionImage.alpha = 0.5
        optionName.alpha = 0.5
        optionDescription.alpha = 0.5
        self.isSelected = false
    }
    
    func selected() {
        optionImage.alpha = 1.0
        optionName.alpha = 1.0
        optionDescription.alpha = 1.0
        self.isSelected = true
    }
}
