//
//  AppointmentCollectionViewCell.swift
//  MyFigaro
//
//  Created by Albert Kwaśkiewicz on 2/21/20.
//  Copyright © 2020 fernando rosa. All rights reserved.
//

import UIKit

class AppointmentCollectionViewCell: UICollectionViewCell {
    
   override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupAppointmentView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let view = AppointmentView(frame: CGRect(x: 0, y: 0, width: 317, height: 162))
    
    func setupAppointmentView() {
        
        addSubview(view)
        
        view.frame = CGRect(x: frame.width/2 - view.frame.width/2, y: 0, width: view.apptWidth, height: view.apptHeight)
    }
    
}
