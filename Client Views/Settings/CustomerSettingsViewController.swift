//
//  SettingsViewController.swift
//  MyFigaro
//
//  Created by Albert Kwaśkiewicz on 2/28/20.
//  Copyright © 2020 fernando rosa. All rights reserved.
//

import UIKit

import UIKit

class CustomerSettingsViewController: UIViewController, UITextFieldDelegate {

    lazy var slideInTransitionDelegate = CustomInPresentationManager()
    
    //MARK: IBOutlets
    
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    @IBOutlet weak var editButton: UIBarButtonItem!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var nameView: UIView!
    @IBOutlet weak var customerNameTextField: UITextField!
    
    @IBOutlet weak var lastNameView: UIView!
    @IBOutlet weak var customerLastNameTextField: UITextField!
    
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var phoneView: UIView!
    @IBOutlet weak var phoneTextField: UITextField!
    
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var changePasswordButton: UIButton!
    
    @IBOutlet weak var logOutBtn: UIButton!
    @IBOutlet weak var deleteAccountBtn: UIButton!
    
    var activeField: UITextField?
        
        override func viewDidLoad() {
                super.viewDidLoad()
                
                let tap = UITapGestureRecognizer(target: self.view, action: #selector(self.view.endEditing))
                view.addGestureRecognizer(tap)
            }
            
            override func viewWillLayoutSubviews() {
                configViews()
            }
            
            override func viewWillAppear(_ animated: Bool) {
                customerNameTextField.text = UserManager.shared.customer.name
                customerLastNameTextField.text = UserManager.shared.customer.surname
                emailTextField.text = UserManager.shared.customer.email
                phoneTextField.text = UserManager.shared.customer.cell
                 
                // Set fields tags and delegates
                customerNameTextField.tag = 1001
                customerNameTextField.delegate = self
                customerLastNameTextField.tag = 1002
                customerLastNameTextField.delegate = self
                emailTextField.tag = 1003
                emailTextField.delegate = self
                phoneTextField.tag = 1004
                phoneTextField.delegate = self
                
                // Disables cancelButton
                self.cancelButton.isEnabled = false
                
                customerNameTextField.textColor = .lightGray
                customerLastNameTextField.textColor = .lightGray
                emailTextField.textColor = .lightGray
                phoneTextField.textColor = .lightGray
                
                self.changePasswordButton.isEnabled = true
                
                self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
            }
            
            override func viewWillDisappear(_ animated: Bool) {
                customerNameTextField.text = UserManager.shared.customer.name
                customerLastNameTextField.text = UserManager.shared.customer.surname
                emailTextField.text = UserManager.shared.customer.email
                phoneTextField.text = UserManager.shared.customer.cell
                customerNameTextField.textColor = .lightGray
                customerLastNameTextField.textColor = .lightGray
                emailTextField.textColor = .lightGray
                phoneTextField.textColor = .lightGray
                customerNameTextField.isEnabled = false
                customerLastNameTextField.isEnabled = false
                //emailTextField.isEnabled = false
                phoneTextField.isEnabled = false
                logOutBtn.isEnabled = true
                logOutBtn.alpha = 1
                deleteAccountBtn.isEnabled = true
                editButton.isEnabled = true
                self.cancelButton.isEnabled = false
                editButton.title = "Edit"
                
                NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
                NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
            }
            
            //MARK: Functions
            
            func configViews() {
               
                //Radii for buttons
                logOutBtn.backgroundColor = .white
                roundCorners(of: logOutBtn, by: 10)
                createShadow(for: logOutBtn)
                
                roundCorners(of: changePasswordButton, by: 5)
                
                //corner radii for each view
                roundCorners(of: nameView, by: 5)
                roundCorners(of: lastNameView, by: 5)
                roundCorners(of: emailView, by: 5)
                roundCorners(of: phoneView, by: 5)
                roundCorners(of: passwordView, by: 5)
                
                
            }
            
            func roundCorners(of view: UIView, by radius: CGFloat) {
                view.layer.cornerRadius = radius
                view.clipsToBounds = true
            }
            
            func createShadow(for view: UIView) {
                //clipsToBounds must be turned off to get shadow to show; FML
                view.clipsToBounds = false
                view.layer.shadowColor = UIColor.gray.cgColor
                view.layer.shadowOpacity = 0.5
                view.layer.shadowOffset = CGSize(width: 0, height: 4)
                view.layer.shadowRadius = 3
            }
            
            @objc func keyboardWillBeHidden(aNotification: NSNotification) {

                 let contentInsets: UIEdgeInsets = .zero

                 self.scrollView.contentInset = contentInsets

                 self.scrollView.scrollIndicatorInsets = contentInsets

            }
            
            @objc func keyboardWillShow(aNotification: NSNotification) {

               let info = aNotification.userInfo!

                let kbSize: CGSize = ((info["UIKeyboardFrameEndUserInfoKey"] as? CGRect)?.size)!

               print("kbSize = \(kbSize)")

                let contentInsets: UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: kbSize.height, right: 0.0)

               scrollView.contentInset = contentInsets

               scrollView.scrollIndicatorInsets = contentInsets

               var aRect: CGRect = self.view.frame

               aRect.size.height -= kbSize.height

               if !aRect.contains(activeField!.frame.origin) {

                    self.scrollView.scrollRectToVisible(activeField!.frame, animated: true)

               }

            }
            
            @IBAction func textFieldEditingChanged(_ sender: UITextField) {
                if customerNameTextField.text!.isEmpty || customerLastNameTextField.text!.isEmpty || emailTextField.text!.isEmpty || phoneTextField.text!.isEmpty {
                    editButton.isEnabled = false
                } else {
                    editButton.isEnabled = true
                }
                
            }
            
            @IBAction func editingDidEnd(_ sender: UITextField) {
                customerNameTextField.resignFirstResponder()
                customerLastNameTextField.resignFirstResponder()
                emailTextField.resignFirstResponder()
                phoneTextField.resignFirstResponder()
            }
            
            func textFieldDidBeginEditing(_ textField: UITextField) {
                self.activeField = textField
                switch textField.tag {
                case 1001:
                    self.nameView.layer.borderColor = UIColor(red: 64.0/255.0, green: 110.0/255.0, blue: 195.0/255.0, alpha: 1.0).cgColor
                    self.nameView.layer.borderWidth = 2.0
                case 1002:
                    self.lastNameView.layer.borderColor = UIColor(red: 64.0/255.0, green: 110.0/255.0, blue: 195.0/255.0, alpha: 1.0).cgColor
                    self.lastNameView.layer.borderWidth = 2.0
                case 1003:
                    self.emailView.layer.borderColor = UIColor(red: 64.0/255.0, green: 110.0/255.0, blue: 195.0/255.0, alpha: 1.0).cgColor
                    self.emailView.layer.borderWidth = 2.0
                case 1004:
                    self.phoneView.layer.borderColor = UIColor(red: 64.0/255.0, green: 110.0/255.0, blue: 195.0/255.0, alpha: 1.0).cgColor
                    self.phoneView.layer.borderWidth = 2.0
                case 1005:
                      self.passwordView.layer.borderColor = UIColor(red: 64.0/255.0, green: 110.0/255.0, blue: 195.0/255.0, alpha: 1.0).cgColor
                      self.passwordView.layer.borderWidth = 2.0
                default:
                    print()
                }
            }
            
            func textFieldDidEndEditing(_ textField: UITextField) {
                switch textField.tag {
                case 1001:
                    self.nameView.layer.borderWidth = 0.0
                case 1002:
                    self.lastNameView.layer.borderWidth = 0.0
                case 1003:
                    self.emailView.layer.borderWidth = 0.0
                case 1004:
                    self.phoneView.layer.borderWidth = 0.0
                case 1005:
                    self.passwordView.layer.borderWidth = 0.0
                default:
                    print()
                }
                self.activeField = nil
                //self.checkTxtFields()
            }
            
            func textFieldShouldReturn(_ textField: UITextField) -> Bool {
                let nextTag = textField.tag + 1

                if let nextResponder = self.view.viewWithTag(nextTag) {
                    nextResponder.becomeFirstResponder()
                } else {
                    textField.resignFirstResponder()
                }
                return true
            }
            
            @objc func dismissKeyboard() {
                view.endEditing(true)
            }
            
            // Cancel Button Action
            @IBAction func cancelButtonTapped(_ sender: UIBarButtonItem) {
                customerNameTextField.text = UserManager.shared.customer.name
                customerLastNameTextField.text = UserManager.shared.customer.surname
                emailTextField.text = UserManager.shared.customer.email
                phoneTextField.text = UserManager.shared.customer.cell
                customerNameTextField.textColor = .lightGray
                customerLastNameTextField.textColor = .lightGray
                emailTextField.textColor = .lightGray
                phoneTextField.textColor = .lightGray
                customerNameTextField.isEnabled = false
                customerLastNameTextField.isEnabled = false
                //emailTextField.isEnabled = false
                phoneTextField.isEnabled = false
                logOutBtn.isEnabled = true
                logOutBtn.alpha = 1
                deleteAccountBtn.isEnabled = true
                editButton.isEnabled = true
                self.cancelButton.isEnabled = false
                editButton.title = "Edit"
                
                self.changePasswordButton.isEnabled = true
            }
            
            // Edit Button Action
            @IBAction func editButtonTapped(_ sender: UIBarButtonItem) {

                NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(aNotification:)), name: UIResponder.keyboardWillHideNotification, object: nil)

               NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(aNotification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
                
                
                
                // Updates BarButtonItem
                if editButton.title == "Edit" {
                    
                    self.changePasswordButton.isEnabled = false
                    
                    self.cancelButton.isEnabled = true
                    logOutBtn.isEnabled = false
                    logOutBtn.alpha = 0.5
                    deleteAccountBtn.isEnabled = false
                    editButton.title = "Save"
                    editButton.isEnabled = false
                    
                    // Enables all the addPictureButton and the Textfields to edit
                    customerNameTextField.isEnabled = true
                    customerLastNameTextField.isEnabled = true
                    //emailTextField.isEnabled = true
                    phoneTextField.isEnabled = true
               
                    // Sets values for each TextField
                    customerNameTextField.placeholder = "Insert First Name"
                    customerLastNameTextField.placeholder = "Insert Last Name"
                    emailTextField.placeholder = "Insert E-mail"
                    phoneTextField.placeholder = "Insert Phone Number"
                    customerNameTextField.text = UserManager.shared.customer.name
                    customerLastNameTextField.text = UserManager.shared.customer.surname
                    emailTextField.text = UserManager.shared.customer.email
                    phoneTextField.text = UserManager.shared.customer.cell
                    
                    // Sets values for each TextField
                    customerNameTextField.textColor = .label
                    customerLastNameTextField.textColor = .label
                    emailTextField.textColor = .label
                    phoneTextField.textColor = .label
                    
                } else if editButton.title == "Save" {
                    
                    self.changePasswordButton.isEnabled = true
                    
                    self.cancelButton.isEnabled = false
                    logOutBtn.isEnabled = true
                    logOutBtn.alpha = 1
                    deleteAccountBtn.isEnabled = true
                    UserManager.shared.customer.name = customerNameTextField.text!
                    UserManager.shared.customer.surname = customerLastNameTextField.text!
                    UserManager.shared.customer.email = emailTextField.text!
                    UserManager.shared.customer.cell = phoneTextField.text!
                    customerNameTextField.textColor = .lightGray
                    customerLastNameTextField.textColor = .lightGray
                    emailTextField.textColor = .lightGray
                    phoneTextField.textColor = .lightGray
               
                    customerNameTextField.isEnabled = false
                    customerLastNameTextField.isEnabled = false
                    //emailTextField.isEnabled = false
                    phoneTextField.isEnabled = false
                    
                    
                    let customerToSave:Client = Client(name: UserManager.shared.customer.name, surname: UserManager.shared.customer.surname, cell: UserManager.shared.customer.cell, email: UserManager.shared.customer.email, isActive: UserManager.shared.customer.isActive)
                    self.showSpinner(onView: self.view)
                    DataBaseManager.shared.addCustomer(customer: customerToSave, completion: { msg in
                        self.removeSpinner()
                        self.showErrorAlert(title: "Customer Update", msg: msg)
                    })
                    
                    editButton.title = "Edit"
                    
                }
            }
            
            @IBAction func changePasswordTap(sender: UIButton) {
               let storyBoard: UIStoryboard = UIStoryboard(name: "Barber", bundle: nil)
               let cpViewController = storyBoard.instantiateViewController(withIdentifier: "changePasswordView") as! ChangePasswordViewController
                cpViewController.transitioningDelegate = self.slideInTransitionDelegate
                self.slideInTransitionDelegate.direction = .bottom
                cpViewController.modalPresentationStyle = .custom
                
                NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)

                NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
                
               self.present(cpViewController, animated: true, completion: nil)
            }
    
    func showErrorAlert(title:String, msg:String) {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { _ in
            
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    var vSpinner: UIView?
    
    func showSpinner(onView : UIView) {
       let spinnerView = UIView.init(frame: onView.frame)
        spinnerView.backgroundColor = UIColor.init(red: 64.0/255.0, green: 110.0/255.0, blue: 195.0/255.0, alpha: 0.1)
       let ai = UIActivityIndicatorView.init(style: .large)
       ai.color =  UIColor(red: 64.0/255.0, green: 110.0/255.0, blue: 195.0/255.0, alpha: 1.0)
    
       let image = UIImageView(image: UIImage(named: "Customer"))
       image.frame.size = CGSize(width: 60, height: 60)
       image.center = CGPoint(x: spinnerView.center.x, y: spinnerView.center.y - 60.0)
       ai.startAnimating()
       ai.center = spinnerView.center
       
       DispatchQueue.main.async {
           spinnerView.addSubview(ai)
           spinnerView.addSubview(image)
           onView.addSubview(spinnerView)
       }
       
       vSpinner = spinnerView
    }
    
    func removeSpinner() {
        DispatchQueue.main.async {
            self.vSpinner?.removeFromSuperview()
            self.vSpinner = nil
        }
    }
            
}
