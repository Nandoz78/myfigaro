//
//  CustomerSettingsNavigationController.swift
//  MyFigaro
//
//  Created by fernando rosa on 10/03/2020.
//  Copyright © 2020 fernando rosa. All rights reserved.
//

import UIKit

class CustomerSettingsNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

        configTabBarItem()
        configNavBar()
        self.navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    private func configTabBarItem() {
        self.tabBarItem.title = "Settings"
        self.tabBarItem.image = UIImage.init(systemName: "gear")
//        self.tabBarItem.selectedImage = UIImage.init(systemName: "house.fill")
        self.tabBarController?.view.tintColor = #colorLiteral(red: 0.2509803922, green: 0.431372549, blue: 0.7647058824, alpha: 1)
    }
    
    private func configNavBar() {
        self.navigationBar.topItem?.title = "Settings"
        self.navigationBar.tintColor = #colorLiteral(red: 0.2509803922, green: 0.431372549, blue: 0.7647058824, alpha: 1)
        self.navigationBar.prefersLargeTitles = true
        self.navigationBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.navigationBar.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
    }

}
