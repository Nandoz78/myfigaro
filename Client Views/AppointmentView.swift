//
//  AppointmentView.swift
//  MyFigaro
//
//  Created by Albert Kwaśkiewicz on 2/21/20.
//  Copyright © 2020 fernando rosa. All rights reserved.
//

import UIKit

class AppointmentView: UIView {
    
    let apptWidth: CGFloat = 317
    let apptHeight: CGFloat = 162
    
    let date: UILabel = {
        let label = UILabel()
        label.text = "Date"
        label.textColor = #colorLiteral(red: 0.2509803922, green: 0.431372549, blue: 0.7647058824, alpha: 1)
        label.font = UIFont.systemFont(ofSize: 25, weight: .semibold)
        label.numberOfLines = 1
        return label
    }()
    
    let time: UILabel = {
        let label = UILabel()
        label.text = "Time"
        label.textColor = #colorLiteral(red: 0.2509803922, green: 0.431372549, blue: 0.7647058824, alpha: 1)
        label.font = UIFont.systemFont(ofSize: 25, weight: .semibold)
        return label
    }()
    
    let shop: UILabel = {
        let label = UILabel()
        label.text = "Shop"
        label.textColor = #colorLiteral(red: 0.2509803922, green: 0.431372549, blue: 0.7647058824, alpha: 1)
        label.font = UIFont.systemFont(ofSize: 20, weight: .light)
        return label
    }()
    
    let service: UILabel = {
        let label = UILabel()
        label.text = "Shop"
        label.textColor = #colorLiteral(red: 0.2509803922, green: 0.431372549, blue: 0.7647058824, alpha: 1)
        label.font = UIFont.systemFont(ofSize: 20, weight: .semibold)
        return label
    }()
    
    let barber: UILabel = {
        let label = UILabel()
        label.text = "Shop"
        label.textColor = #colorLiteral(red: 0.2509803922, green: 0.431372549, blue: 0.7647058824, alpha: 1)
        label.font = UIFont.systemFont(ofSize: 15, weight: .semibold)
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        configApptView()
        setupAppointmentView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupAppointmentView() {
        
        addSubview(date)
        addSubview(time)
        addSubview(shop)
        addSubview(barber)
        
        date.frame = CGRect(x: 20, y: 15, width: self.frame.width/2, height: date.font.pointSize)
        time.frame = CGRect(x: (frame.width - self.frame.width)/2 + self.frame.width - self.frame.width/3 + 20, y: 15, width: self.frame.width/3 - 20, height: date.font.pointSize)
        shop.frame = CGRect(x: 20, y: 50, width: self.frame.width/2, height: shop.font.pointSize)
        barber.frame = CGRect(x: 20, y: self.frame.height - barber.font.pointSize - 20, width: self.frame.width/2, height: barber.font.pointSize)
    }
    
    private func configApptView() {
        self.sizeThatFits(CGSize(width: apptWidth, height: apptHeight))
        roundCorners(of: self, by: 25)
        self.backgroundColor = #colorLiteral(red: 0.968627451, green: 0.968627451, blue: 0.968627451, alpha: 1)
        
        createShadow(for: self)
        
    }
}

extension UIView {
    func roundCorners(of view: UIView, by radius: CGFloat) {
        view.layer.cornerRadius = radius
        view.clipsToBounds = true
    }
    
    func createShadow(for view: UIView) {
        //clipsToBounds must be turned off to get shadow to show; FML
        view.clipsToBounds = false
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: 0, height: 4)
        view.layer.shadowRadius = 3
    }
}
