//
//  ShopTableViewCell.swift
//  MyFigaro
//
//  Created by Albert Kwaśkiewicz on 2/25/20.
//  Copyright © 2020 fernando rosa. All rights reserved.
//

import UIKit

class ShopCollectionViewCell: UICollectionViewCell {
    
    let shop = ShopView()
    
    let shopWidth: CGFloat = 317
    let shopHeight: CGFloat = 110

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupView() {
        addSubview(shop)
        
        shop.frame = CGRect(x: 0, y: 0, width: shopWidth, height: shopHeight)
        
        shop.center.x = self.center.x
    }

}
