//
//  ShopView.swift
//  MyFigaro
//
//  Created by Albert Kwaśkiewicz on 2/25/20.
//  Copyright © 2020 fernando rosa. All rights reserved.
//

import UIKit

class ShopView: UIView {

    let shopWidth: CGFloat = 317
    let shopHeight: CGFloat = 110
    
    let shopImgSideLength: CGFloat = 110
    
    let shopImage: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "Headrush")
        iv.contentMode = .scaleAspectFill
//        iv.backgroundColor = #colorLiteral(red: 0.968627451, green: 0.968627451, blue: 0.968627451, alpha: 1)
        return iv
    }()
    
    let shopName: UILabel = {
        let label = UILabel()
        label.text = "Store Name"
        label.font = UIFont.systemFont(ofSize: 25, weight: .semibold)
        label.textColor = #colorLiteral(red: 0.2509803922, green: 0.431372549, blue: 0.7647058824, alpha: 1)
        label.textAlignment = .center
        return label
    }()
    
    let ownerName: UILabel = {
            let label = UILabel()
            label.text = "Owner Name"
            label.font = UIFont.systemFont(ofSize: 15, weight: .regular)
            label.textColor = #colorLiteral(red: 0.2509803922, green: 0.431372549, blue: 0.7647058824, alpha: 1)
            label.textAlignment = .center
            return label
        }()
    
    let stackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = NSLayoutConstraint.Axis.vertical
        stack.distribution = .equalSpacing
        stack.alignment = .center
        stack.spacing = 5
        return stack
    }()
    
    let chevronImage: UIImageView = {
        let iv = UIImageView()
        let largeSemiboldConfiguration = UIImage.SymbolConfiguration(pointSize: UIFont.systemFontSize, weight: .semibold, scale: .large)
        iv.image = UIImage(systemName: "chevron.right", withConfiguration: largeSemiboldConfiguration)
        return iv
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupShopView()
        configShopView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupShopView() {
        addSubview(shopImage)
//        addSubview(shopName)
//        addSubview(ownerName)
//        addSubview(chevronImage)
        addSubview(stackView)
        stackView.addArrangedSubview(shopName)
        stackView.addArrangedSubview(ownerName)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        shopImage.frame = CGRect(x: 0, y: 0, width: shopImgSideLength, height: shopImgSideLength)
        roundCorners(of: shopImage, by: 25)
        
        stackView.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 48.5).isActive = true
        stackView.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
//        stackView.center.y = shopImage.center.y
        
//        shopName.frame = CGRect(x: 120, y: shopImage.center.y, width: 173, height: shopName.font.pointSize)
//        shopName.center.y = shopImage.center.y
//        shopName.frame = CGRect
//        (shopImage.frame.width + 10)
//        frame.width - shopName.frame.maxX - 22
//        shopName.frame = CGRect(x: 120, y: shopName.center, width: 173, height: shopName.font.pointSize)
//        ownerName.frame = CGRect(x: shopName.frame.maxX, y: shopName.frame.maxY + 5, width: shopName.frame.width, height: ownerName.font.pointSize)
//        ownerName.center.x = shopName.center.x
        
    }
    
    private func configShopView() {
        self.sizeThatFits(CGSize(width: shopWidth, height: shopHeight))
        roundCorners(of: self, by: 25)
        self.backgroundColor = #colorLiteral(red: 0.968627451, green: 0.968627451, blue: 0.968627451, alpha: 1)
        createShadow(for: self)
    }
    
}
