//
//  NotificationsTableViewController.swift
//  MyFigaro
//
//  Created by Albert Kwaśkiewicz on 2/27/20.
//  Copyright © 2020 fernando rosa. All rights reserved.
//

import UIKit

class NotificationsTableViewController: UITableViewController {
    
    //MARK: Variables
    let notificationCell = "notificationCell"
    var didLayout = false

    //MARK: IBOutlets
    
    override func viewDidLoad() {
        super.viewDidLoad()

//        setupTableView()

    }
    
//    override func viewDidLayoutSubviews() {
//        if !self.didLayout {
//            self.didLayout = true
//            self.tableView.reloadData()
//        }
//    }
    
    //MARK: Functions
    func setupTableView() {
//        self.tableView.register(NotificationsTableViewCell.self, forCellReuseIdentifier: notificationCell)
    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 5
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: notificationCell, for: indexPath) as! NotificationsTableViewCell

//        cell.layoutSubviews()
        cell.imageView?.image = UIImage(named: "Headrush")
        configImageView(view: cell.imageView!)
        cell.separatorInset = .zero
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
    }
}

extension UITableViewController {
    
    func configImageView(view: UIImageView) {
        roundCorners(of: view, by: 15)
//        createShadow(for: view)
//        view.translatesAutoresizingMaskIntoConstraints = false
    }
    
    func roundCorners(of view: UIView, by radius: CGFloat) {
        view.layer.cornerRadius = radius
        view.clipsToBounds = true
//        view.layer.mask = true
    }
    
    func createShadow(for view: UIView) {
        //clipsToBounds must be turned off to get shadow to show; FML
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: 0, height: 4)
        view.layer.shadowRadius = 3
        view.clipsToBounds = false
    }
}



