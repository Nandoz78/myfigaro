//
//  NotificationsTableViewCell.swift
//  MyFigaro
//
//  Created by Albert Kwaśkiewicz on 2/27/20.
//  Copyright © 2020 fernando rosa. All rights reserved.
//

import UIKit

class NotificationsTableViewCell: UITableViewCell {
    
    //MARK: IBOutlets
    @IBOutlet weak var shopImageView: UIImageView!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var postDateLabel: UILabel!
    
//    override func layoutSubviews() {
//        super.layoutSubviews()
//
////        setupView()
//    }
//
    override func awakeFromNib() {
        super.awakeFromNib()
        
//        roundCorners(of: shopImageView, by: 15)
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setupView() {
        roundCorners(of: shopImageView, by: 15)
//        createShadow(for: shopImageView)
    }
    
}

