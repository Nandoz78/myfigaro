//
//  NotificationsNavClientViewController.swift
//  MyFigaro
//
//  Created by Albert Kwaśkiewicz on 2/22/20.
//  Copyright © 2020 fernando rosa. All rights reserved.
//

import UIKit

class NotificationsNavClientViewController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

        configTabBarItem()
        configNavBar()
    }
    
    private func configTabBarItem() {
        self.tabBarItem.title = "Notifications"
        self.tabBarItem.image = UIImage.init(systemName: "envelope")
        self.tabBarItem.selectedImage = UIImage.init(systemName: "envelope.fill")
        self.tabBarController?.view.tintColor = #colorLiteral(red: 0.2509803922, green: 0.431372549, blue: 0.7647058824, alpha: 1)
    }
    
    private func configNavBar() {
        self.navigationBar.topItem?.title = "Notifications"
        self.navigationBar.tintColor = #colorLiteral(red: 0.2509803922, green: 0.431372549, blue: 0.7647058824, alpha: 1)
        self.navigationBar.prefersLargeTitles = true
//        self.navigationBar.barTintColor = #colorLiteral(red: 0.9764705882, green: 0.9764705882, blue: 0.9764705882, alpha: 1)
//        self.navigationBar.backgroundColor = #colorLiteral(red: 0.9764705882, green: 0.9764705882, blue: 0.9764705882, alpha: 1)
    }
}
