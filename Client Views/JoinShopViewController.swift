//
//  JoinShopViewController.swift
//  MyFigaro
//
//  Created by fernando rosa on 11/03/2020.
//  Copyright © 2020 fernando rosa. All rights reserved.
//

import UIKit

protocol JoinDelegate {
    func joined(withSuccess:Bool)
}

class JoinShopViewController: UIViewController {
    
    var selectedShop:Shop!
    @IBOutlet weak var imgShop: UIImageView!
    @IBOutlet weak var lblShopName: UILabel!
    @IBOutlet weak var lblOwnerName: UILabel!
    @IBOutlet weak var btnJoin: UIButton!
    @IBOutlet weak var lblAddressShop: UILabel!
    @IBOutlet weak var btnCancel: UIButton!
    
    var delegate:JoinDelegate?
    var joined:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.layer.cornerRadius = 20.0
        self.btnJoin.layer.cornerRadius = 5.0
        self.lblShopName.text = self.selectedShop.shopName
        self.lblOwnerName.text = self.selectedShop.ownerName
        self.lblAddressShop.text = self.selectedShop.address
        
        
    }
    
    override func viewWillLayoutSubviews() {
        self.view.layer.cornerRadius = 20.0
    }
    
    
    @IBAction func join(_ sender: Any) {
        self.showSpinner(onView: self.view)
        DataBaseManager.shared.joinShop(shopId: self.selectedShop.shopId, customer: UserManager.shared.customer, completion: { msg in
            self.removeSpinner()
            if(msg == "Joined!"){
                self.joined = true
            }
            self.showErrorAlert(title: "Join Shop", msg: msg)
        })
    }
    
    
    @IBAction func cancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func showErrorAlert(title:String, msg:String) {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { _ in
            self.dismiss(animated: true, completion: {
                self.delegate?.joined(withSuccess: self.joined)
            })
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    var vSpinner: UIView?
    
    func showSpinner(onView : UIView) {
       let spinnerView = UIView.init(frame: onView.frame)
        spinnerView.backgroundColor = UIColor.init(red: 64.0/255.0, green: 110.0/255.0, blue: 195.0/255.0, alpha: 0.1)
       let ai = UIActivityIndicatorView.init(style: .large)
       ai.color =  UIColor(red: 64.0/255.0, green: 110.0/255.0, blue: 195.0/255.0, alpha: 1.0)
    
       let image = UIImageView(image: UIImage(named: "Customer"))
       image.frame.size = CGSize(width: 60, height: 60)
       image.center = CGPoint(x: spinnerView.center.x, y: spinnerView.center.y - 60.0)
       ai.startAnimating()
       ai.center = spinnerView.center
       
       DispatchQueue.main.async {
           spinnerView.addSubview(ai)
           spinnerView.addSubview(image)
           onView.addSubview(spinnerView)
       }
       
       vSpinner = spinnerView
    }
    
    func removeSpinner() {
        DispatchQueue.main.async {
            self.vSpinner?.removeFromSuperview()
            self.vSpinner = nil
        }
    }

}
