//
//  ScannerNavController.swift
//  MyFigaro
//
//  Created by Albert Kwaśkiewicz on 2/24/20.
//  Copyright © 2020 fernando rosa. All rights reserved.
//

import UIKit

class ScannerNavController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

        configNavBar()
    }
    
    private func configNavBar() {
        self.navigationBar.topItem?.title = "Scan QR Code"
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: nil, action: nil)
        self.navigationBar.tintColor = #colorLiteral(red: 0.2509803922, green: 0.431372549, blue: 0.7647058824, alpha: 1)
        self.navigationBar.prefersLargeTitles = true
        self.navigationBar.barTintColor = #colorLiteral(red: 0.9764705882, green: 0.9764705882, blue: 0.9764705882, alpha: 1)
        self.navigationBar.backgroundColor = #colorLiteral(red: 0.9764705882, green: 0.9764705882, blue: 0.9764705882, alpha: 1)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
