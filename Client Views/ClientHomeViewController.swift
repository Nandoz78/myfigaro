//
//  ClientHomeViewController.swift
//  MyFigaro
//
//  Created by Albert Kwaśkiewicz on 2/21/20.
//  Copyright © 2020 fernando rosa. All rights reserved.
//

import UIKit

class ClientHomeViewController: UIViewController, ScannerDelegate, JoinDelegate {
    
    func joined(withSuccess: Bool) {
        if(withSuccess){
            self.loadData()
        }
    }
    
    
    lazy var slideInTransitionDelegate = CustomInPresentationManager()
    
    func scanned(scannedId: String) {
        self.showSpinner(onView: self.view)
        DataBaseManager.shared.getShopInfoForCustomerSubscription(shopId: scannedId, completion: { shop in
            self.removeSpinner()
            if(shop != nil){
                let storyBoard: UIStoryboard = UIStoryboard(name: "Client", bundle: nil)
                let nvc = storyBoard.instantiateViewController(withIdentifier: "joinView") as! JoinShopViewController
                nvc.transitioningDelegate = self.slideInTransitionDelegate
                self.slideInTransitionDelegate.direction = .bottom
                nvc.modalPresentationStyle = .custom
                nvc.selectedShop = shop
                nvc.delegate = self
                self.present(nvc, animated: true, completion: nil)
            }
        })
    }
    
    
    var apptCollection = [Appointment]()
    var shopsCollection = [Shop]()
    
    var isPlusApptsTapped: Bool = false
    let plusApptsSegueID = "addApptSegue"
    
    private let apptCellID = "apptCell"
    private let shopCellID = "shopCell"
    
    var apptWidth: CGFloat = 317
    var apptHeight: CGFloat = 162
    
    let shopWidth: CGFloat = 317
    let shopHeight: CGFloat = 110
    
    //    let newShopSectionHeight =
    
    //MARK: IBOutlets
    @IBOutlet weak var appointmentsLabel: UILabel!
    @IBOutlet weak var shopsLabel: UILabel!
    @IBOutlet weak var appointments: UICollectionView!
    @IBOutlet weak var addApptButton: UIButton!
    @IBOutlet weak var addShopButton: UIButton!
    @IBOutlet weak var shopSectionStack: UIStackView!
    @IBOutlet weak var shopsCollectionView: UICollectionView!
    
    
    fileprivate func checkApptExists() {
        if apptCollection.isEmpty {
            appointments.isHidden = true
            UIView.animate(withDuration: 0) {
                let translateTransform = CGAffineTransform(translationX: 0, y: -185)
                self.shopSectionStack.transform = translateTransform
                self.shopsCollectionView.transform = translateTransform
            }
            
            shopsCollectionView.translatesAutoresizingMaskIntoConstraints = false
            shopsCollectionView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
        checkApptExists()
        self.navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    func loadData(){
       self.showSpinner(onView: self.view)
        
        if(UserManager.shared.customer.id == ""){
            DataBaseManager.shared.getCustomerInfo(client:  UserManager.shared.customer, completion: { res in
                if (res){
                    DataBaseManager.shared.getClientShops(customer: UserManager.shared.customer, completion: { shops in
                        self.removeSpinner()
                        self.shopsCollection = shops!
                        self.shopsCollectionView.reloadData()
                    })
                }else{
                    self.removeSpinner()
                    self.showErrorAlert(title: "Customer's Info", msg: "...")
                }
            })
        } else {
            DataBaseManager.shared.getClientShops(customer: UserManager.shared.customer, completion: { shops in
                self.removeSpinner()
                self.shopsCollection = shops!
                self.shopsCollectionView.reloadData()
            })
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.prefersLargeTitles = true
        self.loadData()
    }
    
    //MARK: IBActions
    @IBAction func displayShopActionSheet(_ sender: Any) {
        let addShopAlert = setupActionSheet()
        self.present(addShopAlert, animated: true, completion: nil)
    }
    
    @IBAction func addApptPlusTapped(_ sender: Any) {
        isPlusApptsTapped = true
    }
    
    
    //MARK: Functions
    
    func setupViews() {
        let apptLayout = UICollectionViewFlowLayout()
        apptLayout.scrollDirection = .horizontal
        
        //Collection view setup
        appointments.frame = CGRect(x: 0, y: 220, width: view.frame.width, height: apptHeight)
        appointments.backgroundColor = .clear
        appointments.collectionViewLayout = apptLayout
        appointments.showsHorizontalScrollIndicator = false
        appointments.translatesAutoresizingMaskIntoConstraints = false
        
        appointments.delegate = self
        appointments.dataSource = self
        
        appointments.register(AppointmentCollectionViewCell.self, forCellWithReuseIdentifier: apptCellID)
        
        //Collection view setup for shop
        let shopLayout = UICollectionViewFlowLayout()
        shopLayout.scrollDirection = .vertical
        
        //        shopsCollectionView.backgroundColor = .red
        shopsCollectionView.collectionViewLayout = shopLayout
        shopsCollectionView.showsVerticalScrollIndicator = false
        shopsCollectionView.translatesAutoresizingMaskIntoConstraints = false
        
        shopsCollectionView.delegate = self
        shopsCollectionView.dataSource = self
        
        shopsCollectionView.register(ShopCollectionViewCell.self, forCellWithReuseIdentifier: shopCellID)
        
        
    }
    
    /// Sets up action view for adding a store, providing options between manually adding code or scanning QR Code
    func setupActionSheet() -> UIAlertController {
        let addShopAlert = UIAlertController(title: "Add Barber Shop", message: "Select an Option", preferredStyle: .actionSheet)
        
        //TODO: Retrieve shop infomation on successful scan
        let scanQRAction = UIAlertAction(title: "Scan QR Code", style: .default, handler: { (UIAlertAction) in
            
            
            let storyBoard: UIStoryboard = UIStoryboard(name: "Client", bundle: nil)
            let nvc = storyBoard.instantiateViewController(withIdentifier: "scanView") as! ScannerViewController
            nvc.delegate = self
            self.present(nvc, animated: true, completion: nil)
            
            
            print("Scan QR Code")
        })
        
        let okAction = UIAlertAction(title: "OK", style: .default, handler: { (UIAlertAction) in
            print("OK, Code sent")
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: { (UIAlertAction) in
            print("Cancel")
        })
        
        let addCodeAction = UIAlertAction(title: "Add Code", style: .default, handler: { (UIAlertAction) in
            let alert = UIAlertController(title: "Add Barber Shop", message: "Add Code", preferredStyle: .alert)
            
            alert.addAction(okAction)
            alert.addAction(cancelAction)
            alert.addTextField { (textField) in
                textField.placeholder = "Code"
            }
            
            self.present(alert, animated: true, completion: nil)
            print("Add Code")
        })
        
        addShopAlert.addAction(scanQRAction)
        addShopAlert.addAction(addCodeAction)
        addShopAlert.addAction(cancelAction)
        
        return addShopAlert
    }
    
    //MARK: Navigation to booking new appt
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == plusApptsSegueID {
            let destination = segue.destination as! CustomerNewAppointmentViewController
            
            destination.wasPlusApptsTapped = self.isPlusApptsTapped
            
            if shopsCollection.count == 1 {
                destination.selectedShop = shopsCollection[0]
                destination.shopsList = [nil]
            } else {
                destination.selectedShop = nil
                destination.shopsList = shopsCollection
            }
            
            print("Segueing into New Appointment")
            print(shopsCollection.count)
            
        }
    }
    
    func showErrorAlert(title:String, msg:String) {
           let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
           alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { _ in
               
           }))
           self.present(alert, animated: true, completion: nil)
       }
       
       var vSpinner: UIView?
       
       func showSpinner(onView : UIView) {
          let spinnerView = UIView.init(frame: onView.frame)
           spinnerView.backgroundColor = UIColor.init(red: 64.0/255.0, green: 110.0/255.0, blue: 195.0/255.0, alpha: 0.1)
          let ai = UIActivityIndicatorView.init(style: .large)
          ai.color =  UIColor(red: 64.0/255.0, green: 110.0/255.0, blue: 195.0/255.0, alpha: 1.0)
       
          let image = UIImageView(image: UIImage(named: "Customer"))
          image.frame.size = CGSize(width: 60, height: 60)
          image.center = CGPoint(x: spinnerView.center.x, y: spinnerView.center.y - 60.0)
          ai.startAnimating()
          ai.center = spinnerView.center
          
          DispatchQueue.main.async {
              spinnerView.addSubview(ai)
              spinnerView.addSubview(image)
              onView.addSubview(spinnerView)
          }
          
          vSpinner = spinnerView
       }
       
       func removeSpinner() {
           DispatchQueue.main.async {
               self.vSpinner?.removeFromSuperview()
               self.vSpinner = nil
           }
       }
}

//MARK: Collection View Extensions
extension ClientHomeViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.appointments {
            return apptCollection.count
        } else {
            return shopsCollection.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.appointments {
            let cell = appointments.dequeueReusableCell(withReuseIdentifier: apptCellID, for: indexPath) as! AppointmentCollectionViewCell
            //TODO: PASS INFORMATION TO APPT CELLS
            cell.view.date.text = "Tomorrow"
//                apptCollection[indexPath.item].start
            cell.view.time.text = "12:00"
            cell.view.shop.text = "Headrush"
            cell.view.barber.text = apptCollection[indexPath.row].barber.name
            return cell
        } else {
            let cell = shopsCollectionView.dequeueReusableCell(withReuseIdentifier: shopCellID, for: indexPath) as! ShopCollectionViewCell
            
            cell.shop.shopImage.image = UIImage(named: shopsCollection[indexPath.item].image)
            cell.shop.shopName.text = shopsCollection[indexPath.item].shopName
            cell.shop.ownerName.text = shopsCollection[indexPath.item].ownerName
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == self.appointments {
            return CGSize(width: view.frame.width, height: apptHeight + 50)
        } else {
            return CGSize(width: view.frame.width, height: shopHeight)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        if collectionView == self.appointments {
            return UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 20)
        } else {
            return UIEdgeInsets(top: 0, left: 20, bottom: 20, right: 20)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        if collectionView == self.appointments {
            return -75
        } else {
            return 15
        }
    }
    
   
}
