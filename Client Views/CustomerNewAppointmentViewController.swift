//
//  NewAppointmentViewController.swift
//  MyFigaro
//
//  Created by Albert Kwaśkiewicz on 2/24/20.
//  Copyright © 2020 fernando rosa. All rights reserved.
//

import UIKit

//var clientShops = ResourcesManager.shared.getClientsShops(shops: <#T##[String]#>)

class CustomerNewAppointmentViewController: UIViewController {
    
    let shopsCollectionViewTag = 3003
    let servicesCollectionViewTag = 3001
    let barbersCollectionViewTag = 3002
    
    let serviceCellID = "serviceCell"
    let barberCellID = "barberCell"
    let shopCellID = "shopCell"
    
    let optionWidth: CGFloat = 110
    let optionHeight: CGFloat = 200
    
    var wasPlusApptsTapped: Bool = true
    
    var selectedShop: Shop? = nil
    var shopsList: [Shop?] = []
    var selectedServices: [Treatment] = []
    var servicesList: [Treatment?] = ResourcesManager.shared.getTreatments(shopId: "001")
    var selectedBarber: Barber? = nil
    var barberList: [Barber?] = ResourcesManager.shared.getBarbers(shopId: "001")
    
    var toCalendarSegueID = "toCalendarSegue"
    
    
    var barbersList:[Barber] = [Barber]()
    
    //MARK: IBOutlets
    @IBOutlet weak var servicesLabel: UILabel!
    @IBOutlet weak var barbersLabel: UILabel!
    @IBOutlet weak var shopsLabel: UILabel!
    
    @IBOutlet weak var servicesCollectionView: UICollectionView!
    @IBOutlet weak var barbersCollectionView: UICollectionView!
    @IBOutlet weak var shopsCollectionView: UICollectionView!
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var newApptScrollView: UIScrollView!
    
    @IBOutlet weak var nextBarButtonItem: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configNavBar()
        setupCollectionViews()
        determineIfShopsShown()
        
        if servicesList.indices.contains(1) {
            print(servicesList[0]!)
        }

    }
    
    //MARK: Functions
    
    private func configNavBar() {
        self.navigationItem.title = "New Appointment"
    self.navigationController?.navigationBar.prefersLargeTitles = false
        
        nextBarButtonItem.isEnabled = false
        
    }
    
    /// Sets up the two collection views used in the New Appt page
    func setupCollectionViews() {
        
        servicesCollectionView.tag = servicesCollectionViewTag
        barbersCollectionView.tag = barbersCollectionViewTag
        shopsCollectionView.tag = shopsCollectionViewTag
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        
        //servicesCollectionView setup
        //        servicesCollectionView.backgroundColor = .black
        servicesCollectionView.collectionViewLayout = layout
        servicesCollectionView.showsHorizontalScrollIndicator = false
        servicesCollectionView.translatesAutoresizingMaskIntoConstraints = false
        servicesCollectionView.delegate = self
        servicesCollectionView.dataSource = self
        
        //barbersCollectionView setup
        //        barbersCollectionView.backgroundColor = .clear
        barbersCollectionView.collectionViewLayout = layout
        barbersCollectionView.showsHorizontalScrollIndicator = false
        barbersCollectionView.translatesAutoresizingMaskIntoConstraints = false
        barbersCollectionView.delegate = self
        barbersCollectionView.dataSource = self
        
        //shopsCollectionView setup
//        shopsCollectionView.backgroundColor = .black
        shopsCollectionView.collectionViewLayout = layout
        shopsCollectionView.showsHorizontalScrollIndicator = false
        shopsCollectionView.translatesAutoresizingMaskIntoConstraints = false
        shopsCollectionView.delegate = self
        shopsCollectionView.dataSource = self
        
        //Collection views registration
        servicesCollectionView.register(OptionCollectionViewCell.self, forCellWithReuseIdentifier: serviceCellID)
        barbersCollectionView.register(OptionCollectionViewCell.self, forCellWithReuseIdentifier: barberCellID)
        shopsCollectionView.register(OptionCollectionViewCell.self, forCellWithReuseIdentifier: shopCellID)
        
        
    }
    
    private func nextButtonToggle() {
        if !selectedServices.isEmpty && selectedShop != nil {
            nextBarButtonItem.isEnabled = true
        } else {
            nextBarButtonItem.isEnabled = false
        }
    }
    
    private func determineIfShopsShown() -> Bool {
        
        print("Plus Appts Tapped")
        
        if selectedShop != nil {
            shopsLabel.isHidden = true
            shopsLabel.isEnabled = false
            shopsCollectionView.isHidden = true
            shopsCollectionView.isUserInteractionEnabled = false
            
            newApptScrollView.isScrollEnabled = false
            
            UIView.animate(withDuration: 0) {
                let translateTransform = CGAffineTransform(translationX: 0, y: -250)
                self.servicesLabel.transform = translateTransform
                self.servicesCollectionView.transform = translateTransform
                self.barbersLabel.transform = translateTransform
                self.barbersCollectionView.transform = translateTransform
            }
            
            return false
        } else {
            return true
        }
        
    }
    
    func fetchServices() -> [Treatment]  {
        guard let services = selectedShop?.services else { return ResourcesManager.shared.getTreatments(shopId: "001") }
        return services
        
    }
    
    func fetchBarbers() -> [Barber] {
        var count = 0
        var barbers = [Barber]()
        for index in 0..<barbersList.count {
            for index2 in 0..<barbersList[index].treatments.count {
                for index3 in 0..<selectedServices.count {
                    if selectedServices[index3].id == barbersList[index].treatments[index2].id {
                        count += 1
                        if count == selectedServices.count {
                            barbers.append(barbersList[index])
                        }
                    }
                }
            }
            count = 0
        }
        return barbers
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == toCalendarSegueID {
            let destination = segue.destination as! BarberCalendarMonthViewController
        }
    }
    
}

//MARK: Collection View Extension
extension CustomerNewAppointmentViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == shopsCollectionViewTag {
            return shopsList.count
        } else if collectionView.tag == servicesCollectionViewTag {
            return servicesList.count
        } else {
            return barberList.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.servicesCollectionView {
            
            let cellService = collectionView.dequeueReusableCell(withReuseIdentifier: serviceCellID, for: indexPath) as! OptionCollectionViewCell
            
            //            cellService.backgroundColor = .white
            cellService.optionView.optionImage.image = UIImage(named: "Beard")
            cellService.optionView.optionName.text = servicesList[indexPath.item]?.name
            cellService.optionView.optionDescription.text = servicesList[indexPath.item]?.description
            
            return cellService
            
        } else if collectionView == self.barbersCollectionView {
            
            let cellBarber = collectionView.dequeueReusableCell(withReuseIdentifier: barberCellID, for: indexPath) as! OptionCollectionViewCell
            
            //            cellBarber.backgroundColor = .red UIImage(named: "OldSchoolBeard")
            cellBarber.optionView.optionImage.image = UIImage(named: barberList[indexPath.item]?.image ?? "OldSchoolBeard")
            cellBarber.optionView.optionName.text = barberList[indexPath.item]?.name
            cellBarber.optionView.optionDescription.isHidden = true
            
            return cellBarber
            
        } else {
            
            let cellShop = collectionView.dequeueReusableCell(withReuseIdentifier: shopCellID, for: indexPath) as! OptionCollectionViewCell
            
                cellShop.optionView.optionImage.image = UIImage(named: "Mirror")
                cellShop.optionView.optionName.text = shopsList[indexPath.item]?.shopName
                cellShop.optionView.optionDescription.isHidden = true
            
            return cellShop
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        servicesCollectionView.allowsMultipleSelection = true
        
        if let cell = collectionView.cellForItem(at: indexPath) as? OptionCollectionViewCell {
            
            if (collectionView.tag == servicesCollectionViewTag) {
                cell.optionView.selected()
                
                if selectedServices.isEmpty {
                    selectedServices = [servicesList[indexPath.item]!]
                    print(indexPath.item)
                } else {
                    selectedServices.append(servicesList[indexPath.item]!)
                    print(indexPath.item)
                    
                }
                
                for index2 in 0..<barberList.count {
                    barbersCollectionView.deselectItem(at: IndexPath(item: index2, section: indexPath.section), animated: false)
                    (barbersCollectionView.cellForItem(at: IndexPath(item: index2, section: indexPath.section)) as? OptionCollectionViewCell)?.optionView.deselected()
                }
                
                self.barberList = fetchBarbers()
                
                if barberList.isEmpty {
                    barbersLabel.isHidden = true
                    barbersCollectionView.reloadData()
                    barbersCollectionView.isHidden = true
                    
                } else {
                    barbersLabel.isHidden = false
                    barbersCollectionView.reloadData()
                    barbersCollectionView.isHidden = false
                }
                
                nextButtonToggle()
                
                print("Selected services count: \(selectedServices.count)")
                
            } else if (collectionView.tag == barbersCollectionViewTag) {
                
                if (cell.optionView.isSelected) {
                    cell.optionView.deselected()
                    selectedBarber = nil
                } else {
                    cell.optionView.selected()
                    selectedBarber = barberList[indexPath.item]
                }
                
            } else if collectionView.tag == shopsCollectionViewTag {
                
                if (cell.optionView.isSelected) {
                    cell.optionView.deselected()
                    selectedShop = nil
                    
                } else {
                    cell.optionView.selected()
                    selectedShop = shopsList[indexPath.item]
                }
                
                self.servicesList = fetchServices()
                
                if selectedShop == nil {
                    servicesLabel.isHidden = true
                    servicesCollectionView.reloadData()
                    servicesCollectionView.isHidden = true
                    
                    barbersLabel.isHidden = true
                    barbersCollectionView.reloadData()
                    barbersCollectionView.isHidden = true
                    
                } else {
                    servicesLabel.isHidden = false
                    servicesCollectionView.reloadData()
                    servicesCollectionView.isHidden = false
                    
                    barbersLabel.isHidden = false
                    barbersCollectionView.reloadData()
                    barbersCollectionView.isHidden = false
                }
                
                nextButtonToggle()
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) as? OptionCollectionViewCell {
            cell.optionView.deselected()
            
            if collectionView.tag == shopsCollectionViewTag {
                for index in 0..<servicesList.count {
                    servicesCollectionView.deselectItem(at: IndexPath(item: index, section: indexPath.section), animated: false)
                    (servicesCollectionView.cellForItem(at: IndexPath(item: index, section: indexPath.section)) as? OptionCollectionViewCell)?.optionView.deselected()
                }
                for index2 in 0..<barberList.count {
                    barbersCollectionView.deselectItem(at: IndexPath(item: index2, section: indexPath.section), animated: false)
                    (barbersCollectionView.cellForItem(at: IndexPath(item: index2, section: indexPath.section)) as? OptionCollectionViewCell)?.optionView.deselected()
                }
                self.barberList = []
                self.selectedServices = []
                barbersLabel.isHidden = true
                barbersCollectionView.reloadData()
                barbersCollectionView.isHidden = true
            }
            
            if collectionView.tag == servicesCollectionViewTag {
                
                for index2 in 0..<barberList.count {
                    barbersCollectionView.deselectItem(at: IndexPath(item: index2, section: indexPath.section), animated: false)
                    (barbersCollectionView.cellForItem(at: IndexPath(item: index2, section: indexPath.section)) as? OptionCollectionViewCell)?.optionView.deselected()
                }
                
                for index in 0 ..< selectedServices.count {
                    if selectedServices[index].name == cell.optionView.optionName.text {
                        selectedServices.remove(at: index)
                        self.barberList = fetchBarbers()
                        barbersCollectionView.reloadData()
                        
                        if barberList.isEmpty {
                            barbersLabel.isHidden = true
                            barbersCollectionView.reloadData()
                            barbersCollectionView.isHidden = true
                            
                        } else {
                            barbersLabel.isHidden = false
                            barbersCollectionView.reloadData()
                            barbersCollectionView.isHidden = false
                        }
                        
                        nextButtonToggle()
                        
                        print(index)
                        break
                    }
                }
                
                print(indexPath.item)
                print("Selected services count: \(selectedServices.count)")
                
            }

            nextButtonToggle()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == self.servicesCollectionView {
            
            return CGSize(width: optionWidth, height: optionHeight + 100)
            
        } else {
            
            return CGSize(width: optionWidth, height: optionHeight + 10)
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 35
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 40)
    }
}
