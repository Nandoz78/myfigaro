//
//  ViewController.swift
//  MyFigaro
//
//  Created by fernando rosa on 12/02/2020.
//  Copyright © 2020 fernando rosa. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class ViewController: UIViewController {
    
    @IBOutlet weak var btnBarber:UIButton!
    @IBOutlet weak var btnClient:UIButton!
    @IBOutlet weak var lblWelcomeMessage: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.lblWelcomeMessage.text = "Hello,\nnice to meet you!"
        
        self.addBorder()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
         
        let email = UserManager.shared.user.email
        let password = UserManager.shared.user.password
        let userType = UserManager.shared.user.type
        
        if(email != "" && password != ""){
            
            self.showSpinner(onView: self.view)
            var msg:String = ""
            Auth.auth().signIn(withEmail: email, password: password) { [weak self] authResult, error in
                if let error = error {
                  print(error.localizedDescription)
                  msg = error.localizedDescription
                  self!.showErrorAlert(title:"Log In", msg:msg)
                  self?.removeSpinner()
                  return
                }
                print("logged!")
                DataBaseManager.shared.verifyUser(user: UserManager.shared.user)
                self?.removeSpinner()
                self!.continueWithApp(userType: userType)
            }
        }
        
         
    }
    
    func continueWithApp(userType: UserType){
        
        if(userType == .barber){
            let storyBoard: UIStoryboard = UIStoryboard(name: "Barber", bundle: nil)
            let barberTabBarController = storyBoard.instantiateInitialViewController()
            barberTabBarController?.modalPresentationStyle = .fullScreen
            self.present(barberTabBarController as! UIViewController, animated: true, completion: nil)
        } else {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Client", bundle: nil)
            let clientTabBarController = storyBoard.instantiateInitialViewController()
            clientTabBarController?.modalPresentationStyle = .fullScreen
            self.present(clientTabBarController as! UIViewController, animated: true, completion: nil)
        }
    }
    
    func showErrorAlert(title:String, msg:String) {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { _ in
            
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func addBorder(){
        self.btnBarber.layer.borderColor = UIColor.gray.cgColor
        self.btnBarber.layer.cornerRadius = 10.0
        self.btnBarber.layer.borderWidth = 1.0
        
        
        self.btnClient.layer.borderColor = UIColor.gray.cgColor
        self.btnClient.layer.cornerRadius = 10.0
        self.btnClient.layer.borderWidth = 1.0
    }
    
    @IBAction func barberSubscription(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let barberSubscriptionViewController = storyBoard.instantiateViewController(withIdentifier: "BarberSubscription") as! BarberSubscriptionViewController
        barberSubscriptionViewController.modalPresentationStyle = .fullScreen
        self.present(barberSubscriptionViewController, animated: true, completion: nil)
    }
    
    @IBAction func customerSubscription(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let clientSubscriptionViewController = storyBoard.instantiateViewController(withIdentifier: "ClientSubscription") as! ClientSubscriptionViewController
        clientSubscriptionViewController.modalPresentationStyle = .fullScreen
        self.present(clientSubscriptionViewController, animated: true, completion: nil)
    }
    
    var vSpinner: UIView?
    
    func showSpinner(onView : UIView) {
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        let ai = UIActivityIndicatorView.init(style: .large)
        ai.startAnimating()
        ai.center = spinnerView.center
        
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            onView.addSubview(spinnerView)
        }
        
        vSpinner = spinnerView
    }
    
    func removeSpinner() {
        DispatchQueue.main.async {
            self.vSpinner?.removeFromSuperview()
            self.vSpinner = nil
        }
    }
}

