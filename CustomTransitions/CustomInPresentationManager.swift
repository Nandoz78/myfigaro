//
//  CustomInPresentationManager.swift
//  
//
//  Created by fernando rosa on 13/12/2019.
//  Copyright © 2019 fernando rosa. All rights reserved.
//

import UIKit

// Enum to represent the presentation's direction
enum PresentationDirection{
    case left
    case top
    case right
    case bottom
}

class CustomInPresentationManager: NSObject {
    var direction: PresentationDirection = .bottom
}

extension CustomInPresentationManager:UIViewControllerTransitioningDelegate{
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        let presentationController = CustomInPresentationController(presentedViewController: presented, presentingViewController: presenting, direction: direction)
        return presentationController
    }
}
