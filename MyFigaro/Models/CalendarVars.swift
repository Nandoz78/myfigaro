//
//  CalendarVars.swift
//  MyFigaro
//
//  Created by fernando rosa on 26/02/2020.
//  Copyright © 2020 fernando rosa. All rights reserved.
//

import Foundation

// Dates Management
var date = Date()
let calendar = Calendar.current

let day = calendar.component(.day, from: date)
var weekday = calendar.component(.weekday, from: date) - 1
var month = calendar.component(.month, from: date) - 1
var year = calendar.component(.year, from: date)
