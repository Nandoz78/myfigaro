//
//  TimeRange.swift
//  MyFigaro
//
//  Created by fernando rosa on 10/03/2020.
//  Copyright © 2020 fernando rosa. All rights reserved.
//

import Foundation

class TimeRange{
    var stringToDisplay:String
    var date:Date
    var barbers:[Barber]
    
    init(date:Date, stringToDisplay:String, barbers:[Barber]) {
        self.date = date
        self.stringToDisplay = stringToDisplay
        self.barbers = barbers
    }
}
