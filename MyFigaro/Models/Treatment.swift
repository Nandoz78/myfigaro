//
//  Treatment.swift
//  MyFigaro
//
//  Created by fernando rosa on 20/02/2020.
//  Copyright © 2020 fernando rosa. All rights reserved.
//

import Foundation

class Treatment {
    var id:String
    var name:String
    var description:String
    var duration:Int
    var image:String
    
    init() {
        self.id = ""
        self.duration = 0
        self.name = ""
        self.description = ""
        self.image = ""
    }
    
    init(name:String, duration:Int, description:String, image:String) {
        self.id = ""
        self.name = name
        self.description = description
        self.duration = duration
        self.image = image
    }
    
    init(id:String, name:String, duration:Int, description:String) {
        self.id = id
        self.name = name
        self.description = description
        self.duration = duration
        self.image = ""
    }
    
    init(id:String, name:String, duration:Int, description:String, image:String) {
        self.id = id
        self.name = name
        self.description = description
        self.duration = duration
        self.image = image
    }
}
