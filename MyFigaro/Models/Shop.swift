//
//  Shop.swift
//  MyFigaro
//
//  Created by fernando rosa on 17/02/2020.
//  Copyright © 2020 fernando rosa. All rights reserved.
//

import Foundation


class Shop {
    var shopId: String
    var shopName: String
    var ownerName: String
    var address: String
    var country: String
    var district: String
    var city: String
    var phoneNumber: String
    var email: String
    var password: String
    var note: String
    var openingDay:[Day]
    var customers:[Client]
    
    var services:[Treatment]
    var barbers:[Barber]
    var image:String
    
    init() {
        self.shopId = ""
        self.shopName = ""
        self.ownerName = ""
        self.address = ""
        self.phoneNumber = ""
        self.email = ""
        self.password = ""
        self.country = ""
        self.district = ""
        self.city = ""
        self.note = ""
        self.openingDay = [Day]()
        self.customers = [Client]()
        self.services = [Treatment]()
        self.barbers = [Barber]()
        self.image = "TreatmentGenericImage"
    }
    
    init(shopId:String, shopName: String, ownerName:String, address:String, phoneNumber:String, email:String, password:String) {
        self.shopId = shopId
        self.shopName = shopName
        self.ownerName = ownerName
        self.address = address
        self.phoneNumber = phoneNumber
        self.email = email
        self.password = password
        self.country = ""
        self.district = ""
        self.city = ""
        self.note = ""
        self.openingDay = [Day]()
        self.customers = [Client]()
        self.services = [Treatment]()
        self.barbers = [Barber]()
        self.image = "TreatmentGenericImage"
    }
    
    init(shopId:String, shopName: String, ownerName: String, address:String){
        self.shopId = shopId
        self.shopName = shopName
        self.ownerName = ownerName
        self.address = address
        self.phoneNumber = ""
        self.email = ""
        self.password = ""
        self.country = ""
        self.district = ""
        self.city = ""
        self.note = ""
        self.openingDay = [Day]()
        self.customers = [Client]()
        self.services = [Treatment]()
        self.barbers = [Barber]()
        self.image = "TreatmentGenericImage"
    }
}
