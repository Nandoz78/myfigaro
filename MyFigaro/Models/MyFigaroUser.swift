//
//  User.swift
//  MyFigaro
//
//  Created by fernando rosa on 21/02/2020.
//  Copyright © 2020 fernando rosa. All rights reserved.
//

import Foundation

enum UserType:Int {
    case barber = 1
    case customer
    case unknown
}

class MyFigaroUser{
    var name:String
    var email:String
    var password:String
    var type:UserType
    var id:String
    
    init() {
        self.id = ""
        self.name = ""
        self.email = ""
        self.password = ""
        self.type = .unknown
    }
}
