//
//  Notification.swift
//  MyFigaro
//
//  Created by fernando rosa on 26/02/2020.
//  Copyright © 2020 fernando rosa. All rights reserved.
//

import Foundation

class Notification {
    var date:Date
    var message:String
    
    init() {
        self.date = Date()
        self.message = ""
    }
    
    init(date: Date, message: String) {
        self.date = date
        self.message = message
    }
}
