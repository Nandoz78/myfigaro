//
//  Holiday.swift
//  MyFigaro
//
//  Created by fernando rosa on 11/03/2020.
//  Copyright © 2020 fernando rosa. All rights reserved.
//

import Foundation



class Holiday {
    var id:String
    var start:Date
    var end:Date
    var description:String
    var duration:Int
    
    init() {
        self.id = ""
        self.start = Date()
        self.end = Date()
        self.description = ""
        self.duration = 0
    }
    
    init(start:Date, end:Date, description:String) {
        self.id = ""
        self.start = start
        self.end = end
        self.description = description
        
        let calendar = Calendar.current
        let date1 = calendar.startOfDay(for: start)
        let date2 = calendar.startOfDay(for: end)
        let components = calendar.dateComponents([.day], from: date1, to: date2)
        
        self.duration = components.day! + 1
    }
    
    init(id:String, start:Date, end:Date, description:String) {
        self.id = ""
        self.start = start
        self.end = end
        self.description = description
        
        let calendar = Calendar.current
        let date1 = calendar.startOfDay(for: start)
        let date2 = calendar.startOfDay(for: end)
        let components = calendar.dateComponents([.day], from: date1, to: date2)
        
        self.duration = components.day! + 1
    }
    
    func getFrom() -> String{
        let calendar = Calendar.current
        let year = calendar.component(.year, from: self.start)
        let month = calendar.component(.month, from: self.start)
        let day = calendar.component(.day, from: self.start)
      
        return "\(day)/\(month)/\(year)"
    }
    
    func getTo() -> String{
        if(self.duration > 1){
            let calendar = Calendar.current
            let year = calendar.component(.year, from: self.end)
            let month = calendar.component(.month, from: self.end)
            let day = calendar.component(.day, from: self.end)
            
            return "\(day)/\(month)/\(year)"
        } else {
            return ""
        }
    }
    
    func getEditTo() -> String{
        let calendar = Calendar.current
        let year = calendar.component(.year, from: self.start)
        let month = calendar.component(.month, from: self.start)
        let day = calendar.component(.day, from: self.start)
        
        return "\(day)/\(month)/\(year)"
    }

}
