//
//  Day.swift
//  MyFigaro
//
//  Created by fernando rosa on 27/02/2020.
//  Copyright © 2020 fernando rosa. All rights reserved.
//

import Foundation
class Day{
    
    var name:String
    var isClosed:Bool
    var morningStart:String
    var morningEnd:String
    var afternoonStart:String
    var afternoonEnd:String
    
    init(name:String, isClosed:Bool, morningStart:String, morningEnd:String, afternoonStart:String, afternoonEnd:String ) {
        self.name = name
        self.isClosed = isClosed
        self.morningStart = morningStart
        self.morningEnd = morningEnd
        self.afternoonStart = afternoonStart
        self.afternoonEnd = afternoonEnd
    }
}
