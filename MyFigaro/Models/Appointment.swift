//
//  Appointment.swift
//  MyFigaro
//
//  Created by fernando rosa on 17/02/2020.
//  Copyright © 2020 fernando rosa. All rights reserved.
//

import Foundation

enum AppointmentStatus: Int{
    case pending
    case cancelled
    case done
    case unknown
}

class Appointment {
    var id:String
    var customer:Client
    var time:Date
    var status:AppointmentStatus
    var treatments:[Treatment]
    var barber:Barber
    var duration:Int
    
    init() {
        self.id = ""
        self.customer = Client()
        self.time = Date()
        self.status = .pending
        self.treatments = [Treatment]()
        self.barber = Barber()
        self.duration = 0
    }
    
    init(customer:Client, time:Date, treatments:[Treatment], barber:Barber) {
        self.id = ""
        self.customer = customer
        self.time = time
        self.status = .pending
        self.treatments = treatments
        self.barber = barber
        self.duration = 0
        var duration:Int = 0
        for trt in self.treatments {
            duration += trt.duration
        }
    }
    
    func getDuration() -> Int{
        var duration:Int = 0
        for trt in self.treatments {
            duration += trt.duration
        }
        return duration
    }
    
    func getTreatmentsId() -> [String]{
        var list:[String] = [String]()
        for trt in self.treatments {
            list.append(trt.id)
        }
        return list
    }
    
    func getTime() -> String {
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: self.time)
        let minute = calendar.component(.minute, from: self.time)
        let txtHour = hour < 10 ? "0\(hour)" : "\(hour)"
        let txtminute = minute < 10 ? "0\(minute)" : "\(minute)"
        return "\(txtHour):\(txtminute)"
    }
    
    func getStatus() -> String{
        switch self.status {
        case .pending:
            return "pending"
        case .done:
            return "done"
        case .cancelled:
            return "done"
        default:
            return "unknow"
        }
    }
    
    func setStatus(status:String){
        switch status {
        case "pending":
            self.status = .pending
        case "done":
            self.status = .done
        case "cancelled":
            self.status = .cancelled
        default:
            self.status = .unknown
        }
    }
    
}

