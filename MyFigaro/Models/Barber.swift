//
//  Barber.swift
//  MyFigaro
//
//  Created by fernando rosa on 17/02/2020.
//  Copyright © 2020 fernando rosa. All rights reserved.
//

import Foundation

class Barber {
    var id:String
    var name:String
    var funnyDescription:String
    var image:String
    var treatmentsIds:[String]
    var treatments:[Treatment]
    var holidays:[Holiday]
    
    init() {
        self.id = ""
        self.name = ""
        self.funnyDescription = ""
        self.image = ""
        self.treatmentsIds = [String]()
        self.treatments = [Treatment]()
        self.holidays = [Holiday]()
    }
    
    init(name:String, image:String, funnyDescription:String, treatments:[Treatment]) {
        self.id = ""
        self.name = name
        self.funnyDescription = funnyDescription
        self.image = image
        self.treatmentsIds = [String]()
        self.treatments = treatments
        self.holidays = [Holiday]()
        
        for item in treatments {
           self.treatmentsIds.append(item.id)
        }
    }
    
    init(id:String, name:String, image:String, funnyDescription:String, treatments:[Treatment], treatmentsIds:[String]) {
        self.id = id
        self.name = name
        self.funnyDescription = funnyDescription
        self.image = image
        self.treatmentsIds = [String]()
        self.treatments = treatments
        self.holidays = [Holiday]()
        
        for item in treatments {
            self.treatmentsIds.append(item.id)
        }
    }
    
    func updateServices(servicesUpdated:[Treatment]){
        self.treatments.removeAll()
        self.treatmentsIds.removeAll()
        
        for item in servicesUpdated {
            self.treatments.append(item)
            self.treatmentsIds.append(item.id)
        }
    }
    
    func updateHolidays(holidayUpdated:[Holiday]){
        self.holidays.removeAll()
        
        for item in holidayUpdated {
            self.holidays.append(item)
        }
    }
    
}
