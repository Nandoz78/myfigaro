//
//  Client.swift
//  MyFigaro
//
//  Created by fernando rosa on 17/02/2020.
//  Copyright © 2020 fernando rosa. All rights reserved.
//

import Foundation

class Client {
    var id:String
    var name:String
    var surname:String
    var cell:String
    var email:String
    var isActive:Bool
    var shops:[String]
    
    init() {
        self.id = ""
        self.name = ""
        self.surname = ""
        self.cell = ""
        self.email = ""
        self.isActive = false
        self.shops = [String]()
    }
    
    init(name:String, surname:String, cell:String, email:String, isActive:Bool) {
        self.id = ""
        self.name = name
        self.surname = surname
        self.cell = cell
        self.email = email
        self.isActive = isActive
        self.shops = [String]()
    }
    
    init(id:String, name:String, surname:String, cell:String, email:String, isActive:Bool) {
        self.id = id
        self.name = name
        self.surname = surname
        self.cell = cell
        self.email = email
        self.isActive = isActive
        self.shops = [String]()
    }
    
    init(id:String, name:String, surname:String, cell:String, email:String, isActive:Bool, shops:[String]) {
        self.id = id
        self.name = name
        self.surname = surname
        self.cell = cell
        self.email = email
        self.isActive = isActive
        self.shops = shops
    }
    
    func removeShop(idShop:String){
        self.shops = self.shops.filter() { $0 != idShop }
    }
    
    func addShop(idShop:String){
        self.shops.append(idShop)
    }
}
