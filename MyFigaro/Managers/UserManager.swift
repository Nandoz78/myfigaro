//
//  UserManager.swift
//  MyFigaro
//
//  Created by fernando rosa on 21/02/2020.
//  Copyright © 2020 fernando rosa. All rights reserved.
//

import Foundation

class UserManager {

    static let shared = UserManager()
    var user:MyFigaroUser
    var shop:Shop
    var customer:Client
    
    private init(){
        self.user = MyFigaroUser()
        self.shop = Shop()
        self.customer = Client()
    }
    
    func loadUserSettings() -> NSDictionary?{
        let localpath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent("UserSettings.plist")
        if(!FileManager.default.fileExists(atPath: localpath.relativePath)){
            if let path = Bundle.main.path(forResource: "UserSettings", ofType: "plist") {
                var nsDictionary:[String : Any] = NSDictionary(contentsOfFile: path) as! [String : Any]
                do  {
                let data = try PropertyListSerialization.data(fromPropertyList: nsDictionary, format: PropertyListSerialization.PropertyListFormat.binary, options: 0)
                    do {
                     try data.write(to: localpath , options: .atomic)
                        print("Successfully write")
                        return nsDictionary as! NSDictionary
                    }catch (let err){
                        print(err.localizedDescription)
                        return nil
                    }
                }catch (let err){
                    print(err.localizedDescription)
                    return nil
                }
            }
        }
        else
        {
            return NSDictionary(contentsOfFile: localpath.relativePath)!
        }
        return nil
    }
    
    func saveUserSettings() -> Bool{
        let localpath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent("UserSettings.plist")
        
         var nsDictionary:[String : Any] = NSDictionary(contentsOfFile: localpath.relativePath) as! [String : Any]
         nsDictionary["id"] = self.user.id
         nsDictionary["name"] = self.user.name
         nsDictionary["email"] =  self.user.email
         nsDictionary["password"] =  self.user.password
         nsDictionary["type"] = self.user.type.rawValue
         nsDictionary["last_access"] = "last_access"
        
        
        if(self.shop.shopId != ""){
            var shopDictionary:[String : Any] = [String : Any]()
                    shopDictionary["name"] = self.shop.shopName
                    shopDictionary["address"] = self.shop.address
                    shopDictionary["city"] = self.shop.city
                    shopDictionary["country"] = self.shop.country
                    shopDictionary["district"] = self.shop.district
                    shopDictionary["owner"] = self.shop.ownerName
                    shopDictionary["note"] = self.shop.note
                    shopDictionary["id"] = self.shop.shopId
                    shopDictionary["phone"] = self.shop.phoneNumber
                    shopDictionary["email"] = self.shop.email
                   
                    nsDictionary["shop"] = shopDictionary
        }
        
        if(self.customer.id != ""){
            var customerDictionary:[String : Any] = [String : Any]()
                    customerDictionary["id"] = self.customer.id
                    customerDictionary["cell"] = self.customer.cell
                    customerDictionary["email"] = self.customer.email
                    customerDictionary["surname"] = self.customer.surname
                    customerDictionary["name"] = self.customer.name
                    customerDictionary["active"] = self.customer.isActive
                   
                    nsDictionary["customer"] = customerDictionary
        }
        
           guard FileManager.default.fileExists(atPath: localpath.relativePath) else {
               return false
           }
           do  {
           let data = try PropertyListSerialization.data(fromPropertyList: nsDictionary, format: PropertyListSerialization.PropertyListFormat.binary, options: 0)
               do {
                try data.write(to: localpath , options: .atomic)
                   print("Successfully write")
                   return true
               }catch (let err){
                   print(err.localizedDescription)
                   return false
               }
           }catch (let err){
               print(err.localizedDescription)
               return false
           }
        
    }
}
