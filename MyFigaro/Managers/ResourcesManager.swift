//
//  ResourceManager.swift
//  MyFigaro
//
//  Created by fernando rosa on 20/02/2020.
//  Copyright © 2020 fernando rosa. All rights reserved.
//

import Foundation
import UIKit

class ResourcesManager {

    static let shared = ResourcesManager()
    
    var selectedDay = String()
    
    private init(){}
    
    func getAppointmentsList(date:Date, shopId:String) -> [Appointment]{
        
        var appointmentList = [Appointment]()
        return appointmentList
    }
    
    func getShopById(shopId:String) -> Shop {
        let shop = Shop(shopId: shopId, shopName: "HeadRush", ownerName: "Mario De Luca", address: "Corso Protopisani 70", phoneNumber: "+393930000000", email: "mariodeluca@gmail.com", password: "")
        
        shop.openingDay = [Day(name: "MONDAY", isClosed: true, morningStart: "00:00", morningEnd: "00:00", afternoonStart: "00:00", afternoonEnd: "00:00"),Day(name: "TUESDAY", isClosed: false, morningStart: "08:00", morningEnd: "13:30", afternoonStart: "15:00", afternoonEnd: "19:00"),Day(name: "WEDNESDAY", isClosed: false, morningStart: "08:00", morningEnd: "13:30", afternoonStart: "15:00", afternoonEnd: "19:00"),Day(name: "THURSDAY", isClosed: false, morningStart: "08:00", morningEnd: "13:30", afternoonStart: "15:00", afternoonEnd: "19:00"),Day(name: "FRIDAY", isClosed: false, morningStart: "08:00", morningEnd: "13:30", afternoonStart: "15:00", afternoonEnd: "19:00"),Day(name: "SATURDAY", isClosed: false, morningStart: "08:00", morningEnd: "13:30", afternoonStart: "13:30", afternoonEnd: "21:00"),Day(name: "SUNDAY", isClosed: true, morningStart: "00:00", morningEnd: "00:00", afternoonStart: "00:00", afternoonEnd: "00:00")]
        
        return shop
    }
    
    func getTreatments(shopId:String) -> [Treatment] {
        let treatmentsList = [Treatment(id: "000", name: "Hair cut", duration: 30, description: "Hair cut"),
                              Treatment(id: "001", name: "Shave", duration: 10, description: "Shave"),
                              Treatment(id: "002", name: "Shampo", duration: 10, description: "Shampo")]
        return treatmentsList
    }
    
    func getBarbers(shopId:String) -> [Barber] {
        let barbersList = [Barber(name: "Mario", image: "", funnyDescription: "Sono bello e ballo", treatments: [Treatment]()),
                           Barber(name: "Santo", image: "", funnyDescription: "Pigiamino?", treatments: [Treatment]()),
                           Barber(name: "Albert", image: "", funnyDescription: "Dads called!", treatments: [Treatment]()),
                           Barber(name: "Luca", image: "", funnyDescription: "Non ve lo do il codice", treatments: [Treatment]())]
        
        return barbersList
    }
    
    func getNotifications(shopId:String) -> [Notification] {
        let notificationsList = [Notification(date: date, message: "Oggi tutto ok"), Notification(date: date, message: "Pure oggi"), Notification(date: date, message: "Non saprei oggi, mi fa male la schiena"), Notification(date: date, message: "Oggi chiuso"), Notification(date: date, message: "Che palle")]
        return notificationsList
    }
    
    var clientsList:[Client] = [Client]()
//    func getClients(shopId:String){
//        DataBaseManager.shared.getShopClients(shopId: shopId)
//    }
    
    
    func getDate(date:String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = Locale.current
        return dateFormatter.date(from: date)!
    }
    
    func getAppointmentByTimeSlot(date:Date, start:String) -> Appointment {
        var appointment = Appointment()
        let appointmentList = getAppointmentsList(date: date, shopId: "000")
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = Locale.current
        for item in appointmentList {
            if dateFormatter.string(from: item.time) == start {
                appointment = item
            }
        }
        return appointment
    }
    
    func rearrangeAppointments(appointmentsList: [Appointment]) -> [Appointment] {
        let sortedAppointments = appointmentsList.sorted{ $0.time < $1.time }
        for item in sortedAppointments {
            print(item.time)
        }
        return sortedAppointments
    }
    
    func getCustomerById(customerId:String) -> Client {
        return Client(id: customerId, name: "Luca", surname: "Palmese", cell: "+393333333333", email: "lu.palmese@gmail.com", isActive: true, shops: ["s85lmH5eMbis1KDQOBSF"])
    }
    
    // IMAGES
    func loadBasicImages() -> [String]?{
        let localpath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent("BasicImages.plist")
        if(!FileManager.default.fileExists(atPath: localpath.relativePath)){
            if let path = Bundle.main.path(forResource: "BasicImages", ofType: "plist") {
                let nsDictionary:[String : String] = NSDictionary(contentsOfFile: path) as! [String : String]
                do  {
                let data = try PropertyListSerialization.data(fromPropertyList: nsDictionary, format: PropertyListSerialization.PropertyListFormat.binary, options: 0)
                    do {
                     try data.write(to: localpath , options: .atomic)
                        print("Successfully write")
                        var images:[String] = [String]()
                        for item in nsDictionary {
                            images.append(item.value)
                        }
                        return images
                    }catch (let err){
                        print(err.localizedDescription)
                        return nil
                    }
                }catch (let err){
                    print(err.localizedDescription)
                    return nil
                }
            }
        }
        else
        {
            let nsDictionary:[String : String] = NSDictionary(contentsOfFile: localpath.relativePath) as! [String : String]
            var images:[String] = [String]()
            for item in nsDictionary {
                images.append(item.value)
            }
            return images
        }
        return nil
    }
    
    public func store(image: UIImage, forKey key: String) {
        if let pngRepresentation = image.pngData() {
            UserDefaults.standard.set(pngRepresentation, forKey: key)
        }
    }
    
    public func retrieveImage(forKey key: String) -> UIImage? {
        if let imageData = UserDefaults.standard.object(forKey: key) as? Data,
            let image = UIImage(data: imageData) {
            return image
        }
        return nil
    }
    
}
