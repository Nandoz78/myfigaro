//
//  StringsManager.swift
//  MyFigaro
//
//  Created by fernando rosa on 20/02/2020.
//  Copyright © 2020 fernando rosa. All rights reserved.
//

import Foundation

enum Language:Int{
    case italian
    case english
}

class StringsManager {

    static let shared = StringsManager()
    private init(){}
    
    var WELCOME_MESSAGE = ""
    
    func setLanguage(lang:Language){
        switch lang {
        case .italian:
            self.WELCOME_MESSAGE = "Ciao,\nè un piacere conoscerti!"
        default:
            self.WELCOME_MESSAGE = "Hello,\nnice to meet you!"
        }
    }
}
