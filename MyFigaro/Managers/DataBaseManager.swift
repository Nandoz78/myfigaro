//
//  DataBaseManager.swift
//  MyFigaro
//
//  Created by fernando rosa on 03/03/2020.
//  Copyright © 2020 fernando rosa. All rights reserved.
//

import Foundation
import Firebase
import FirebaseFirestore

class DataBaseManager {

    static let shared = DataBaseManager()

    private init(){
        
    }
    
    public func verifyUser(user:MyFigaroUser){
        let db = Firestore.firestore()
        if(user.id != ""){
            let docRef = db.collection("Users").document(user.id)
            docRef.getDocument { (document, error) in
                if let document = document, document.exists {
                    let dataDescription = document.data().map(String.init(describing:)) ?? "nil"
                    print("Document data: \(dataDescription)")
                } else {
                    print("Document does not exist")
                }
            }
        }else{
            db.collection("Users").whereField("Email", isEqualTo:user.email)
                .getDocuments() { (querySnapshot, err) in
                if let err = err {
                    print("Error getting documents: \(err)")
                } else {
                    for document in querySnapshot!.documents {
                        print("\(document.documentID) => \(document.data())")
                         user.id = document.documentID
                    }
                }
            }
        }
        
    }
    
    public func addUser(name:String, email:String, status:String, type:String, uid:String) {
        let db = Firestore.firestore()
        var ref: DocumentReference? = nil
        ref = db.collection("Users").addDocument(data: [
            "Name": name,
            "Email": email,
            "Status": status,
            "Type": type,
            "UID": uid
        ]) { err in
            if let err = err {
                print("Error adding document: \(err)")
            } else {
                print("Document added with ID: \(ref!.documentID)")
                UserManager.shared.user.id = ref!.documentID
                UserManager.shared.saveUserSettings()
            }
        }
    }
    
    public func addShop(shop:Shop, completion: @escaping (String)->()) {
        let db = Firestore.firestore()
        var ref: DocumentReference? = nil
        ref = db.collection("Shop").addDocument(data: [
            "Active": "true",
            "Address": shop.address as! String,
            "City": shop.city as! String,
            "Country": shop.country as! String,
            "District": shop.district as! String,
            "Name": shop.shopName as! String,
            "Note": shop.note as! String,
            "Owner": shop.ownerName as! String,
            "Operators": nil,
            "Subscription": nil,
            "SubscriptionExpiryDate": nil,
            "Timetable": nil,
        ]) { err in
            if let err = err {
                print("Error adding document: \(err)")
                completion("Error adding document: \(err)")
            } else {
                print("Document added with ID: \(ref!.documentID)")
                UserManager.shared.shop.shopId = ref!.documentID
                self.initShopServices(shopId: ref!.documentID, completion: { res in
                    completion(res)
                })
                UserManager.shared.saveUserSettings()
                
            }
        }
    }
    
    func getShopClients(shopId:String, completion: @escaping ([Client])->()) {
        let db = Firestore.firestore()
        var list:[Client] = [Client]()
        db.collection("Client").whereField("Shops", arrayContains:shopId)
            .getDocuments() { (querySnapshot, err) in
                if let err = err {
                    print("Error getting documents: \(err)")
                } else {
                    
                    for document in querySnapshot!.documents {
                        print("\(document.documentID) => \(document.data())")
                        
                        let item:Client = Client(id:document.documentID, name: (document.data())["Name"] as! String, surname: (document.data())["Surname"] as! String, cell: (document.data() )["Cell"] as! String, email: (document.data())["Email"] as! String, isActive: (document.data())["Active"] as! Bool, shops: (document.data()["Shops"] as! [String]))
                        list.append(item)
                    }
                }
                completion(list)
        }
    }
    
    func addCustomerToShop(shopId:String, customer:Client, completion: @escaping (String)->()) {
        let db = Firestore.firestore()
        var ref: DocumentReference? = nil
       ref = db.collection("Client").addDocument(data: [
           "Active": customer.isActive,
           "Name": customer.name,
           "Surname": customer.surname,
           "Email": customer.email,
           "Cell": customer.cell,
           "Shops": [shopId]
       ]) { err in
           if let err = err {
               print("Error adding document: \(err)")
               completion("Error adding document: \(err.localizedDescription)")
           } else {
               print("Document added with ID: \(ref!.documentID)")
               completion(ref!.documentID)
           }
       }
    }
    
    func updateCustomer(customer:Client, completion: @escaping (String)->()) {
        let db = Firestore.firestore()
        let selectedCustomer = db.collection("Client").document(customer.id)

       selectedCustomer.updateData([
            "Active": customer.isActive,
            "Name": customer.name,
            "Surname": customer.surname,
            "Email": customer.email,
            "Cell": customer.cell
        ]) { err in
            if let err = err {
                print("Error writing document: \(err)")
                completion("Error writing document: \(err.localizedDescription)")
            } else {
                print("Document successfully written!")
                completion("Document successfully written!")
            }
        }
    }
    
    func deleteCustomer(shopId:String, customer:Client, completion: @escaping (String)->()) {
        let db = Firestore.firestore()
        if(customer.shops.count > 0){
            customer.removeShop(idShop: shopId)
            let selectedCustomer = db.collection("Client").document(customer.id)
            selectedCustomer.updateData([
                "Shops": customer.shops
            ]) { err in
                if let err = err {
                    print("Error writing document: \(err)")
                    completion("Error writing document: \(err.localizedDescription)")
                } else {
                    print("Document successfully written!")
                    completion("Document successfully written!")
                }
            }
        }else{
            db.collection("Client").document(customer.id).delete() { err in
                if let err = err {
                    print("Error removing document: \(err)")
                    completion("Error removing document: \(err.localizedDescription)")
                } else {
                    print("Document successfully removed!")
                    completion("Document successfully removed!")
                }
            }
        }
    }
    
    // Services
    func getShopServices(shopId:String, completion: @escaping ([Treatment])->()) {
        let db = Firestore.firestore()
        var list:[Treatment] = [Treatment]()
        db.collection("Treatment").whereField("Shop", isEqualTo: shopId)
            .getDocuments() { (querySnapshot, err) in
                if let err = err {
                    print("Error getting documents: \(err)")
                } else {
                    
                    for document in querySnapshot!.documents {
                        print("\(document.documentID) => \(document.data())")
                        
                        let item:Treatment = Treatment(id: document.documentID, name: (document.data())["Name"] as! String, duration: (document.data())["Duration"] as! Int, description: (document.data())["Description"] as! String, image: (document.data())["Image"] as! String)
                        
                        list.append(item)
                    }
                }
                completion(list)
        }
    }
    
    func initShopServices(shopId:String, completion: @escaping (String)->()) {
        let db = Firestore.firestore()
        
        db.collection("BasicTreatments").getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
                completion("Error getting documents: \(err)")
            } else {
                for document in querySnapshot!.documents {
                    print("\(document.documentID) => \(document.data())")
                    var ref: DocumentReference? = nil
                    ref = db.collection("Treatment").addDocument(data: [
                        "Description": (document.data())["Description"] as! String,
                        "Duration": (document.data())["Duration"] as! Int,
                        "Image": (document.data())["Image"] as! String,
                        "Name": (document.data())["Name"] as! String,
                        "Shop": shopId
                    ]) { err in
                        if let err = err {
                            print("Error adding document: \(err)")
                            completion("Error adding document: \(err)")
                        } else {
                            print("Document added with ID: \(ref!.documentID)")
                        }
                    }
                    
                }
                completion("Shop created with success!")
            }
        }
       
    }
    
    func getShopBarbers(shopId:String, completion: @escaping ([Barber])->()) {
        let db = Firestore.firestore()
        var list:[Barber] = [Barber]()
        db.collection("Operator").whereField("Shop", isEqualTo: shopId)
            .getDocuments() { (querySnapshot, err) in
                if let err = err {
                    print("Error getting documents: \(err)")
                } else {
                    
                    for document in querySnapshot!.documents {
                        print("\(document.documentID) => \(document.data())")
                        
                        let treatmentList = (document.data())["Services"] as! [String]
                        var barberTreatments = [Treatment]()
                        for item in treatmentList {
                            let selectedTreatment:[Treatment] = UserManager.shared.shop.services.filter({$0.id == item})
                            barberTreatments.append(selectedTreatment[0])
                        }
                        
                        let item:Barber = Barber(id:document.documentID, name: (document.data())["Name"] as! String, image: "", funnyDescription: (document.data())["FunnyDescription"] as! String, treatments:barberTreatments,treatmentsIds: (document.data())["Services"] as! [String])
                        
                        list.append(item)
                    }
                }
                completion(list)
        }
    }
    
    // BARBER
    
    public func addBarber(idShop:String, barber:Barber, completion: @escaping (String)->()) {
        let db = Firestore.firestore()
        var ref: DocumentReference? = nil
        ref = db.collection("Operator").addDocument(data: [
            "Name": barber.name,
            "FunnyDescription": barber.funnyDescription,
            "Image": barber.image,
            "Services": barber.treatmentsIds,
            "Shop": idShop
        ]) { err in
            if let err = err {
                print("Error adding document: \(err)")
                completion("Error adding document: \(err)")
            } else {
                print("Document added with ID: \(ref!.documentID)")
                completion(ref!.documentID)
            }
        }
    }
    
     func deleteBarber( barber:Barber, completion: @escaping (String)->()) {
           let db = Firestore.firestore()
           db.collection("Operator").document(barber.id).delete() { err in
               if let err = err {
                   print("Error removing document: \(err)")
                   completion("Error removing document: \(err.localizedDescription)")
               } else {
                   print("Document successfully removed!")
                   completion("Document successfully removed!")
               }
           }
       }
    
    func updateBarber(barber:Barber, completion: @escaping (String)->()) {
           let db = Firestore.firestore()
           let selectedBarber = db.collection("Operator").document(barber.id)

          selectedBarber.updateData([
               "Name": barber.name,
               "FunnyDescription": barber.funnyDescription,
               "Image": barber.image,
               "Services": barber.treatmentsIds
           ]) { err in
               if let err = err {
                   print("Error writing document: \(err)")
                   completion("Error writing document: \(err.localizedDescription)")
               } else {
                   print("Document successfully written!")
                   completion("Document successfully written!")
               }
           }
       }
    
    // SERVICES
    
    public func addService(idShop:String, service:Treatment, completion: @escaping (String)->()) {
        let db = Firestore.firestore()
        var ref: DocumentReference? = nil
        ref = db.collection("Treatment").addDocument(data: [
            "Description": service.description,
            "Duration": service.duration,
            "Image": service.image,
            "Name": service.name,
            "Shop": idShop
        ]) { err in
            if let err = err {
                print("Error adding document: \(err)")
                completion("Error adding document: \(err)")
            } else {
                print("Document added with ID: \(ref!.documentID)")
                completion(ref!.documentID)
            }
        }
    }
    
    func deleteService( service:Treatment, completion: @escaping (String)->()) {
        let db = Firestore.firestore()
        db.collection("Treatment").document(service.id).delete() { err in
            if let err = err {
                print("Error removing document: \(err)")
                completion("Error removing document: \(err.localizedDescription)")
            } else {
                print("Document successfully removed!")
                completion("Document successfully removed!")
            }
        }
    }
    
    func updateService(service:Treatment, completion: @escaping (String)->()) {
        let db = Firestore.firestore()
        let selectedService = db.collection("Treatment").document(service.id)

       selectedService.updateData([
            "Description": service.description,
            "Duration": service.duration,
            "Image": service.image,
            "Name": service.name
        ]) { err in
            if let err = err {
                print("Error writing document: \(err)")
                completion("Error writing document: \(err.localizedDescription)")
            } else {
                print("Document successfully written!")
                completion("Document successfully written!")
            }
        }
    }
    
    // APPOINTMENTS
    
    public func addAppointment(idShop:String, appointment:Appointment, completion: @escaping (String)->()) {
        let db = Firestore.firestore()
        var ref: DocumentReference? = nil
        ref = db.collection("Appointment").addDocument(data: [
            "Client": appointment.customer.id,
            "Operator": appointment.barber.id,
            "Duration": appointment.duration,
            "Shop": idShop,
            "Status": appointment.getStatus(),
            "Time": appointment.time,
            "Treatments": appointment.getTreatmentsId()
        ]) { err in
            if let err = err {
                print("Error adding document: \(err)")
                completion("Error adding document: \(err)")
            } else {
                print("Document added with ID: \(ref!.documentID)")
                completion(ref!.documentID)
            }
        }
    }
    
    func deleteAppointment( appointment:Appointment, completion: @escaping (String)->()) {
        let db = Firestore.firestore()
        db.collection("Appointment").document(appointment.id).delete() { err in
            if let err = err {
                print("Error removing document: \(err)")
                completion("Error removing document: \(err.localizedDescription)")
            } else {
                print("Document successfully removed!")
                completion("Document successfully removed!")
            }
        }
    }
    
    // Services
    func getShopAppointments(shopId:String, dateRef:Date, completion: @escaping ([Appointment])->()) {
        let db = Firestore.firestore()
        var list:[Appointment] = [Appointment]()
        db.collection("Appointment").whereField("Shop", isEqualTo: shopId)
            .getDocuments() { (querySnapshot, err) in
                if let err = err {
                    print("Error getting documents: \(err)")
                } else {
                    
                    for document in querySnapshot!.documents {
                        
                        let date  = (document.data()["Time"] as! Timestamp).dateValue()
                        print(date)
                        print(dateRef)
                        
                        let calendar = Calendar.current
                        let appyear = calendar.component(.year, from: date)
                        let appmonth = calendar.component(.month, from: date)
                        let appday = calendar.component(.day, from: date)
                        
                        let refyear = calendar.component(.year, from: dateRef)
                        let refmonth = calendar.component(.month, from: dateRef)
                        let refday = calendar.component(.day, from: dateRef)
                        
                        if(appyear == refyear && appmonth == refmonth && appday == refday){
                           print("\(document.documentID) => \(document.data())")
                                                   
                           let item:Appointment = Appointment()
                           item.id = document.documentID
                           
                           
                           let customer:[Client] = UserManager.shared.shop.customers.filter({$0.id == (document.data())["Client"] as! String})
                           item.customer = customer[0]
                           
                           let barber:[Barber] = UserManager.shared.shop.barbers.filter({$0.id == (document.data())["Operator"] as! String})
                           item.barber = barber[0]
                           
                          
                           item.setStatus(status: (document.data()["Status"] as! String))
                           item.duration = (document.data())["Duration"] as! Int
                           
                           
                           for trtId in ((document.data())["Treatments"] as! [String]){
                               let service:[Treatment] = UserManager.shared.shop.services.filter({$0.id == trtId})
                               item.treatments.append(service[0])
                           }
                            
                           item.time = date
                           
                           list.append(item)
                        }
   
                    }
                }
                completion(list)
        }
    }
    
    // CUSTOMER
    
    func addCustomer(customer:Client, completion: @escaping (String)->()) {
        let db = Firestore.firestore()
        var ref: DocumentReference? = nil
       ref = db.collection("Client").addDocument(data: [
           "Active": customer.isActive,
           "Name": customer.name,
           "Surname": customer.surname,
           "Email": customer.email,
           "Cell": customer.cell,
           "Shops": []
       ]) { err in
           if let err = err {
               print("Error adding document: \(err)")
               completion("Error adding document: \(err.localizedDescription)")
           } else {
               print("Document added with ID: \(ref!.documentID)")
              UserManager.shared.customer.id = ref!.documentID
              UserManager.shared.saveUserSettings()
              completion("Customer created with success!")
           }
       }
    }
   
    func getShopInfoForCustomerSubscription(shopId:String, completion: @escaping (Shop?)->()) {
        let db = Firestore.firestore()
        let docRef = db.collection("Shop").document(shopId)

        docRef.getDocument { (document, error) in
            var shop:Shop?
            if let document = document, document.exists {
                let dataDescription = document.data().map(String.init(describing:)) ?? "nil"
                print("Document data: \(dataDescription)")
                let name = document.data()!["Name"] as! String
                let owner = document.data()!["Owner"] as! String
                let address = document.data()!["Address"] as! String
                shop = Shop(shopId: document.documentID, shopName: name, ownerName: owner, address: address)
            } else {
                print("Document does not exist")
                
            }
            completion(shop)
        }
     }
    
    func joinShop(shopId:String, customer:Client, completion: @escaping (String)->()) {
           let db = Firestore.firestore()
           let selectedCustomer = db.collection("Client").document(customer.id)
          customer.addShop(idShop: shopId)
          selectedCustomer.updateData([
               "Active": customer.isActive,
               "Name": customer.name,
               "Surname": customer.surname,
               "Email": customer.email,
               "Cell": customer.cell,
               "Shops": customer.shops
           ]) { err in
               if let err = err {
                   print("Error writing document: \(err)")
                   completion("Error writing document: \(err.localizedDescription)")
               } else {
                   print("Document successfully written!")
                   completion("Joined!")
               }
           }
    }
    
    func getClientShops(customer:Client, completion: @escaping ([Shop]?)->()) {
        let db = Firestore.firestore()
        var list:[Shop]?
        let docRef = db.collection("Client").document(customer.id)

        docRef.getDocument { (document, error) in
            if let document = document, document.exists {
                let lists = document.data()!["Shops"] as! [String]
                list = [Shop]()
                for shopId in lists {
                    let shopDocRef = db.collection("Shop").document(shopId)
                    shopDocRef.getDocument { (document, error) in
                        if let document = document, document.exists {
                             let name = document.data()!["Name"] as! String
                             let owner = document.data()!["Owner"] as! String
                             let address = document.data()!["Address"] as! String
                             let shop = Shop(shopId: document.documentID, shopName: name, ownerName: owner, address: address)
                            list?.append(shop)
                        } else {
                            print("Document does not exist")
                        }
                        completion(list)
                    }
                    
                }
                completion(list)
                
            } else {
                print("Document does not exist")
                completion(list)
            }
        }
    }
    
    public func getCustomerInfo(client:Client, completion: @escaping (Bool)->()) {
        let db = Firestore.firestore()
        db.collection("Client").whereField("Email", isEqualTo:client.email)
            .getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
                completion(false)
            } else {
                if(querySnapshot!.documents.count == 1){
                    for document in querySnapshot!.documents {
                        print("\(document.documentID) => \(document.data())")
                        client.id = document.documentID
                        client.name = (document.data()["Name"] as! String)
                        client.surname = (document.data()["Surname"] as! String)
                        client.isActive = (document.data()["Active"] as! Bool)
                        client.cell = (document.data()["Cell"] as! String)
                    }
                    completion(true)
                }else{
                    completion(false)
                }
                
            }
        }
    }
    
    // Holidays
    
    func getBarberHolidays(barber:Barber, completion: @escaping ([Holiday]?)->()) {
        let db = Firestore.firestore()
        var list:[Holiday]?
        db.collection("Holiday").whereField("Operator", isEqualTo: barber.id)
                 .getDocuments() { (querySnapshot, err) in
                     if let err = err {
                         print("Error getting documents: \(err)")
                         completion(list)
                     } else {
                        list = [Holiday]()
                        for document in querySnapshot!.documents {
                            let startDate  = (document.data()["Start"] as! Timestamp).dateValue()
                            let endDate  = (document.data()["End"] as! Timestamp).dateValue()

                            let item:Holiday = Holiday()
                            item.id = document.documentID
                            item.start = startDate
                            item.end = endDate
                            item.description = (document.data())["Description"] as! String
                            item.duration = (document.data())["Duration"] as! Int
                            
                            list!.append(item)
        
                         }
                        
                        list!.sort {
                            $0.start > $1.start
                        }
                        
                        completion(list)
                     }
                     
             }
    }
}
