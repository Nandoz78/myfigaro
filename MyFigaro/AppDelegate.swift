//
//  AppDelegate.swift
//  MyFigaro
//
//  Created by fernando rosa on 12/02/2020.
//  Copyright © 2020 fernando rosa. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestore


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {



    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        FirebaseApp.configure()
        
        let db = Firestore.firestore()
        print(db)
        
        var dictionary:NSDictionary = UserManager.shared.loadUserSettings()!
        UserManager.shared.user.id = dictionary["id"] as! String
        UserManager.shared.user.name = dictionary["name"] as! String
        UserManager.shared.user.email = dictionary["email"] as! String
        UserManager.shared.user.password = dictionary["password"] as! String
        UserManager.shared.user.type =  UserType(rawValue: dictionary["type"] as! Int)!
        
        if(UserManager.shared.user.type == .customer){
            var customerDictionary:NSDictionary = dictionary["customer"] as! NSDictionary
            UserManager.shared.customer.id = customerDictionary["id"] as! String
            UserManager.shared.customer.cell = customerDictionary["cell"] as! String
            UserManager.shared.customer.email = dictionary["email"] as! String
            UserManager.shared.customer.surname = customerDictionary["surname"] as! String
            UserManager.shared.customer.name = customerDictionary["name"] as! String
            UserManager.shared.customer.isActive = customerDictionary["active"] as! Bool
        }
        
        if(UserManager.shared.user.type == .barber){
             
            var shopDictionary:NSDictionary = dictionary["shop"] as! NSDictionary
            UserManager.shared.shop.shopId = shopDictionary["id"] as! String
            UserManager.shared.shop.shopName = shopDictionary["name"] as! String
            UserManager.shared.shop.address = shopDictionary["address"] as! String
            UserManager.shared.shop.ownerName = shopDictionary["owner"] as! String
            UserManager.shared.shop.country = shopDictionary["country"] as! String
            UserManager.shared.shop.district = shopDictionary["district"] as! String
            UserManager.shared.shop.city = shopDictionary["city"] as! String
            UserManager.shared.shop.note = shopDictionary["note"] as! String
            UserManager.shared.shop.phoneNumber = shopDictionary["phone"] as! String
            UserManager.shared.shop.email = shopDictionary["email"] as! String
            
            UserManager.shared.shop.openingDay = [Day(name: "MONDAY", isClosed: true, morningStart: "00:00", morningEnd: "00:00", afternoonStart: "00:00", afternoonEnd: "00:00"),Day(name: "TUESDAY", isClosed: false, morningStart: "08:00", morningEnd: "13:30", afternoonStart: "15:00", afternoonEnd: "19:00"),Day(name: "WEDNESDAY", isClosed: false, morningStart: "08:00", morningEnd: "13:30", afternoonStart: "15:00", afternoonEnd: "19:00"),Day(name: "THURSDAY", isClosed: false, morningStart: "08:00", morningEnd: "13:30", afternoonStart: "15:00", afternoonEnd: "19:00"),Day(name: "FRIDAY", isClosed: false, morningStart: "08:00", morningEnd: "13:30", afternoonStart: "15:00", afternoonEnd: "19:00"),Day(name: "SATURDAY", isClosed: false, morningStart: "08:00", morningEnd: "13:30", afternoonStart: "13:30", afternoonEnd: "20:00"),Day(name: "SUNDAY", isClosed: true, morningStart: "00:00", morningEnd: "00:00", afternoonStart: "00:00", afternoonEnd: "00:00")]
            
            DataBaseManager.shared.getShopClients(shopId: UserManager.shared.shop.shopId) { listCustomers in
                UserManager.shared.shop.customers = listCustomers
            }
            
            DataBaseManager.shared.getShopServices(shopId: UserManager.shared.shop.shopId) { listServices in
                UserManager.shared.shop.services = listServices
                
                DataBaseManager.shared.getShopBarbers(shopId: UserManager.shared.shop.shopId) { listBarbers in
                    UserManager.shared.shop.barbers = listBarbers
                    
                }
            }
            
            return true
        }else{
            return true
        }
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

