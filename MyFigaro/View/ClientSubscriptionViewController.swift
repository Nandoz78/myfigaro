//
//  ClientSubscriptionViewController.swift
//  MyFigaro
//
//  Created by fernando rosa on 17/02/2020.
//  Copyright © 2020 fernando rosa. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase

class ClientSubscriptionViewController: UIViewController, UITextFieldDelegate{
     @IBOutlet weak var lblMessage: UILabel!
        @IBOutlet weak var lblMessageLogSign: UILabel!
        @IBOutlet weak var lblTerms: UILabel!
        
        @IBOutlet weak var fieldsView: UIView!
        @IBOutlet weak var logView: UIView!
        @IBOutlet weak var buttomView: UIView!
        
        @IBOutlet weak var txtName: UITextField!
        @IBOutlet weak var nameView: UIView!
        @IBOutlet weak var nameImg: UIImageView!
        
        @IBOutlet weak var txtMail: UITextField!
        @IBOutlet weak var mailView: UIView!
        @IBOutlet weak var mailImg: UIImageView!
        
        @IBOutlet weak var txtPassword: UITextField!
        @IBOutlet weak var passwordView: UIView!
        @IBOutlet weak var passwordImg: UIImageView!
        @IBOutlet weak var forgotView: UIView!
        
        @IBOutlet weak var btnSignUp: UIButton!
        @IBOutlet weak var btnFacebook: UIButton!
        
        var checkName:Bool = false
        var checkMail:Bool = false
        var checkPassword:Bool = false
    
        var state:ViewState = .normal
        var type:TypeView = .sign
        
        override func viewDidLoad() {
            super.viewDidLoad()

            self.btnSignUp.layer.cornerRadius = self.btnSignUp.frame.height * 0.5
            self.btnFacebook.layer.cornerRadius = self.btnFacebook.frame.height * 0.5
            
            self.txtName.delegate = self
            self.txtMail.delegate = self
            self.txtPassword.delegate = self
            self.txtName.tag = 1001
            self.txtMail.tag = 1002
            self.txtPassword.tag = 1003
            
            self.txtName.attributedPlaceholder = NSAttributedString(string: "Name",
            attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
            self.txtName.addTarget(self, action: #selector(self.textFieldDidChange(_:)),
                                   for: UIControl.Event.editingChanged)
            self.txtMail.attributedPlaceholder = NSAttributedString(string: "Mail",
            attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
            self.txtMail.addTarget(self, action: #selector(self.textFieldDidChange(_:)),
            for: UIControl.Event.editingChanged)
            self.txtPassword.attributedPlaceholder = NSAttributedString(string: "Password",
            attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
            self.txtPassword.addTarget(self, action: #selector(self.textFieldDidChange(_:)),
            for: UIControl.Event.editingChanged)
            
            self.btnSignUp.alpha = 0.0
            self.disableBtnSignUp()
            
            let recognizer = UITapGestureRecognizer(target: self, action: #selector(handleTap(recognizer:)))
            self.view.addGestureRecognizer(recognizer)
            
            self.forgotView.alpha = 0.0
            
            self.nameView.layer.cornerRadius = 5.0
            self.mailView.layer.cornerRadius = 5.0
            self.passwordView.layer.cornerRadius = 5.0
            
            self.txtName.text = UserManager.shared.user.name
            self.txtMail.text = UserManager.shared.user.email
            self.txtPassword.text = UserManager.shared.user.password
            
        }
    
        func enableBtnSignUp(){
            self.btnSignUp.isEnabled = true
            self.btnSignUp.backgroundColor = UIColor(red: 61.0/255.0, green: 101.0/255.0, blue: 186.0/255.0, alpha: 1.0)
        }
        
        func disableBtnSignUp(){
            self.btnSignUp.isEnabled = false
            self.btnSignUp.backgroundColor = UIColor(red: 61.0/255.0, green: 101.0/255.0, blue: 186.0/255.0, alpha: 0.2)
        }

        @IBAction func signUp(_ sender: Any) {
            if(self.state == .writing){
                self.state = .normal
                UIView.animate(withDuration: 0.3, animations: {
                    self.fieldsView.frame.origin.y += self.btnSignUp.frame.height * 2.5
                })
                UIView.animate(withDuration: 0.2, animations: {
                    self.btnSignUp.alpha = 0.0
                    self.buttomView.alpha = 1.0
                })
                self.view.endEditing(true)
            }
            
            var msg:String = ""
            switch self.type {
            case .log:
                print("login")
                let email = self.txtMail.text!
                let password = self.txtPassword.text!
                self.showSpinner(onView: self.view)
                Auth.auth().signIn(withEmail: email, password: password) { [weak self] authResult, error in
                    if let error = error {
                      print(error.localizedDescription)
                      msg = error.localizedDescription
                      self?.removeSpinner()
                      self!.showErrorAlert(title:"Log In", msg:msg)
                      return
                    }
                    print("logged!")
                    self?.removeSpinner()
                    self!.continueWithApp()
                }
            case .sign:
                print("sign")
                let name = self.txtName.text!
                let email = self.txtMail.text!
                let password = self.txtPassword.text!
                self.showSpinner(onView: self.view)
                Auth.auth().createUser(withEmail: email, password: password) { authResult, error in
                    guard let user = authResult?.user, error == nil else {
                        print(error!.localizedDescription)
                        msg = error!.localizedDescription
                        self.removeSpinner()
                        self.showErrorAlert(title:"Sign In", msg:msg)
                        return
                    }
                    print("\(user.email!) created!")
                    DataBaseManager.shared.addUser(name: name, email: email, status: "active", type: "customer", uid: "")
                    self.removeSpinner()
                    self.continueWithApp()
                }
            default:
                print()
            }
        }
    
    func continueWithApp(){
        
        if(self.type == .log){
            UserManager.shared.user.name = self.txtName.text!
            UserManager.shared.user.email = self.txtMail.text!
            UserManager.shared.user.password = self.txtPassword.text!
            UserManager.shared.user.type = .customer
        }else{
            UserManager.shared.user.email = self.txtMail.text!
            UserManager.shared.user.password = self.txtPassword.text!
            UserManager.shared.user.type = .customer
        }
        UserManager.shared.saveUserSettings()
        
         let storyBoard: UIStoryboard = UIStoryboard(name: "Client", bundle: nil)
           let clientTabBarController = storyBoard.instantiateInitialViewController()
           clientTabBarController?.modalPresentationStyle = .fullScreen
           self.present(clientTabBarController as! UIViewController, animated: true, completion: nil)
    }
    
    func showErrorAlert(title:String, msg:String) {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { _ in
            
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
        @IBAction func logSignBtn(_ sender: Any) {
            if(self.type == .sign){
                self.type = .log
            }else{
                self.type = .sign
            }
            switch self.type {
            case .sign:
                self.lblMessage.text = "Create your customer account"
                self.lblMessageLogSign.text = "Have an account?"
                (sender as! UIButton).setTitle("Log in", for: .normal)
                UIView.animate(withDuration: 0.2, animations: {
                    self.nameView.alpha = 1.0
                    self.lblTerms.alpha = 1.0
                    self.forgotView.alpha = 0.0
                    self.logView.frame.origin.y += self.logView.frame.height * 0.5
                })
            case .log:
                self.lblMessage.text = "Log into your MyFigaro account"
                self.lblMessageLogSign.text = "Need an account?"
                (sender as! UIButton).setTitle("Sign up", for: .normal)
                UIView.animate(withDuration: 0.2, animations: {
                    self.nameView.alpha = 0.0
                    self.lblTerms.alpha = 0.0
                    self.forgotView.alpha = 1.0
                    self.logView.frame.origin.y -= self.logView.frame.height * 0.5
                })
            default:
                print()
            }
        }
        
        @IBAction func close(_ sender: Any) {
            self.dismiss(animated: true, completion: nil)
        }
        
        func textFieldShouldReturn(_ textField: UITextField) -> Bool {
           let nextTag = textField.tag + 1

           if let nextResponder = self.view.viewWithTag(nextTag) {
               nextResponder.becomeFirstResponder()
           } else {
               textField.resignFirstResponder()
            if(self.state == .writing){
                self.state = .normal
                UIView.animate(withDuration: 0.3, animations: {
                    self.fieldsView.frame.origin.y += self.btnSignUp.frame.height * 2.5
                })
                UIView.animate(withDuration: 0.2, animations: {
                    self.btnSignUp.alpha = 0.0
                    self.buttomView.alpha = 1.0
                })
                self.view.endEditing(true)
            }
           }

           return true
       }
    
        func textFieldDidBeginEditing(_ textField: UITextField) {
            
            if(self.state == .normal){
                self.state = .writing
                UIView.animate(withDuration: 0.3, animations: {
                    self.fieldsView.frame.origin.y -= self.btnSignUp.frame.height * 2.5
                })
                UIView.animate(withDuration: 0.2, animations: {
                    self.btnSignUp.alpha = 1.0
                    self.buttomView.alpha = 0.0
                })
            }
            switch textField.tag {
            case 1001:
                self.nameView.layer.borderColor = UIColor(red: 61.0/255.0, green: 101.0/255.0, blue: 186.0/255.0, alpha: 1.0).cgColor
                self.nameView.layer.borderWidth = 2.0
            case 1002:
                self.mailView.layer.borderColor = UIColor(red: 61.0/255.0, green: 101.0/255.0, blue: 186.0/255.0, alpha: 1.0).cgColor
                self.mailView.layer.borderWidth = 2.0
            case 1003:
                self.passwordView.layer.borderColor = UIColor(red: 61.0/255.0, green: 101.0/255.0, blue: 186.0/255.0, alpha: 1.0).cgColor
                self.passwordView.layer.borderWidth = 2.0
            default:
                print()
            }
            
        }
        
        func textFieldDidEndEditing(_ textField: UITextField) {
            switch textField.tag {
            case 1001:
                self.nameView.layer.borderWidth = 0.0
            case 1002:
                self.mailView.layer.borderWidth = 0.0
            case 1003:
                self.passwordView.layer.borderWidth = 0.0
            default:
                print()
            }
            self.checkTxtFields()
        }
        
        @objc func handleTap(recognizer:UITapGestureRecognizer) {
            if(self.state == .writing){
                self.state = .normal
                UIView.animate(withDuration: 0.3, animations: {
                    self.fieldsView.frame.origin.y += self.btnSignUp.frame.height * 2.5
                })
                UIView.animate(withDuration: 0.2, animations: {
                    self.btnSignUp.alpha = 0.0
                    self.buttomView.alpha = 1.0
                })
                self.view.endEditing(true)
            }
            
        }
    
        @objc func textFieldDidChange(_ textField: UITextField) {
            self.checkTxtFields()
        }
        
        func checkTxtFields(){
            if(self.txtName.text != ""){
                if(self.txtName.text!.count >= 2){
                    self.nameImg.tintColor = .green
                    self.checkName = true
                }else{
                    self.nameImg.tintColor = .red
                    self.checkName = false
                }
            }else{
                self.nameImg.tintColor = .white
                self.checkName = false
            }
            
            if(self.txtMail.text != ""){
                if(self.txtMail.text!.isValidEmail){
                    self.mailImg.tintColor = .green
                    self.checkMail = true
                }else{
                    self.mailImg.tintColor = .red
                    self.checkMail = false
                }
            }else{
                self.mailImg.tintColor = .white
                self.checkMail = false
            }
            
            if(self.txtPassword.text != ""){
                if(self.txtPassword.text!.count >= 8){
                    self.passwordImg.tintColor = .green
                    self.checkPassword = true
                }else{
                    self.passwordImg.tintColor = .red
                    self.checkPassword = false
                }
            }else{
                self.passwordImg.tintColor = .white
                self.checkPassword = false
            }
            
            
            if(self.type == .sign){
                if(self.checkName && self.checkMail && self.checkPassword){
                    self.enableBtnSignUp()
                }else{
                    self.disableBtnSignUp()
                }
            } else if(self.type == .log){
                if(self.checkMail && self.checkPassword){
                    self.enableBtnSignUp()
                }else{
                    self.disableBtnSignUp()
                }
            }
        }
    
    var vSpinner: UIView?
    
    func showSpinner(onView : UIView) {
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        let ai = UIActivityIndicatorView.init(style: .large)
        ai.startAnimating()
        ai.center = spinnerView.center
        
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            onView.addSubview(spinnerView)
        }
        
        vSpinner = spinnerView
    }
    
    func removeSpinner() {
        DispatchQueue.main.async {
            self.vSpinner?.removeFromSuperview()
            self.vSpinner = nil
        }
    }
}
