//
//  EditDayViewController.swift
//  MyFigaro
//
//  Created by fernando rosa on 28/02/2020.
//  Copyright © 2020 fernando rosa. All rights reserved.
//

import UIKit

protocol EditDayViewControllerDelegate {
    func changed(day:Day)
}

class EditDayViewController: UIViewController,UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    
    var selectedDay:Day!
    var delegate: EditDayViewControllerDelegate?
    
    @IBOutlet weak var lblClosingDay: UILabel!
    @IBOutlet weak var switchClosed: UISwitch!
    @IBOutlet weak var lblMorning: UILabel!
    @IBOutlet weak var txtStartMorning: UITextField!
    @IBOutlet weak var txtEndMorning: UITextField!
    @IBOutlet weak var lblOpenFromMorning: UILabel!
    @IBOutlet weak var lblToMorning: UILabel!
    @IBOutlet weak var lblAfternoon: UILabel!
    @IBOutlet weak var txtStartAfternoon: UITextField!
    @IBOutlet weak var txtEndAfternoon: UITextField!
    @IBOutlet weak var lblOpenFromAfternoon: UILabel!
    @IBOutlet weak var lblToAfternoon: UILabel!
    
    @IBOutlet weak var closingView: UIView!
    @IBOutlet weak var morningView: UIView!
    @IBOutlet weak var afternoonView: UIView!
    
    @IBOutlet weak var hoursPicker: UIPickerView!
    var hoursPickerIsVisible:Bool = false
    var selectedTxtField:UITextField!
    
    let hourFirst:[String] = ["0","1","2"]
    let hourSecond:[String] = ["0","1","2","3","4","5","6","7","8","9"]
    let minuteFirst:[String] = ["0","1","2","3","4","5"]
    let minuteSecond:[String] = ["0","1","2","3","4","5","6","7","8","9"]
    
    var selectedValue:String!
    var comma:String = ":"
    
    @IBOutlet weak var btnSave: UIBarButtonItem!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = self.selectedDay.name.uppercased()
        
        self.switchClosed.isOn = self.selectedDay.isClosed
        self.txtStartMorning.text = self.selectedDay.morningStart
        self.txtEndMorning.text = self.selectedDay.morningEnd
        self.txtStartAfternoon.text = self.selectedDay.afternoonStart
        self.txtEndAfternoon.text = self.selectedDay.afternoonEnd
        
        self.txtStartMorning.delegate = self
        self.txtEndMorning.delegate = self
        self.txtStartAfternoon.delegate = self
        self.txtEndAfternoon.delegate = self
        
        self.txtStartMorning.inputView = self.hoursPicker
        self.txtEndMorning.inputView = self.hoursPicker
        self.txtStartAfternoon.inputView = self.hoursPicker
        self.txtEndAfternoon.inputView = self.hoursPicker
        
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let button = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneAction))
        button.tintColor = UIColor(red: 229.0/255.0, green: 73.0/255.0, blue: 44.0/255.0, alpha: 1.0)
        toolBar.setItems([button], animated: true)
        toolBar.isUserInteractionEnabled = true
        self.txtStartMorning.inputAccessoryView = toolBar
        self.txtEndMorning.inputAccessoryView = toolBar
        self.txtStartAfternoon.inputAccessoryView = toolBar
        self.txtEndAfternoon.inputAccessoryView = toolBar
        
        self.closingView.layer.cornerRadius = 5.0
        self.morningView.layer.cornerRadius = 5.0
        self.afternoonView.layer.cornerRadius = 5.0
        
        self.hoursPicker.delegate = self
        self.hoursPicker.dataSource = self
        
        self.hoursPicker.frame.origin.y += (self.hoursPicker.frame.height + 30.0)
        self.hoursPicker.backgroundColor?.withAlphaComponent(0.0)
        
        if(self.selectedDay.isClosed)
        {
            self.afternoonView.frame.origin.y -= (self.afternoonView.frame.height + 8.0)
            self.afternoonView.alpha = 0.0
            self.morningView.frame.origin.y -= (self.morningView.frame.height + 8.0)
            self.afternoonView.alpha = 0.0
        }
        
        self.btnSave.isEnabled = false
    }

    @objc func doneAction() {
        self.selectedTxtField.layer.borderWidth = 0.0
        self.selectedTxtField.endEditing(true)
        self.selectedTxtField = nil
        if(self.hoursPickerIsVisible){
          self.hoursPickerIsVisible = false
          UIView.animate(withDuration: 0.3, animations: {
              self.hoursPicker.frame.origin.y += (self.hoursPicker.frame.height + 30.0)
          })
       }
       self.checkData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }
    
    
    @IBAction func save(_ sender: Any) {
        self.selectedDay.isClosed = self.switchClosed.isOn
        if(self.selectedDay.isClosed){
            self.selectedDay.morningStart = "00:00"
            self.selectedDay.morningEnd = "00:00"
            self.selectedDay.afternoonStart = "00:00"
            self.selectedDay.afternoonEnd = "00:00"
        } else {
            self.selectedDay.morningStart = self.txtStartMorning.text!
            self.selectedDay.morningEnd = self.txtEndMorning.text!
            self.selectedDay.afternoonStart = self.txtStartAfternoon.text!
            self.selectedDay.afternoonEnd = self.txtEndAfternoon.text!
        }
        self.changed(day: self.selectedDay)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.selectedTxtField = textField
        self.selectedTxtField.layer.borderColor = UIColor(red: 229.0/255.0, green: 73.0/255.0, blue: 44.0/255.0, alpha: 1.0).cgColor
        self.selectedTxtField.layer.borderWidth = 2.0
        
        
         let values = self.selectedTxtField.text!.split(separator: ":")
         let first = String(String(values[0]).first!)
         let second = String(String(values[0]).last!)
         let third = String(String(values[1]).first!)
         let fourth = String(String(values[1]).last!)
        
         self.hoursPicker.selectRow(Int(first)!, inComponent: 0, animated: false)
         self.hoursPicker.selectRow(Int(second)!, inComponent: 1, animated: false)
         self.hoursPicker.selectRow(Int(third)!, inComponent: 3, animated: false)
         self.hoursPicker.selectRow(Int(fourth)!, inComponent: 4, animated: false)
        
        
        if(!self.hoursPickerIsVisible){
            self.hoursPickerIsVisible = true
            UIView.animate(withDuration: 0.3, animations: {
                self.hoursPicker.frame.origin.y -= (self.hoursPicker.frame.height + 30.0)
            })
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.selectedTxtField.layer.borderWidth = 0.0
        self.selectedTxtField = nil
        if(self.hoursPickerIsVisible){
            self.hoursPickerIsVisible = false
            UIView.animate(withDuration: 0.3, animations: {
                self.hoursPicker.frame.origin.y += (self.hoursPicker.frame.height + 30.0)
            })
        }
        self.checkData()
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 5
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        switch component {
        case 0:
            return self.hourFirst.count
        case 1:
            return self.hourSecond.count
        case 2:
            return 1
        case 3:
            return self.minuteFirst.count
        case 4:
            return self.minuteSecond.count
        default:
            return 1
        }
        
    }
     
     func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        switch component {
        case 0:
            return self.hourFirst[row]
        case 1:
            return self.hourSecond[row]
        case 2:
            return self.comma
        case 3:
            return self.minuteFirst[row]
        case 4:
            return self.minuteSecond[row]
        default:
            return ""
        }
     }
     
     func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
         
         let first = self.hoursPicker.selectedRow(inComponent: 0)
         let second = self.hoursPicker.selectedRow(inComponent: 1)
         let third = self.hoursPicker.selectedRow(inComponent: 3)
         let fourth = self.hoursPicker.selectedRow(inComponent: 4)
         let textToDisplay = "\(first)\(second)\(self.comma)\(third)\(fourth)"
        self.selectedTxtField.text = textToDisplay
        print(textToDisplay)
     }
    
    @IBAction func switchChanged(_ sender: Any) {
        if ((sender as! UISwitch).isOn){
            UIView.animate(withDuration: 0.2, animations: {
                self.afternoonView.frame.origin.y -= (self.afternoonView.frame.height + 8.0)
                self.afternoonView.alpha = 0.0
            }, completion: { (finished: Bool) in
                UIView.animate(withDuration: 0.2, animations: {
                   self.morningView.frame.origin.y -= (self.morningView.frame.height + 8.0)
                   self.morningView.alpha = 0.0
               })
            })
        } else {
            UIView.animate(withDuration: 0.2, animations: {
                self.morningView.frame.origin.y += (self.morningView.frame.height + 8.0)
                self.morningView.alpha = 1.0
            }, completion: { (finished: Bool) in
                UIView.animate(withDuration: 0.2, animations: {
                   self.afternoonView.frame.origin.y += (self.afternoonView.frame.height + 8.0)
                   self.afternoonView.alpha = 1.0
               })
            })
        }
        self.checkData()
    }
    
    func changed(day:Day){
        self.delegate?.changed(day: day)
    }
    
    func checkData(){
        if((self.selectedDay.isClosed != self.switchClosed.isOn) ||
            (self.selectedDay.morningStart != self.txtStartMorning.text) ||
            (self.selectedDay.morningEnd != self.txtEndMorning.text) ||
            (self.selectedDay.afternoonStart != self.txtStartAfternoon.text) ||
            (self.selectedDay.afternoonEnd != self.txtEndAfternoon.text)){
            self.btnSave.isEnabled = true
        }else{
            self.btnSave.isEnabled = false
        }
    }

}
