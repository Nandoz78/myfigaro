//
//  ChangePasswordViewController.swift
//  MyFigaro
//
//  Created by fernando rosa on 27/02/2020.
//  Copyright © 2020 fernando rosa. All rights reserved.
//

import UIKit

class ChangePasswordViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var fieldsScrolling: UIScrollView!
    
    @IBOutlet weak var currentView: UIView!
    @IBOutlet weak var txtCurrent: UITextField!
    
    @IBOutlet weak var newView: UIView!
    @IBOutlet weak var txtNew: UITextField!
    
    @IBOutlet weak var confirmView: UIView!
    @IBOutlet weak var txtConfirm: UITextField!
    
    @IBOutlet weak var btnSave: UIButton!
    
    var activeField: UITextField?
    var deltaHeight:CGFloat = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        btnSave.layer.cornerRadius = 10
        btnSave.layer.shadowColor = UIColor.gray.cgColor
        btnSave.layer.shadowOffset = CGSize(width: 0, height: 2)
        btnSave.layer.shadowOpacity = 0.5
        btnSave.layer.shadowRadius = 3
        btnSave.layer.masksToBounds = false
        
        self.currentView.layer.cornerRadius = 5.0
        self.newView.layer.cornerRadius = 5.0
        self.confirmView.layer.cornerRadius = 5.0
        
        self.txtCurrent.tag = 1001
        self.txtNew.tag = 1002
        self.txtConfirm.tag = 1003
        
        self.txtCurrent.delegate = self
        self.txtCurrent.isEnabled = true
        self.txtNew.delegate = self
        self.txtNew.isEnabled = true
        self.txtConfirm.delegate = self
        self.txtConfirm.isEnabled = true
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
            NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(aNotification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    
            NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(aNotification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
            NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    
            NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.activeField = textField
        switch textField.tag {
        case 1001:
            self.currentView.layer.borderColor = UIColor(red: 229.0/255.0, green: 73.0/255.0, blue: 44.0/255.0, alpha: 1.0).cgColor
            self.currentView.layer.borderWidth = 2.0
        case 1002:
            self.newView.layer.borderColor = UIColor(red: 229.0/255.0, green: 73.0/255.0, blue: 44.0/255.0, alpha: 1.0).cgColor
            self.newView.layer.borderWidth = 2.0
        case 1003:
            self.confirmView.layer.borderColor = UIColor(red: 229.0/255.0, green: 73.0/255.0, blue: 44.0/255.0, alpha: 1.0).cgColor
            self.confirmView.layer.borderWidth = 2.0
        default:
            print()
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField.tag {
        case 1001:
            self.currentView.layer.borderWidth = 0.0
        case 1002:
            self.newView.layer.borderWidth = 0.0
        case 1003:
            self.confirmView.layer.borderWidth = 0.0
        default:
            print()
        }
        self.activeField = nil
        //self.checkTxtFields()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTag = textField.tag + 1

        if let nextResponder = self.view.viewWithTag(nextTag) {
            nextResponder.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return true
    }
    
    override func viewWillLayoutSubviews() {
           self.view.layer.cornerRadius = 20.0
       }
    
    @IBAction func cancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    @objc func keyboardWillBeHidden(aNotification: NSNotification) {

         let contentInsets: UIEdgeInsets = .zero

         self.fieldsScrolling.contentInset = contentInsets

         self.fieldsScrolling.scrollIndicatorInsets = contentInsets

    }
    
    @objc func keyboardWillShow(aNotification: NSNotification) {

       var info = aNotification.userInfo!

        let kbSize: CGSize = ((info["UIKeyboardFrameEndUserInfoKey"] as? CGRect)?.size)!

       print("kbSize = \(kbSize)")

        let contentInsets: UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: kbSize.height + 50.0, right: 0.0)

       fieldsScrolling.contentInset = contentInsets

       fieldsScrolling.scrollIndicatorInsets = contentInsets

       var aRect: CGRect = self.view.frame

       aRect.size.height -= (kbSize.height)// + self.deltaHeight)
        
       print("aRect.size.height = \(aRect.size.height)")
       if !aRect.contains(activeField!.frame.origin) {
        self.fieldsScrolling.scrollRectToVisible(activeField!.frame, animated: true)
       }

    }

}
