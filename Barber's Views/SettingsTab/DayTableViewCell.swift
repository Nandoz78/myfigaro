//
//  DayTableViewCell.swift
//  MyFigaro
//
//  Created by fernando rosa on 27/02/2020.
//  Copyright © 2020 fernando rosa. All rights reserved.
//

import UIKit

class DayTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblNameOfTheDay: UILabel!
    @IBOutlet weak var closed: UISwitch!
    @IBOutlet weak var lblMorning: UILabel!
    @IBOutlet weak var lblClosed: UILabel!
    @IBOutlet weak var lblAfternoon: UILabel!
    
    @IBOutlet weak var startMorning: UITextField!
    @IBOutlet weak var endMorning: UITextField!
    @IBOutlet weak var startAfternoon: UITextField!
    @IBOutlet weak var endAfternoon: UITextField!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    

}
