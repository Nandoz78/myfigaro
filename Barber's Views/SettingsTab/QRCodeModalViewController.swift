//
//  QRCodeModalViewController.swift
//  MyFigaro
//
//  Created by Luca Palmese on 24/02/2020.
//  Copyright © 2020 fernando rosa. All rights reserved.
//

import UIKit

class QRCodeModalViewController: UIViewController {
    
    var codeToDisplay:String!
    
    // QR Code Outlet
    @IBOutlet weak var qrCode: UIImageView!
    
    // Code Outlet
    @IBOutlet weak var code: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.code.text = UserManager.shared.shop.shopId
        self.createQrCode()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillLayoutSubviews() {
        self.view.layer.cornerRadius = 20.0
    }
    
    func createQrCode(){
        if let stringCode = self.code.text {
            let data = stringCode.data(using: .ascii, allowLossyConversion: false)
            let filter = CIFilter(name: "CIQRCodeGenerator")
            filter?.setValue(data, forKey: "InputMessage")
            let ciImage = filter?.outputImage
            let transform = CGAffineTransform(scaleX: 10, y: 10)
            let transformImage = ciImage?.transformed(by: transform)
            let image = UIImage(ciImage: transformImage!)
            self.qrCode.image = image
        }
    }
    @IBAction func shareCode(_ sender: Any) {
        let items = ["Connect to my shop with MyFigaro app, this is my code: \(self.codeToDisplay!)"]
        let ac = UIActivityViewController(activityItems: items, applicationActivities: nil)
        present(ac, animated: true)
    }
}
