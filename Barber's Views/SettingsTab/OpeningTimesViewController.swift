//
//  OpeningTimesViewController.swift
//  MyFigaro
//
//  Created by fernando rosa on 27/02/2020.
//  Copyright © 2020 fernando rosa. All rights reserved.
//

import UIKit

class OpeningTimesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, EditDayViewControllerDelegate {
    
    var selectedShopOpeningDay:[Day]!
    var selectedDayToEdit:Day!
    
    @IBOutlet weak var weekTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
 
        self.weekTable.delegate = self
        self.weekTable.dataSource = self
        
         self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillLayoutSubviews() {
           self.view.layer.cornerRadius = 20.0
       }
    
   
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "dayCell") as! DayTableViewCell
        cell.lblNameOfTheDay.text = self.selectedShopOpeningDay[indexPath.row].name
        cell.closed.isOn = self.selectedShopOpeningDay[indexPath.row].isClosed
        cell.startMorning.text = self.selectedShopOpeningDay[indexPath.row].morningStart
        cell.endMorning.text = self.selectedShopOpeningDay[indexPath.row].morningEnd
        cell.startAfternoon.text = self.selectedShopOpeningDay[indexPath.row].afternoonStart
        cell.endAfternoon.text = self.selectedShopOpeningDay[indexPath.row].afternoonEnd
        
        return cell
    }
       
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return self.selectedShopOpeningDay.count
   }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedDayToEdit = self.selectedShopOpeningDay[indexPath.row]
        performSegue(withIdentifier: "goToEditView", sender: self)
        print(indexPath.row)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "goToEditView"){
            let nextVC = segue.destination as! EditDayViewController
            nextVC.selectedDay = self.selectedDayToEdit
            nextVC.delegate = self
        }
        
    }
    
    func changed(day: Day) {
        for item in selectedShopOpeningDay{
            if(item.name == day.name){
                item.isClosed = day.isClosed
                item.morningStart = day.morningStart
                item.morningEnd = day.morningEnd
                item.afternoonStart = day.afternoonStart
                item.afternoonEnd = day.afternoonEnd
            }
        }
        self.weekTable.reloadData()
    }
}
