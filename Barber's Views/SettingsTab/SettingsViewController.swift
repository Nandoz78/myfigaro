//
//  SettingsViewController.swift
//  MyFigaro
//
//  Created by Luca Palmese on 24/02/2020.
//  Copyright © 2020 fernando rosa. All rights reserved.
//

import UIKit




class SettingsViewController: UIViewController, UITextFieldDelegate {
    
    lazy var slideInTransitionDelegate = CustomInPresentationManager()
    
    // Outlets
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    @IBOutlet weak var editButton: UIBarButtonItem!
    @IBOutlet weak var shopImage: UIImageView!
    @IBOutlet weak var addPictureButtonBorder: UIView!
    @IBOutlet weak var addPictureButton: UIButton!
    
    
    @IBOutlet weak var scrollView: UIScrollView!
    var activeField: UITextField?
    
    @IBOutlet weak var logOutButton: UIButton!
    @IBOutlet weak var deleteAccountButton: UIButton!
    
    //Shop name field
    @IBOutlet weak var shopNameView: UIView!
    @IBOutlet weak var shopName: UITextField!
    
    //Owner field
    @IBOutlet weak var ownerView: UIView!
    @IBOutlet weak var ownerName: UITextField!
    
    // Address field
    @IBOutlet weak var addressView: UIView!
    @IBOutlet weak var address: UITextField!
    
    // Phone number field
    @IBOutlet weak var phonenumberView: UIView!
    @IBOutlet weak var phoneNumber: UITextField!
    
    // Email field
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var email: UITextField!
    
    // Password field
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var btnChangePassword: UIButton!
    
    @IBOutlet weak var btnOpeningTime: UIButton!
    @IBOutlet weak var openingTimeView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.shopName.text = UserManager.shared.shop.shopName
        self.ownerName.text = UserManager.shared.shop.ownerName
        self.address.text = UserManager.shared.shop.address
        self.email.text = UserManager.shared.shop.email
        self.phoneNumber.text = UserManager.shared.shop.phoneNumber
        self.navigationItem.title = UserManager.shared.shop.shopName
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
    
         
        // Set fields tags and delegates
        shopName.tag = 1000
        shopName.delegate = self
        ownerName.tag = 1001
        ownerName.delegate = self
        address.tag = 1002
        address.delegate = self
        phoneNumber.tag = 1003
        phoneNumber.delegate = self
        email.tag = 1004
        email.delegate = self
         
        // Set view border
        shopNameView.layer.cornerRadius = 5.0
        ownerView.layer.cornerRadius = 5.0
        addressView.layer.cornerRadius = 5.0
        phonenumberView.layer.cornerRadius = 5.0
        emailView.layer.cornerRadius = 5.0
        passwordView.layer.cornerRadius = 5.0
        openingTimeView.layer.cornerRadius = 5.0
        btnOpeningTime.layer.cornerRadius = 5.0
        btnChangePassword.layer.cornerRadius = 5.0
        
        // Disables cancelButton
        self.cancelButton.isEnabled = false
        
        shopName.textColor = .lightGray
        ownerName.textColor = .lightGray
        address.textColor = .lightGray
        phoneNumber.textColor = .lightGray
        email.textColor = .lightGray
        
        self.btnChangePassword.isEnabled = true
        self.btnOpeningTime.isEnabled = true
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)

    }
    
    @objc func keyboardWillBeHidden(aNotification: NSNotification) {

         let contentInsets: UIEdgeInsets = .zero

         self.scrollView.contentInset = contentInsets

         self.scrollView.scrollIndicatorInsets = contentInsets

    }
    
    @objc func keyboardWillShow(aNotification: NSNotification) {

       var info = aNotification.userInfo!

        let kbSize: CGSize = ((info["UIKeyboardFrameEndUserInfoKey"] as? CGRect)?.size)!

       print("kbSize = \(kbSize)")

        let contentInsets: UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: kbSize.height, right: 0.0)

       scrollView.contentInset = contentInsets

       scrollView.scrollIndicatorInsets = contentInsets

       var aRect: CGRect = self.view.frame

       aRect.size.height -= kbSize.height

       if !aRect.contains(activeField!.frame.origin) {

            self.scrollView.scrollRectToVisible(activeField!.frame, animated: true)

       }

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        shopName.textColor = .lightGray
        ownerName.textColor = .lightGray
        address.textColor = .lightGray
        phoneNumber.textColor = .lightGray
        email.textColor = .lightGray
        addPictureButton.isEnabled = false
        shopName.isEnabled = false
        ownerName.isEnabled = false
        address.isEnabled = false
        phoneNumber.isEnabled = false
        email.isEnabled = false
        logOutButton.isEnabled = true
        logOutButton.alpha = 1
        deleteAccountButton.isEnabled = true
        editButton.isEnabled = true
        self.cancelButton.isEnabled = false
        editButton.title = "Edit"
        
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)

        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
    }
    
    override func viewWillLayoutSubviews() {
        // IMAGE OF THE SHOP
        shopImage.clipsToBounds = false
        shopImage.layer.cornerRadius = 23
        shopImage.layer.shadowColor = UIColor.gray.cgColor
        shopImage.layer.shadowOpacity = 0.5
        shopImage.layer.shadowOffset = CGSize(width: 0, height: 3)
        shopImage.layer.shadowRadius = 3
        // LOG OUT BUTTON
        logOutButton.layer.cornerRadius = 10
        logOutButton.layer.shadowColor = UIColor.gray.cgColor
        logOutButton.layer.shadowOffset = CGSize(width: 0, height: 2)
        logOutButton.layer.shadowOpacity = 0.5
        logOutButton.layer.shadowRadius = 3
        logOutButton.layer.masksToBounds = false
        // addPictureButton Border
        addPictureButtonBorder.layer.cornerRadius = addPictureButtonBorder.frame.height/2
    }
    
    // Cancel Button Action
    @IBAction func cancelButtonTapped(_ sender: UIBarButtonItem) {
        shopName.textColor = .lightGray
        ownerName.textColor = .lightGray
        address.textColor = .lightGray
        phoneNumber.textColor = .lightGray
        email.textColor = .lightGray
        addPictureButton.isEnabled = false
        ownerName.isEnabled = false
        address.isEnabled = false
        phoneNumber.isEnabled = false
        email.isEnabled = false
        logOutButton.isEnabled = true
        logOutButton.alpha = 1
        deleteAccountButton.isEnabled = true
        editButton.isEnabled = true
        self.cancelButton.isEnabled = false
        editButton.title = "Edit"
        
        self.btnChangePassword.isEnabled = true
        self.btnOpeningTime.isEnabled = true
    }
    
    // Edit Button Action
    @IBAction func editButtonTapped(_ sender: UIBarButtonItem) {

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(aNotification:)), name: UIResponder.keyboardWillHideNotification, object: nil)

       NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(aNotification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        
        
        // Updates BarButtonItem
        if editButton.title == "Edit" {
            
            self.btnChangePassword.isEnabled = false
            self.btnOpeningTime.isEnabled = false
            
            self.cancelButton.isEnabled = true
            logOutButton.isEnabled = false
            logOutButton.alpha = 0.5
            deleteAccountButton.isEnabled = false
            editButton.title = "Save"
            editButton.isEnabled = false
            
            // Enables all the addPictureButton and the Textfields to edit
            addPictureButton.isEnabled = true
            shopName.isEnabled = true
            ownerName.isEnabled = true
            address.isEnabled = true
            phoneNumber.isEnabled = true
            email.isEnabled = true
       
            // Sets values for each TextField
            shopName.placeholder = "Insert Full Name"
            ownerName.placeholder = "Insert Full Name"
            address.placeholder = "Insert Address"
            phoneNumber.placeholder = "Insert Phone Number"
            email.placeholder = "Insert E-mail"
           
            // Sets values for each TextField
            shopName.textColor = .black
            ownerName.textColor = .black
            address.textColor = .black
            phoneNumber.textColor = .black
            email.textColor = .black
            
        } else if editButton.title == "Save" {
            
            self.btnChangePassword.isEnabled = true
            self.btnOpeningTime.isEnabled = true
            
            self.cancelButton.isEnabled = false
            logOutButton.isEnabled = true
            logOutButton.alpha = 1
            deleteAccountButton.isEnabled = true
           
            shopName.textColor = .lightGray
            ownerName.textColor = .lightGray
            address.textColor = .lightGray
            phoneNumber.textColor = .lightGray
            email.textColor = .lightGray
       
            addPictureButton.isEnabled = false
            shopName.isEnabled = false
            ownerName.isEnabled = false
            address.isEnabled = false
            phoneNumber.isEnabled = false
            email.isEnabled = false
            editButton.title = "Edit"
            
            UserManager.shared.shop.shopName = self.shopName.text!
            UserManager.shared.shop.address = self.address.text!
            UserManager.shared.shop.city = "City"
            UserManager.shared.shop.country = "Country"
            UserManager.shared.shop.district = "District"
            UserManager.shared.shop.note = "Note"
            UserManager.shared.shop.ownerName = self.ownerName.text!
            UserManager.shared.shop.phoneNumber = self.phoneNumber.text!
            UserManager.shared.shop.email = self.email.text!
            
            if(UserManager.shared.shop.shopId == ""){
                self.showSpinner(onView: self.view)
                DataBaseManager.shared.addShop(shop: UserManager.shared.shop){ res in
                    self.removeSpinner()
                    self.showErrorAlert(title: "Edit Shop", msg: res)
                }
            }else{
                UserManager.shared.saveUserSettings()
            }
            
        }
    }
    
    func showErrorAlert(title:String, msg:String) {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { _ in
            
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    var vSpinner: UIView?
    
    func showSpinner(onView : UIView) {
       let spinnerView = UIView.init(frame: onView.frame)
       spinnerView.backgroundColor = UIColor.init(red: 229.0/255.0, green: 73.0/255.0, blue: 44.0/255.0, alpha: 0.1)
       let ai = UIActivityIndicatorView.init(style: .large)
       ai.color =  UIColor(red: 229.0/255.0, green: 73.0/255.0, blue: 44.0/255.0, alpha: 1.0)
    
       let image = UIImageView(image: UIImage(named: "Barber"))
       image.frame.size = CGSize(width: 60, height: 60)
       image.center = CGPoint(x: spinnerView.center.x, y: spinnerView.center.y - 60.0)
       ai.startAnimating()
       ai.center = spinnerView.center
       
       DispatchQueue.main.async {
           spinnerView.addSubview(ai)
           spinnerView.addSubview(image)
           onView.addSubview(spinnerView)
       }
       
       vSpinner = spinnerView
    }
    
    func removeSpinner() {
        DispatchQueue.main.async {
            self.vSpinner?.removeFromSuperview()
            self.vSpinner = nil
        }
    }
    
    // Add Picture Button Action
    @IBAction func addPictureButtonTapped(_ sender: UIButton) {
        
        let alert = UIAlertController.init(title: "Select Image", message: nil, preferredStyle: .actionSheet)
        
        
        let cameraAction = UIAlertAction(title: "Take from Camera", style: .default) { (_) in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                self.showImagePickerController(sourceType: .camera)
                print("Camera")
            }
        }
        
        
        let photoLibraryAction = UIAlertAction(title: "Choose from Photo Library", style: .default) { (_) in
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                self.showImagePickerController(sourceType: .photoLibrary)
                print("Photo Library")
            }
        }
        
        alert.addAction(cameraAction)
        alert.addAction(photoLibraryAction)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        // POPOVER Action Sheet
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        
        self.present(alert, animated: true)
    }
    
    @IBAction func textFieldEditingChanged(_ sender: UITextField) {
        self.checkData()
    }
    
    @IBAction func editingDidEnd(_ sender: UITextField) {
        ownerName.resignFirstResponder()
        address.resignFirstResponder()
        phoneNumber.resignFirstResponder()
        email.resignFirstResponder()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.activeField = textField
        switch textField.tag {
        case 1000:
            self.shopNameView.layer.borderColor = UIColor(red: 229.0/255.0, green: 73.0/255.0, blue: 44.0/255.0, alpha: 1.0).cgColor
            self.shopNameView.layer.borderWidth = 2.0
        case 1001:
            self.ownerView.layer.borderColor = UIColor(red: 229.0/255.0, green: 73.0/255.0, blue: 44.0/255.0, alpha: 1.0).cgColor
            self.ownerView.layer.borderWidth = 2.0
        case 1002:
            self.addressView.layer.borderColor = UIColor(red: 229.0/255.0, green: 73.0/255.0, blue: 44.0/255.0, alpha: 1.0).cgColor
            self.addressView.layer.borderWidth = 2.0
        case 1003:
            self.phonenumberView.layer.borderColor = UIColor(red: 229.0/255.0, green: 73.0/255.0, blue: 44.0/255.0, alpha: 1.0).cgColor
            self.phonenumberView.layer.borderWidth = 2.0
        case 1004:
              self.emailView.layer.borderColor = UIColor(red: 229.0/255.0, green: 73.0/255.0, blue: 44.0/255.0, alpha: 1.0).cgColor
              self.emailView.layer.borderWidth = 2.0
        case 1005:
              self.passwordView.layer.borderColor = UIColor(red: 229.0/255.0, green: 73.0/255.0, blue: 44.0/255.0, alpha: 1.0).cgColor
              self.passwordView.layer.borderWidth = 2.0
        default:
            print()
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField.tag {
        case 1000:
            self.shopNameView.layer.borderWidth = 0.0
        case 1001:
            self.ownerView.layer.borderWidth = 0.0
        case 1002:
            self.addressView.layer.borderWidth = 0.0
        case 1003:
            self.phonenumberView.layer.borderWidth = 0.0
        case 1004:
            self.emailView.layer.borderWidth = 0.0
        case 1005:
            self.passwordView.layer.borderWidth = 0.0
        default:
            print()
        }
        self.activeField = nil
        self.checkData()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTag = textField.tag + 1

        if let nextResponder = self.view.viewWithTag(nextTag) {
            nextResponder.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        self.checkData()
        return true
    }
     
    func checkData(){
        if((self.shopName.text != UserManager.shared.shop.shopName) ||
            (self.address.text != UserManager.shared.shop.address) ||
            (self.ownerName.text != UserManager.shared.shop.ownerName) ||
            (self.phoneNumber.text != UserManager.shared.shop.phoneNumber) ||
            (self.email.text != UserManager.shared.shop.email)){
            self.editButton.isEnabled = true
        }else{
            self.editButton.isEnabled = false
        }
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @IBAction func codeTap(sender: UIButton) {
       let storyBoard: UIStoryboard = UIStoryboard(name: "Barber", bundle: nil)
       let codeViewController = storyBoard.instantiateViewController(withIdentifier: "codeView") as! QRCodeModalViewController
        codeViewController.transitioningDelegate = self.slideInTransitionDelegate
        self.slideInTransitionDelegate.direction = .bottom
        codeViewController.modalPresentationStyle = .custom
        codeViewController.codeToDisplay = UserManager.shared.shop.shopId
        
       self.present(codeViewController, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "openingTimeView"){
            let otViewController = segue.destination as! OpeningTimesViewController
            otViewController.selectedShopOpeningDay = UserManager.shared.shop.openingDay
            
            NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)

            NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
            
            
            
        }
    }
    
    @IBAction func changePasswordTap(sender: UIButton) {
       let storyBoard: UIStoryboard = UIStoryboard(name: "Barber", bundle: nil)
       let cpViewController = storyBoard.instantiateViewController(withIdentifier: "changePasswordView") as! ChangePasswordViewController
        cpViewController.transitioningDelegate = self.slideInTransitionDelegate
        self.slideInTransitionDelegate.direction = .bottom
        cpViewController.modalPresentationStyle = .custom
        
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)

        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        
       self.present(cpViewController, animated: true, completion: nil)
    }
    
}


// Load ImagePickerController Extension
extension SettingsViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    
    func showImagePickerController(sourceType: UIImagePickerController.SourceType) {
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            
            let imagePicker = UIImagePickerController()

            imagePicker.delegate = self
            imagePicker.sourceType = sourceType
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerController (_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let editedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            shopImage.image = editedImage
            editButton.isEnabled = true
        } else if let originalImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            shopImage.image = originalImage
            editButton.isEnabled = true
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
}
