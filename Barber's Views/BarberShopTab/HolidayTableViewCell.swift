//
//  HolidayTableViewCell.swift
//  MyFigaro
//
//  Created by fernando rosa on 13/03/2020.
//  Copyright © 2020 fernando rosa. All rights reserved.
//

import UIKit

class HolidayTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblBigNumber:UILabel!
    @IBOutlet weak var lblDays:UILabel!
    @IBOutlet weak var lblFrom:UILabel!
    @IBOutlet weak var lblTo:UILabel!
    @IBOutlet weak var lblFromDate:UILabel!
    @IBOutlet weak var lblToDate:UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func initCell(bigNumber:Int, fromDate:String, toDate:String){
        self.lblBigNumber.text = "\(bigNumber)"
        self.lblDays.text = bigNumber > 1 ? "days" : "day"
        if(bigNumber > 1){
            self.lblFrom.isHidden = false
            self.lblFromDate.isHidden = false
            self.lblTo.isHidden = false
            self.lblToDate.isHidden = false
            self.lblFromDate.text = fromDate
            self.lblToDate.text = toDate
        } else {
            self.lblFrom.isHidden = false
            self.lblFrom.text = "On"
            self.lblFromDate.isHidden = false
            self.lblTo.isHidden = true
            self.lblToDate.isHidden = true
            self.lblFromDate.text = fromDate
            self.lblToDate.text = ""
        }
        
    }

}
