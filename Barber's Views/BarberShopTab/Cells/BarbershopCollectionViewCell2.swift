//
//  BarbershopCollectionViewCell2.swift
//  MyFigaro
//
//  Created by Luca Palmese on 21/02/2020.
//  Copyright © 2020 fernando rosa. All rights reserved.
//

import UIKit

class BarbershopCollectionViewCell2: UICollectionViewCell {
    
    // Barber ImageView Outlet
    @IBOutlet weak var barberImage: UIImageView!
    
    // Barber's Full Name Label Outlet
    @IBOutlet weak var barberName: UILabel!
    
    // Barber's Description Label Outlet
    @IBOutlet weak var barberDescription: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
