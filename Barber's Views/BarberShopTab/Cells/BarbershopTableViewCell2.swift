//
//  BarbershopTableViewCell2.swift
//  MyFigaro
//
//  Created by Luca Palmese on 21/02/2020.
//  Copyright © 2020 fernando rosa. All rights reserved.
//

import UIKit



class BarbershopTableViewCell2: UITableViewCell {
    
    var barbersList = [Barber]()
    
    // TableView Cell's Label Outlet
    @IBOutlet weak var titleLabel: UILabel!
    
    // CollectionView Outlet
    @IBOutlet weak var collectionView2: UICollectionView!
    
    // Delegate to select BarbershopCollectionViewCell
    weak var delegate: BarbershopCollectionViewCellSelectionDelegate?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Giving CollectionView the BarbershopTableView as Delegate and DataSource
        collectionView2.delegate = self
        collectionView2.dataSource = self
        collectionView2.tag = 4002
        
        // Initialization code
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func reload(){
        self.collectionView2.reloadData()
    }
}


// BarbershopTableViewCell2 extension to support the CollectionView
extension BarbershopTableViewCell2: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return barbersList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let barbershopCollectionViewCell2 = collectionView.dequeueReusableCell(withReuseIdentifier: "barbershopCollectionViewCell2", for: indexPath) as? BarbershopCollectionViewCell2 else {fatalError("Unable to create a CollectionView cell")}
        
        barbershopCollectionViewCell2.barberImage.clipsToBounds = false
        barbershopCollectionViewCell2.barberImage.layer.cornerRadius = 30
        barbershopCollectionViewCell2.barberImage.layer.shadowColor = UIColor.gray.cgColor
        barbershopCollectionViewCell2.barberImage.layer.shadowOpacity = 0.5
        barbershopCollectionViewCell2.barberImage.layer.shadowOffset = CGSize(width: 0, height: 4)
        barbershopCollectionViewCell2.barberImage.layer.shadowRadius = 3
        barbershopCollectionViewCell2.barberImage.image = UIImage(named: "Barber")
        barbershopCollectionViewCell2.barberName.adjustsFontSizeToFitWidth = true
        barbershopCollectionViewCell2.barberName.minimumScaleFactor = 0.5
        barbershopCollectionViewCell2.barberName.text = barbersList[indexPath.row].name
        barbershopCollectionViewCell2.barberDescription.adjustsFontSizeToFitWidth = true
        barbershopCollectionViewCell2.barberDescription.minimumScaleFactor = 0.5
        barbershopCollectionViewCell2.barberDescription.text = barbersList[indexPath.row].funnyDescription
        
        return barbershopCollectionViewCell2
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let selectedItem = collectionView.cellForItem(at: indexPath) as! BarbershopCollectionViewCell2
        
//        Session.sharedInstance.selectedSong = Song(cover: selectedItem.coverImage.image!, title: selectedItem.trackName.text!, artist: selectedItem.artistName.text!)
        
        self.delegate?.selectedItem(tag: collectionView.tag, indexPath: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 125, height: 165)
    }
    
}
