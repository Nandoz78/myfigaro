//
//  BarbershopCollectionViewCell.swift
//  MyFigaro
//
//  Created by Luca Palmese on 20/02/2020.
//  Copyright © 2020 fernando rosa. All rights reserved.
//

import UIKit


import UIKit

class BarbershopCollectionViewCell: UICollectionViewCell {
    
    // Treatment ImageView Outlet
    @IBOutlet weak var treatmentImage: UIImageView!
    
    // Treatment Name Label Outlet
    @IBOutlet weak var treatmentName: UILabel!
    
    var treatmentDuration: Int?
    
    // Treatment Description Label Outlet
    @IBOutlet weak var treatmentDescription: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
