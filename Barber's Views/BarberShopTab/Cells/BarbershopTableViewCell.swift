//
//  BarbershopTableViewCell.swift
//  MyFigaro
//
//  Created by Luca Palmese on 20/02/2020.
//  Copyright © 2020 fernando rosa. All rights reserved.
//

import UIKit

protocol BarbershopCollectionViewCellSelectionDelegate: class {
    func selectedItem(tag: Int, indexPath: IndexPath)
}

class BarbershopTableViewCell: UITableViewCell {
    
    // TreatmentsList
    var treatmentsList = [Treatment]()
    
    // TableView Cell's Label Outlet
    @IBOutlet weak var titleLabel: UILabel!
    
    // CollectionView Outlet
    @IBOutlet weak var collectionView: UICollectionView!
    
    // Delegate to select BarbershopCollectionViewCell
    weak var delegate: BarbershopCollectionViewCellSelectionDelegate?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Giving CollectionView the BarbershopTableViewCell as Delegate and DataSource
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.tag = 4001
        
        // Initialization code
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func reload(){
        self.collectionView.reloadData()
    }

}


// BarbershopTableViewCell extension to support the CollectionView
extension BarbershopTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return treatmentsList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let barbershopCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "barbershopCollectionViewCell", for: indexPath) as? BarbershopCollectionViewCell else {fatalError("Unable to create a CollectionView cell")}
        
        barbershopCollectionViewCell.treatmentImage.clipsToBounds = false
        barbershopCollectionViewCell.treatmentImage.layer.cornerRadius = 30
        barbershopCollectionViewCell.treatmentImage.layer.shadowColor = UIColor.gray.cgColor
        barbershopCollectionViewCell.treatmentImage.layer.shadowOpacity = 0.5
        barbershopCollectionViewCell.treatmentImage.layer.shadowOffset = CGSize(width: 0, height: 4)
        barbershopCollectionViewCell.treatmentImage.layer.shadowRadius = 3
        barbershopCollectionViewCell.treatmentImage.image = UIImage(named: "TreatmentGenericImage")
        barbershopCollectionViewCell.treatmentName.adjustsFontSizeToFitWidth = true
        barbershopCollectionViewCell.treatmentName.minimumScaleFactor = 0.5
        barbershopCollectionViewCell.treatmentName.text = treatmentsList[indexPath.row].name
        barbershopCollectionViewCell.treatmentDuration = treatmentsList[indexPath.row].duration
        barbershopCollectionViewCell.treatmentDescription.adjustsFontSizeToFitWidth = true
        barbershopCollectionViewCell.treatmentDescription.minimumScaleFactor = 0.5
        
      
        if let savedImage = ResourcesManager.shared.retrieveImage(forKey: treatmentsList[indexPath.row].image){
            barbershopCollectionViewCell.treatmentImage.image = savedImage
        } else {
             barbershopCollectionViewCell.treatmentImage.image = UIImage(named:  treatmentsList[indexPath.row].image != "" ? treatmentsList[indexPath.row].image : "TreatmentGenericImage")
        }
        
        barbershopCollectionViewCell.treatmentDescription.text = treatmentsList[indexPath.row].description + " - " +
        "\(treatmentsList[indexPath.row].duration) min"
        
        return barbershopCollectionViewCell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let selectedItem = collectionView.cellForItem(at: indexPath) as! BarbershopCollectionViewCell
        
//        Session.sharedInstance.selectedSong = Song(cover: selectedItem.coverImage.image!, title: selectedItem.trackName.text!, artist: selectedItem.artistName.text!)
        
        self.delegate?.selectedItem(tag: collectionView.tag, indexPath: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 125, height: 165)
    }
    
}
