//
//  BarbershopCollectionViewCell3.swift
//  MyFigaro
//
//  Created by fernando rosa on 11/03/2020.
//  Copyright © 2020 fernando rosa. All rights reserved.
//

import UIKit

class BarbershopCollectionViewCell3: UICollectionViewCell {
    
    @IBOutlet weak var holydayName: UILabel!
    @IBOutlet weak var holidayDay: UILabel!
    @IBOutlet weak var holidayMonth: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
