//
//  BarbershopTableViewCell3.swift
//  MyFigaro
//
//  Created by Luca Palmese on 21/02/2020.
//  Copyright © 2020 fernando rosa. All rights reserved.
//

import UIKit

var notificationsList = ResourcesManager.shared.getNotifications(shopId: "000")

class BarbershopTableViewCell3: UITableViewCell {
    
    // TableView Cell's Label Outlet
    @IBOutlet weak var titleLabel: UILabel!
    
    // TableView Outlet
    @IBOutlet weak var tableView: UITableView!
    
    // Delegate to select BarbershopCollectionViewCell
    weak var delegate: BarbershopCollectionViewCellSelectionDelegate?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Giving CollectionView the BarbershopTableViewCell as Delegate and DataSource
        tableView.delegate = self
        tableView.dataSource = self
        
        // Initialization code
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func reload(){
        self.tableView.reloadData()
    }

}


// Extension to manage the TableView
extension BarbershopTableViewCell3: UITableViewDelegate, UITableViewDataSource, UITextViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notificationsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let innerBarberShopTableViewCell = tableView.dequeueReusableCell(withIdentifier: "innerBarbershopTableViewCell", for: indexPath) as? InnerBarberShopTableViewCell else {
            fatalError("Unable to create barber calendar table view cell")
        }
        
        innerBarberShopTableViewCell.dateLabel.adjustsFontForContentSizeCategory = true
        innerBarberShopTableViewCell.dateLabel.minimumScaleFactor = 0.5
        innerBarberShopTableViewCell.dateLabel.text = dateFormatter4.string(from: notificationsList[indexPath.row].date)
        innerBarberShopTableViewCell.notificationMessageTextView.text = notificationsList[indexPath.row].message
        
        return innerBarberShopTableViewCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let delete = deleteAction(tableView, at: indexPath)
        let swipeAction = UISwipeActionsConfiguration(actions: [delete])
        swipeAction.performsFirstActionWithFullSwipe = false
        return swipeAction
    }
    
    func deleteAction(_ tableView: UITableView, at indexPath: IndexPath) -> UIContextualAction {
        let action = UIContextualAction(style: .destructive, title: "Delete") {
            (action, view, completion) in
            notificationsList.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .automatic)
            completion(true)
        }
        action.image = UIImage(systemName: "trash")
        action.backgroundColor = .systemRed
        
        return action
    }
    
    // Use this if you have a UITextView
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        // get the current text, or use an empty string if that failed
        let currentText = textView.text ?? ""
        
        // attempt to read the range they are trying to change, or exit if we can't
        guard let stringRange = Range(range, in: currentText) else { return false }
        
        // add their new text to the existing text
        let updatedText = currentText.replacingCharacters(in: stringRange, with: text)
        
        // make sure the result is under 16 characters
        return updatedText.count <= 50
    }
}
