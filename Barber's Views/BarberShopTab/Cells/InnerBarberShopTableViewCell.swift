//
//  InnerBarberShopTableViewCell.swift
//  MyFigaro
//
//  Created by Luca Palmese on 21/02/2020.
//  Copyright © 2020 fernando rosa. All rights reserved.
//

import UIKit

class InnerBarberShopTableViewCell: UITableViewCell {
    
    // Date Outlet
    @IBOutlet weak var dateLabel: UILabel!
    
    // Notification Message Outlet
    @IBOutlet weak var notificationMessageTextView: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
