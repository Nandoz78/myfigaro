//
//  BarberManagementViewController.swift
//  MyFigaro
//
//  Created by Luca Palmese on 24/02/2020.
//  Copyright © 2020 fernando rosa. All rights reserved.
//

import UIKit

import UIKit

enum TypeBarberView: Int{
    case newBarber
    case editBarber
    case unknown
}

protocol EditBarberViewControllerDelegate {
    func updateBarber(barber: Barber?)
    func addBarber(barber: Barber)
    func reloadBarbers()
}

class BarberManagementViewController: UIViewController, UITextFieldDelegate {
    
    var treatmentsList:[Treatment] = [Treatment]()
    
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var picture: UIImageView!
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var addPictureButtonBorder: UIView!
    @IBOutlet weak var addPictureButton: UIButton!

    //Name field
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var nameView: UIView!
    @IBOutlet weak var txtName: UITextField!
    
    //Funny Description field
    @IBOutlet weak var lblFunnyDescription: UILabel!
    @IBOutlet weak var funnyDescriptionView: UIView!
    @IBOutlet weak var txtfunnyDescription: UITextField!
    
    // Barber Holidays
    @IBOutlet weak var lblHolidays: UILabel!
    @IBOutlet weak var btnCalendar: UIButton!
    @IBOutlet weak var calendarView: UIView!
    
    @IBOutlet weak var lblService: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblImage: UILabel!
    
    @IBOutlet weak var scrollView:UIScrollView!
    
    var activeField: UITextField!
    var typeView: TypeBarberView = .newBarber
    var barberToEdit: Barber!
    var selectedTreatments = [Treatment]()
    var delegate: EditBarberViewControllerDelegate?
    
    @IBOutlet weak var deleteBtn: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Giving CollectionView the BarbershopTableViewCell as Delegate and DataSource
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.allowsMultipleSelection = true
       
        self.txtName.delegate = self
        self.txtfunnyDescription.delegate = self
        
        
        self.txtName.tag = 1001
        self.txtfunnyDescription.tag = 1002
        
        self.txtName.isEnabled = true
        self.txtfunnyDescription.isEnabled = true
        
        self.nameView.layer.cornerRadius = 5.0
        self.funnyDescriptionView.layer.cornerRadius = 5.0
        
        self.deleteBtn.layer.cornerRadius = 10.0
        
        self.lblTitle.text = (self.typeView == .newBarber) ? "New Barber" : "Edit Barber"
        if(self.typeView == .editBarber){
            self.txtName.textColor = .label
            self.txtfunnyDescription.textColor = .label
            self.txtName.text = self.barberToEdit.name
            self.txtfunnyDescription.text = self.barberToEdit.funnyDescription
            self.deleteBtn.isHidden = false
        }else{
            self.deleteBtn.isHidden = true
        }
        
        self.btnCalendar.layer.cornerRadius = 5.0
        self.btnCalendar.addTarget(self, action: #selector(self.calendar(_:)),
                                   for: .touchDown)
        
    }
    
    @objc func calendar(_ sender: UIButton) {
        self.showSpinner(onView: self.view)
        DataBaseManager.shared.getBarberHolidays(barber: self.barberToEdit, completion: {
          list in
            self.removeSpinner()
            if(list != nil){
                self.barberToEdit.holidays = list!
            }else{
                self.barberToEdit.holidays = [Holiday]()
            }
            self.performSegue(withIdentifier: "goToCalendarView", sender: self)
        })
    }
       
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(aNotification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(aNotification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if(self.typeView == .editBarber) {
            selectPersonalServices()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
    }
    
    override func viewWillLayoutSubviews() {
        picture.clipsToBounds = false
        picture.layer.cornerRadius = 30
        picture.layer.shadowColor = UIColor.gray.cgColor
        picture.layer.shadowOpacity = 0.5
        picture.layer.shadowOffset = CGSize(width: 0, height: 4)
        picture.layer.shadowRadius = 3
        picture.image = UIImage(named: "Barber")
        addPictureButtonBorder.layer.cornerRadius = addPictureButtonBorder.frame.height/2
    }
    
    func selectPersonalServices() {
        for index2 in 0..<treatmentsList.count {
            for index in 0..<barberToEdit.treatments.count {
                if treatmentsList[index2].id == self.barberToEdit.treatments[index].id {
                    self.collectionView.selectItem(at: IndexPath(item: index2, section: 0), animated: false, scrollPosition: [])
                    (collectionView.cellForItem(at: IndexPath(item: index2, section: 0)) as!  ServicesSelectionModalCollectionViewCell).serviceImage?.alpha = 1.0
                    (collectionView.cellForItem(at: IndexPath(item: index2, section: 0)) as!  ServicesSelectionModalCollectionViewCell).serviceName!.alpha = 1.0
                    (collectionView.cellForItem(at: IndexPath(item: index2, section: 0)) as!  ServicesSelectionModalCollectionViewCell).serviceDuration!.alpha = 1.0
                }
            }
        }
        self.selectedTreatments = self.barberToEdit.treatments
    }
    
    func saveButtonToggle() {
        if txtName.text!.isEmpty || selectedTreatments.isEmpty || (txtName.text == barberToEdit?.name && txtfunnyDescription.text == barberToEdit?.funnyDescription && areTreatmentsArraysTheSame(array1: selectedTreatments, array2: barberToEdit.treatments)) {
            saveButton.isEnabled = false
        } else {
            saveButton.isEnabled = true
        }
    }
    
    func areTreatmentsArraysTheSame(array1: [Treatment], array2: [Treatment]) -> Bool {
        var count = 0
        if array1.count != array2.count {
            return false
        }
        for index in 0..<array1.count {
            for index2 in 0..<array2.count {
                if array1[index].name == array2[index2].name && array1[index].duration == array2[index2].duration && array1[index].description == array2[index2].description {
                    count += 1
                    if count == array1.count {
                        return true
                    }
                }
            }
        }
        return false
    }
    
    @objc func keyboardWillBeHidden(aNotification: NSNotification) {
        
        let contentInsets: UIEdgeInsets = .zero
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
    }
    
    @objc func keyboardWillShow(aNotification: NSNotification) {
        
        let info = aNotification.userInfo!
        let kbSize: CGSize = ((info["UIKeyboardFrameEndUserInfoKey"] as? CGRect)?.size)!
        print("kbSize = \(kbSize)")
        let contentInsets: UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: kbSize.height, right: 0.0)
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
        var aRect: CGRect = self.view.frame
        aRect.size.height -= kbSize.height
        if !aRect.contains(activeField!.frame.origin) {
            self.scrollView.scrollRectToVisible(activeField!.frame, animated: true)
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.activeField = textField
        if self.typeView == .newBarber {
            txtName.textColor = .label
            txtfunnyDescription.textColor = .label
        }
        switch textField.tag {
        case 1001:
            self.nameView.layer.borderColor = UIColor(red: 229.0/255.0, green: 73.0/255.0, blue: 44.0/255.0, alpha: 1.0).cgColor
            self.nameView.layer.borderWidth = 2.0
        case 1002:
            self.funnyDescriptionView.layer.borderColor = UIColor(red: 229.0/255.0, green: 73.0/255.0, blue: 44.0/255.0, alpha: 1.0).cgColor
            self.funnyDescriptionView.layer.borderWidth = 2.0
       default:
            print()
        }
    }
    
    @IBAction func textFieldEditingChanged(_ sender: UITextField) {
        saveButtonToggle()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField.tag {
        case 1001:
            self.nameView.layer.borderWidth = 0.0
        case 1002:
            self.funnyDescriptionView.layer.borderWidth = 0.0
         default:
            print()
        }
        self.activeField = nil
        //self.checkData()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTag = textField.tag + 1

        if let nextResponder = self.view.viewWithTag(nextTag) {
            nextResponder.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        //self.checkData()
        return true
    }
    
    func updateBarber(barber: Barber?){
        self.delegate?.updateBarber(barber: barber)
    }
    
    
    func addBarber(barber: Barber){
        self.delegate?.addBarber(barber: barber)
    }
    
    @IBAction func deleteButtonTapped(_ sender: UIButton) {
           self.showSpinner(onView: self.view)
           DataBaseManager.shared.deleteBarber(barber: self.barberToEdit, completion: { msg in
                  self.removeSpinner()
                  if msg.contains("Error") {
                      self.showErrorAlert(title: "Delete Barber", msg: msg)
                  }else{
                      self.showErrorAlert(title: "Delete Barber", msg: msg)
                  }
              })
          }
    
    // CANCEL BUTTON ACTION
    @IBAction func cancelButtonTapped(_ sender: UIButton) {
        self.updateBarber(barber: nil)
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    // ADD BUTTON ACTION
    @IBAction func saveButtonTapped(_ sender: UIButton) {
        self.dismiss(animated: true) {
            if self.typeView == .newBarber {
                let newBarber:Barber = Barber(name: self.txtName.text!, image: "", funnyDescription: self.txtfunnyDescription.text ?? "", treatments:  self.selectedTreatments)
                self.showSpinner(onView: self.view)
                DataBaseManager.shared.addBarber(idShop: UserManager.shared.shop.shopId, barber: newBarber, completion: { res in
                    self.removeSpinner()
                    if !res.contains("Error"){
                        newBarber.id = res
                        self.addBarber(barber: newBarber)
                        self.dismiss(animated: true) {

                        }
                    }else{
                        self.showErrorAlert(title: "New Barber", msg: res)
                    }
                })
            } else if self.typeView == .editBarber {
                self.barberToEdit.name = self.txtName.text!
                self.barberToEdit.image = ""
                self.barberToEdit.funnyDescription = self.txtfunnyDescription.text ?? ""
                self.barberToEdit.updateServices(servicesUpdated: self.selectedTreatments)
                self.showSpinner(onView: self.view)
                DataBaseManager.shared.updateBarber(barber: self.barberToEdit, completion: { msg in
                    self.removeSpinner()
                    self.showErrorAlert(title: "Edit Barber", msg: msg)
                })
                self.updateBarber(barber: self.barberToEdit)
            }
        }
    }
    

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    
    
    // TAKE A PICTURE: addPictureButtonTapped
    @IBAction func addPictureButtonTapped(_ sender: UIButton) {
        
        
        let alert = UIAlertController.init(title: "Add Image", message: nil, preferredStyle: .actionSheet)
        

        let cameraAction = UIAlertAction(title: "Take from Camera", style: .default) { (_) in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                self.showImagePickerController(sourceType: .camera)
                print("Camera")
            }
        }
        

        let photoLibraryAction = UIAlertAction(title: "Choose from Photo Library", style: .default) { (_) in
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                self.showImagePickerController(sourceType: .photoLibrary)
                print("Photo Library")
            }
        }
        
        alert.addAction(cameraAction)
        alert.addAction(photoLibraryAction)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        // POPOVER Action Sheet
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        
        self.present(alert, animated: true)
        
    }
    
    func showErrorAlert(title:String, msg:String) {
           let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
           alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { _ in
               self.dismiss(animated: true, completion: {
                   self.delegate?.reloadBarbers()
               })
           }))
           self.present(alert, animated: true, completion: nil)
       }
       
       var vSpinner: UIView?
       
       func showSpinner(onView : UIView) {
          let spinnerView = UIView.init(frame: onView.frame)
          spinnerView.backgroundColor = UIColor.init(red: 229.0/255.0, green: 73.0/255.0, blue: 44.0/255.0, alpha: 0.1)
          let ai = UIActivityIndicatorView.init(style: .large)
          ai.color =  UIColor(red: 229.0/255.0, green: 73.0/255.0, blue: 44.0/255.0, alpha: 1.0)
       
          let image = UIImageView(image: UIImage(named: "Barber"))
          image.frame.size = CGSize(width: 60, height: 60)
          image.center = CGPoint(x: spinnerView.center.x, y: spinnerView.center.y - 60.0)
          ai.startAnimating()
          ai.center = spinnerView.center
          
          DispatchQueue.main.async {
              spinnerView.addSubview(ai)
              spinnerView.addSubview(image)
              onView.addSubview(spinnerView)
          }
          
          vSpinner = spinnerView
       }
       
       func removeSpinner() {
           DispatchQueue.main.async {
               self.vSpinner?.removeFromSuperview()
               self.vSpinner = nil
           }
       }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "goToCalendarView")
        {
            NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
            NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
            let nvc = segue.destination as! BarberHolidaysCalendarViewController
            nvc.holidays = self.barberToEdit.holidays
        }
    }

}


// Load ImagePictureController Extension
extension BarberManagementViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func showImagePickerController(sourceType: UIImagePickerController.SourceType) {
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            
            let imagePicker = UIImagePickerController()

            imagePicker.delegate = self
            imagePicker.sourceType = sourceType
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerController (_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let editedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            picture.image = editedImage
        } else if let originalImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            picture.image = originalImage
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
   
}


// BarberManagementViewController extension to support the CollectionView
extension BarberManagementViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.treatmentsList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let servicesSelectionModalCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "servicesSelectionModalCollectionViewCell", for: indexPath) as? ServicesSelectionModalCollectionViewCell else {fatalError("Unable to create a CollectionView cell")}
        
        servicesSelectionModalCollectionViewCell.isSelected = false
        servicesSelectionModalCollectionViewCell.serviceImage.alpha = 0.5
        servicesSelectionModalCollectionViewCell.serviceName.alpha = 0.5
        servicesSelectionModalCollectionViewCell.serviceDuration.alpha = 0.5
        servicesSelectionModalCollectionViewCell.serviceImage.clipsToBounds = false
        servicesSelectionModalCollectionViewCell.serviceImage.layer.cornerRadius = 25
        servicesSelectionModalCollectionViewCell.serviceImage.layer.shadowColor = UIColor.gray.cgColor
        servicesSelectionModalCollectionViewCell.serviceImage.layer.shadowOpacity = 0.5
        servicesSelectionModalCollectionViewCell.serviceImage.layer.shadowOffset = CGSize(width: 0, height: 3.5)
        servicesSelectionModalCollectionViewCell.serviceImage.layer.shadowRadius = 2.5
        servicesSelectionModalCollectionViewCell.serviceImage.image = UIImage(named: treatmentsList[indexPath.row].image)
        servicesSelectionModalCollectionViewCell.serviceName.adjustsFontSizeToFitWidth = true
        servicesSelectionModalCollectionViewCell.serviceName.minimumScaleFactor = 0.5
        servicesSelectionModalCollectionViewCell.serviceName.text = treatmentsList[indexPath.row].name
        servicesSelectionModalCollectionViewCell.serviceDuration.adjustsFontSizeToFitWidth = true
        servicesSelectionModalCollectionViewCell.serviceDuration.minimumScaleFactor = 0.5
        servicesSelectionModalCollectionViewCell.serviceDuration.text = "\(treatmentsList[indexPath.row].duration) min"
        servicesSelectionModalCollectionViewCell.serviceDescription = treatmentsList[indexPath.row].description
        
        return servicesSelectionModalCollectionViewCell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let selectedItem = collectionView.cellForItem(at: indexPath) as! ServicesSelectionModalCollectionViewCell
        if (!selectedTreatments.isEmpty) {
            selectedTreatments.append(treatmentsList[indexPath.item])
        } else {
            selectedTreatments = [treatmentsList[indexPath.item]]
        }
        selectedItem.serviceImage.alpha = 1
        selectedItem.serviceName.alpha = 1
        selectedItem.serviceDuration.alpha = 1
        
        saveButtonToggle()
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let selectedItem = collectionView.cellForItem(at: indexPath) as! ServicesSelectionModalCollectionViewCell
        selectedItem.serviceImage.alpha = 0.5
        selectedItem.serviceName.alpha = 0.5
        selectedItem.serviceDuration.alpha = 0.5
        
        for index in 0 ..< selectedTreatments.count {
            if selectedTreatments[index].name == selectedItem.serviceName.text {
                selectedTreatments.remove(at: index)
                break
            }
        }
        
        saveButtonToggle()
        //        selectedServices.removeValue(forKey: selectedItem.serviceName.text!)
        //        print(selectedServices)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 120, height: 169)
    }
    
    
    
}
