//
//  NewNotificationViewController.swift
//  MyFigaro
//
//  Created by Luca Palmese on 25/02/2020.
//  Copyright © 2020 fernando rosa. All rights reserved.
//

import UIKit

protocol AddNotificationViewControllerDelegate {
    func addNotification(notification: Notification)
}

class NewNotificationViewController: UIViewController, UITextViewDelegate {
    
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var newNotification: UITextView!
    
    var delegate: AddNotificationViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        newNotification.delegate = self
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        newNotification.becomeFirstResponder()
    }
    
    // CANCEL BUTTON ACTION
    @IBAction func cancelButtonTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // ADD BUTTON ACTION
    @IBAction func sendButtonTapped(_ sender: UIButton) {
        self.delegate?.addNotification(notification: Notification(date: date, message: newNotification.text))
        print("Notification Sent!")
        dismiss(animated: true)
    }
    
    // TextView Action when it changes
    func textViewDidChange(_ textView: UITextView) {
        textView.textColor = .label
        if !newNotification.text!.isEmpty {
            sendButton.isEnabled = true
        } else {
            sendButton.isEnabled = false
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
