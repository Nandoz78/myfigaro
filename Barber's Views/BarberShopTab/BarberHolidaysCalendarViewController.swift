//
//  BarberHolidaysCalendarViewController.swift
//  MyFigaro
//
//  Created by fernando rosa on 13/03/2020.
//  Copyright © 2020 fernando rosa. All rights reserved.
//

import UIKit

enum TypeHolidayView:Int{
    case newHoliday
    case editHoliday
    case unknown
}

class BarberHolidaysCalendarViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.holidays.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "holidayCell") as! HolidayTableViewCell
        cell.initCell(bigNumber: self.holidays[indexPath.row].duration, fromDate: self.holidays[indexPath.row].getFrom(), toDate: self.holidays[indexPath.row].getTo())
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedHoliday = indexPath.row
        self.editDay(holiday: self.holidays[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            self.tableView.beginUpdates()
            self.holidays.remove(at: indexPath.row)
            self.tableView.deleteRows(at: [indexPath], with: .automatic)
            self.tableView.endUpdates()
        }
    }
    
    @IBAction func cancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
   
  
    @IBOutlet weak var btnCancel: UIBarButtonItem!

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnAdd: UIButton!
  
    @IBOutlet weak var btnConfirmAdd: UIButton!
    @IBOutlet weak var btnCancelAdd: UIButton!
    @IBOutlet weak var addView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    
    @IBOutlet weak var fromView: UIView!
    @IBOutlet weak var txtFrom: UITextField!
    
    @IBOutlet weak var toView: UIView!
    @IBOutlet weak var txtTo: UITextField!
    
    @IBOutlet weak var descriptionView: UIView!
    @IBOutlet weak var txtDescription: UITextField!
    @IBOutlet weak var lblOperationalMode: UILabel!
    
    var typeView:TypeHolidayView = .newHoliday
    
    var holidays:[Holiday]!
    var newHoliday:Holiday!
    var selectedHoliday:Int = 0
    let datePicker = UIDatePicker()
    var selectedTxt:UITextField!
    var activeField: UITextField?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.btnAdd.layer.cornerRadius = 5.0
        self.btnConfirmAdd.layer.cornerRadius = 5.0
        
        self.scrollView.alpha = 0.0
        self.btnAdd.alpha = 1.0
        self.lblOperationalMode.alpha = 0.0
        self.addView.frame.origin.y += (self.addView.frame.height - 100)
        
        self.txtFrom.tag = 1001
        self.txtTo.tag = 1002
        self.txtDescription.tag = 1003
        
        self.txtFrom.delegate = self
        self.txtTo.delegate = self
        self.txtDescription.delegate = self
        
        self.txtFrom.isEnabled = true
        self.txtTo.isEnabled = true
        self.txtDescription.isEnabled = true
        
        self.fromView.layer.cornerRadius = 5.0
        self.toView.layer.cornerRadius = 5.0
        self.descriptionView.layer.cornerRadius = 5.0
        
        self.showDatePicker()
    }
    
    override func viewWillAppear(_ animated: Bool) {
       NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(aNotification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
       NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(aNotification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
    }

    override func viewWillDisappear(_ animated: Bool) {
       NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
       NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
    }
    @objc func keyboardWillBeHidden(aNotification: NSNotification) {
        
        let contentInsets: UIEdgeInsets = .zero
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
    }
    
    @objc func keyboardWillShow(aNotification: NSNotification) {
        
        let info = aNotification.userInfo!
        let kbSize: CGSize = ((info["UIKeyboardFrameEndUserInfoKey"] as? CGRect)?.size)!
        print("kbSize = \(kbSize)")
        let contentInsets: UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: kbSize.height, right: 0.0)
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
        var aRect: CGRect = self.view.frame
        aRect.size.height -= kbSize.height
        if !aRect.contains(activeField!.frame.origin) {
            self.scrollView.scrollRectToVisible(activeField!.frame, animated: true)
        }
    }
    
    func showDatePicker(){
       //Formate Date
       datePicker.datePickerMode = .date

      //ToolBar
      let toolbar = UIToolbar();
      toolbar.sizeToFit()
      let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
     let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));

    toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        toolbar.tintColor = UIColor(red: 229.0/255.0, green: 73.0/255.0, blue: 44.0/255.0, alpha: 1.0)
        
        self.txtFrom.inputAccessoryView = toolbar
        self.txtFrom.inputView = datePicker
        
        self.txtTo.inputAccessoryView = toolbar
        self.txtTo.inputView = datePicker

    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.selectedTxt = textField
        self.activeField = textField
        switch textField.tag {
        case 1001:
            self.fromView.layer.borderColor = UIColor(red: 229.0/255.0, green: 73.0/255.0, blue: 44.0/255.0, alpha: 1.0).cgColor
            self.fromView.layer.borderWidth = 2.0
            if(self.typeView == .editHoliday){
                self.datePicker.setDate(self.holidays[self.selectedHoliday].start, animated: false)
            }
        case 1002:
            self.toView.layer.borderColor = UIColor(red: 229.0/255.0, green: 73.0/255.0, blue: 44.0/255.0, alpha: 1.0).cgColor
            self.toView.layer.borderWidth = 2.0
            if(self.typeView == .editHoliday){
                self.datePicker.setDate(self.holidays[self.selectedHoliday].end, animated: false)
            }
        case 1003:
            self.descriptionView.layer.borderColor = UIColor(red: 229.0/255.0, green: 73.0/255.0, blue: 44.0/255.0, alpha: 1.0).cgColor
            self.descriptionView.layer.borderWidth = 2.0
        default:
            print()
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField.tag {
        case 1001:
            self.fromView.layer.borderWidth = 0.0
        case 1002:
            self.toView.layer.borderWidth = 0.0
        case 1003:
            self.descriptionView.layer.borderWidth = 0.0
        default:
            print()
        }
        self.activeField = nil
        //self.checkData()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTag = textField.tag + 1

        if let nextResponder = self.view.viewWithTag(nextTag) {
            nextResponder.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        //self.checkData()
        return true
    }
    
    @objc func donedatePicker(){

      let formatter = DateFormatter()
      formatter.dateFormat = "dd/MM/yyyy"
      self.selectedTxt.text = formatter.string(from: datePicker.date)
      self.view.endEditing(true)
    }

    @objc func cancelDatePicker(){
       self.view.endEditing(true)
    }
    
    func editDay(holiday:Holiday)
    {
        self.typeView = .editHoliday
        self.lblOperationalMode.text = "Edit Holiday"
        self.txtFrom.text = holiday.getFrom()
        self.txtTo.text = holiday.getEditTo()
        self.txtDescription.text = holiday.description
        
        UIView.animate(withDuration: 0.2, animations: {
            self.addView.frame.origin.y -= (self.addView.frame.height - 100)
            self.scrollView.alpha = 1.0
            self.btnAdd.alpha = 0.0
            self.lblOperationalMode.alpha = 1.0
        })
    }
    
    @IBAction func addNewDay(_ sender: Any) {
        self.typeView = .newHoliday
        self.lblOperationalMode.text = "New Holiday"
        self.txtFrom.text = ""
        self.txtTo.text = ""
        self.txtDescription.text = ""
        UIView.animate(withDuration: 0.2, animations: {
            self.addView.frame.origin.y -= (self.addView.frame.height - 100)
            self.scrollView.alpha = 1.0
            self.btnAdd.alpha = 0.0
            self.lblOperationalMode.alpha = 1.0
        })
    }
    
    @IBAction func cancelNewDay(_ sender: Any) {
        UIView.animate(withDuration: 0.2, animations: {
            self.addView.frame.origin.y += (self.addView.frame.height - 100)
            self.scrollView.alpha = 0.0
            self.btnAdd.alpha = 1.0
            self.lblOperationalMode.alpha = 0.0
        })
    }
    
}
