//
//  NewEntityViewController.swift
//  MyFigaro
//
//  Created by Luca Palmese on 20/02/2020.
//  Copyright © 2020 fernando rosa. All rights reserved.
//

import UIKit

enum TypeTreatmentView: Int{
    case newTreatment
    case editTreatment
    case unknown
}

protocol EditServiceViewControllerDelegate {
    func updateService(treatment: Treatment?)
    func addService(treatment: Treatment)
    func reloadServices()
}

class TreatmentManagementViewController: UIViewController, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource {
    
    
    
    
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var picture: UIImageView!
    
    @IBOutlet weak var addPictureButtonBorder: UIView!
    @IBOutlet weak var addPictureButton: UIButton!

    @IBOutlet weak var lblTitle: UILabel!
    
    //Image field
    @IBOutlet weak var lblImage: UILabel!
    
    //Name field
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var nameView: UIView!
    @IBOutlet weak var txtName: UITextField!
    
    //Duration field
    @IBOutlet weak var lblDuration: UILabel!
    @IBOutlet weak var durationView: UIView!
    @IBOutlet weak var txtDuration: UITextField!
    
    //Description field
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var descriptionView: UIView!
    @IBOutlet weak var txtDescription: UITextField!
    
    @IBOutlet weak var scrollView:UIScrollView!
    var activeField: UITextField?
    
    @IBOutlet weak var hoursPicker: UIPickerView!
    var hoursPickerIsVisible:Bool = false
    
    let hourFirst:String = "0"
    let hourSecond:[String] = ["0","1","2"]
    let minuteFirst:[String] = ["0","1","2","3","4","5"]
    let minuteSecond:[String] = ["0","1","2","3","4","5","6","7","8","9"]
    
    var selectedValue:String!
    var comma:String = ":"
    
    var typeView:TypeTreatmentView = .newTreatment
    var serviceToEdit: Treatment!
    var delegate: EditServiceViewControllerDelegate?
    
    @IBOutlet weak var galleryView: UIView!
    var selectedImage:String!
    
    // BASIC IMAGES
    var basicImages:[String]!
    @IBOutlet weak var basicImagesCollectionView: UICollectionView!
    
    
    @IBOutlet weak var deleteService: UIButton!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.deleteService.layer.cornerRadius = 10.0
        
        self.basicImages = ResourcesManager.shared.loadBasicImages()
        self.basicImagesCollectionView.delegate = self
        self.basicImagesCollectionView.dataSource = self
        print(self.typeView)
        
        self.txtName.delegate = self
        self.txtDuration.delegate = self
        self.txtDescription.delegate = self
       
        
        self.txtName.tag = 1001
        self.txtDuration.tag = 1002
        self.txtDescription.tag = 1003
        
        
        self.txtName.isEnabled = true
        self.txtDuration.isEnabled = true
        self.txtDescription.isEnabled = true
      
        
        self.nameView.layer.cornerRadius = 5.0
        self.durationView.layer.cornerRadius = 5.0
        self.descriptionView.layer.cornerRadius = 5.0
        
        self.txtDuration.inputView = self.hoursPicker
        
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let button = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneAction))
        button.tintColor = UIColor(red: 229.0/255.0, green: 73.0/255.0, blue: 44.0/255.0, alpha: 1.0)
        toolBar.setItems([button], animated: true)
        toolBar.isUserInteractionEnabled = true
        self.txtDuration.inputAccessoryView = toolBar
        
        self.hoursPicker.delegate = self
        self.hoursPicker.frame.origin.y += (self.hoursPicker.frame.height + 30.0)
        self.hoursPicker.backgroundColor?.withAlphaComponent(0.0)
        
        self.lblTitle.text = (self.typeView == .newTreatment) ? "New Service" : "Edit Service"
        if(self.typeView == .editTreatment){
            self.txtName.textColor = .label
            self.txtDuration.textColor = .label
            self.txtDescription.textColor = .label
            self.txtName.text = self.serviceToEdit.name
            if self.serviceToEdit.duration % 60 < 10 {
                self.txtDuration.text = "\(self.serviceToEdit.duration / 60):0\(self.serviceToEdit.duration % 60)"
            } else if self.serviceToEdit.duration / 60 < 10 {
                self.txtDuration.text = "0\(self.serviceToEdit.duration / 60):\(self.serviceToEdit.duration % 60)"
            } else if self.serviceToEdit.duration / 60 < 10 && self.serviceToEdit.duration % 60 < 10 {
                self.txtDuration.text = "0\(self.serviceToEdit.duration / 60):0\(self.serviceToEdit.duration % 60)"
            } else {
                self.txtDuration.text = "\(self.serviceToEdit.duration / 60):\(self.serviceToEdit.duration % 60)"
            }
            self.txtDescription.text = self.serviceToEdit.description
            self.selectedImage = self.serviceToEdit.image
            self.deleteService.isHidden = false
        }else{
            self.deleteService.isHidden = true
            self.saveButton.isEnabled = true
            self.selectedImage = "TreatmentGenericImage"
        }
        
        self.galleryView.frame.origin.y += self.galleryView.frame.height
       
    }
    
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 5
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        switch component {
        case 0:
            return 1
        case 1:
            return self.hourSecond.count
        case 2:
            return 1
        case 3:
            return self.minuteFirst.count
        case 4:
            return self.minuteSecond.count
        default:
            return 1
        }
        
    }
     
     func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        switch component {
        case 0:
            return self.hourFirst
        case 1:
            return self.hourSecond[row]
        case 2:
            return self.comma
        case 3:
            return self.minuteFirst[row]
        case 4:
            return self.minuteSecond[row]
        default:
            return ""
        }
     }
     
     func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
         
         let first = self.hoursPicker.selectedRow(inComponent: 0)
         let second = self.hoursPicker.selectedRow(inComponent: 1)
         let third = self.hoursPicker.selectedRow(inComponent: 3)
         let fourth = self.hoursPicker.selectedRow(inComponent: 4)
         let textToDisplay = "\(first)\(second)\(self.comma)\(third)\(fourth)"
         self.txtDuration.text = textToDisplay
        print(textToDisplay)
     }
    
    @objc func doneAction() {
        self.durationView.layer.borderWidth = 0.0
        self.txtDuration.endEditing(true)
        if(self.hoursPickerIsVisible){
          self.hoursPickerIsVisible = false
          UIView.animate(withDuration: 0.3, animations: {
              self.hoursPicker.frame.origin.y += (self.hoursPicker.frame.height + 30.0)
          })
       }
       self.checkData()
    }
    
    func checkData(){
        if (txtName.text!.isEmpty || txtDuration.text!.isEmpty || txtDuration.text == "00:00") || (txtName.text == serviceToEdit?.name && Int(txtDuration.text!) == serviceToEdit?.duration && txtDescription.text == serviceToEdit?.description) {
            self.saveButton.isEnabled = false
        } else {
            self.saveButton.isEnabled = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(aNotification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
       NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(aNotification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
    }
    
    @objc func keyboardWillBeHidden(aNotification: NSNotification) {

         let contentInsets: UIEdgeInsets = .zero
         self.scrollView.contentInset = contentInsets
         self.scrollView.scrollIndicatorInsets = contentInsets

    }
    
    @objc func keyboardWillShow(aNotification: NSNotification) {
        
        let info = aNotification.userInfo!
        let kbSize: CGSize = ((info["UIKeyboardFrameEndUserInfoKey"] as? CGRect)?.size)!
        print("kbSize = \(kbSize)")
        let contentInsets: UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: kbSize.height, right: 0.0)
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
        var aRect: CGRect = self.view.frame
        aRect.size.height -= kbSize.height
        if !aRect.contains(activeField!.frame.origin) {
            self.scrollView.scrollRectToVisible(activeField!.frame, animated: true)
        }
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.activeField = textField
        if self.typeView == .newTreatment {
            txtName.textColor = .label
            txtDuration.textColor = .label
            txtDescription.textColor = .label
        }
        switch textField.tag {
        case 1001:
            self.nameView.layer.borderColor = UIColor(red: 229.0/255.0, green: 73.0/255.0, blue: 44.0/255.0, alpha: 1.0).cgColor
            self.nameView.layer.borderWidth = 2.0
        case 1002:
            self.durationView.layer.borderColor = UIColor(red: 229.0/255.0, green: 73.0/255.0, blue: 44.0/255.0, alpha: 1.0).cgColor
            self.durationView.layer.borderWidth = 2.0
        case 1003:
            self.descriptionView.layer.borderColor = UIColor(red: 229.0/255.0, green: 73.0/255.0, blue: 44.0/255.0, alpha: 1.0).cgColor
            self.descriptionView.layer.borderWidth = 2.0
        default:
            print()
        }
    }
    
    @IBAction func textFieldEditingChanged(_ sender: UITextField) {
        if (txtName.text!.isEmpty || txtDuration.text!.isEmpty || txtDuration.text == "00:00") || (txtName.text == serviceToEdit?.name && Int(txtDuration.text!) == serviceToEdit?.duration && txtDescription.text == serviceToEdit?.description) {
            saveButton.isEnabled = false
        } else {
            saveButton.isEnabled = true
        }
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField.tag {
        case 1001:
            self.nameView.layer.borderWidth = 0.0
        case 1002:
            self.durationView.layer.borderWidth = 0.0
        case 1003:
            self.descriptionView.layer.borderWidth = 0.0
        default:
            print()
        }
        self.activeField = nil
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTag = textField.tag + 1

        if let nextResponder = self.view.viewWithTag(nextTag) {
            nextResponder.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return true
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    override func viewWillLayoutSubviews() {
        picture.clipsToBounds = false
        picture.layer.cornerRadius = 30
        picture.layer.shadowColor = UIColor.gray.cgColor
        picture.layer.shadowOpacity = 0.5
        picture.layer.shadowOffset = CGSize(width: 0, height: 4)
        picture.layer.shadowRadius = 3
        picture.image = self.serviceToEdit != nil ? UIImage(named: self.serviceToEdit.image) : UIImage(named: "TreatmentGenericImage") 
        addPictureButtonBorder.layer.cornerRadius = addPictureButtonBorder.frame.height/2
    }
    
    
    
    // DELETE BUTTON ACTION
       @IBAction func deleteButtonTapped(_ sender: UIButton) {
        self.showSpinner(onView: self.view)
        DataBaseManager.shared.deleteService(service: self.serviceToEdit, completion: { msg in
               self.removeSpinner()
               if msg.contains("Error") {
                   self.showErrorAlert(title: "Delete Service", msg: msg)
               }else{
                   self.showErrorAlert(title: "Delete Service", msg: msg)
               }
           })
       }
    
    // CANCEL BUTTON ACTION
    @IBAction func cancelButtonTapped(_ sender: UIButton) {
        self.updateService(treatment: nil)
        self.dismiss(animated: true, completion: nil)
    }
    
    
    // ADD BUTTON ACTION
    @IBAction func saveButtonTapped(_ sender: UIButton) {
        let values = self.txtDuration.text!.split(separator: ":")
        let first = String(String(values[0]).first!)
        let second = String(String(values[0]).last!)
        let third = String(String(values[1]).first!)
        let fourth = String(String(values[1]).last!)
        let durationInMinutes = (Int(first)! * 10 + Int(second)!) * 60 + (Int(third)! * 10 + Int(fourth)!)
        
        if self.typeView == .newTreatment {
            let now = Date()
            let imageId:String = "\(now.timeIntervalSince1970)"
           
            let newTreatment = Treatment(name: self.txtName.text!, duration: durationInMinutes, description: self.txtDescription.text ?? "", image: self.selectedImage != "" ? self.selectedImage : imageId)
            self.showSpinner(onView: self.view)
            DataBaseManager.shared.addService(idShop: UserManager.shared.shop.shopId, service: newTreatment, completion: { res in
                self.removeSpinner()
                if !res.contains("Error"){
                    newTreatment.id = res
                    self.addService(treatment: newTreatment)
                    ResourcesManager.shared.store(image: self.picture.image!, forKey: imageId)
                    self.dismiss(animated: true) {

                    }
                }else{
                    self.showErrorAlert(title: "New Service", msg: res)
                }
            })
            
           
        } else if self.typeView == .editTreatment {
            let now = Date()
            let imageId:String = "\(now.timeIntervalSince1970)"
            
            self.serviceToEdit.name = self.txtName.text!
            self.serviceToEdit.duration = durationInMinutes
            self.serviceToEdit.description = self.txtDescription.text ?? ""
            self.serviceToEdit.image = self.selectedImage != "" ? self.selectedImage : imageId
            self.showSpinner(onView: self.view)
            DataBaseManager.shared.updateService(service: self.serviceToEdit, completion: { msg in
                self.removeSpinner()
                self.showErrorAlert(title: "Edit Service", msg: msg)
            })
           
        }
    }
    
    func showErrorAlert(title:String, msg:String) {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { _ in
            self.dismiss(animated: true, completion: {
                self.delegate?.reloadServices()
            })
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    var vSpinner: UIView?
    
    func showSpinner(onView : UIView) {
       let spinnerView = UIView.init(frame: onView.frame)
       spinnerView.backgroundColor = UIColor.init(red: 229.0/255.0, green: 73.0/255.0, blue: 44.0/255.0, alpha: 0.1)
       let ai = UIActivityIndicatorView.init(style: .large)
       ai.color =  UIColor(red: 229.0/255.0, green: 73.0/255.0, blue: 44.0/255.0, alpha: 1.0)
    
       let image = UIImageView(image: UIImage(named: "Barber"))
       image.frame.size = CGSize(width: 60, height: 60)
       image.center = CGPoint(x: spinnerView.center.x, y: spinnerView.center.y - 60.0)
       ai.startAnimating()
       ai.center = spinnerView.center
       
       DispatchQueue.main.async {
           spinnerView.addSubview(ai)
           spinnerView.addSubview(image)
           onView.addSubview(spinnerView)
       }
       
       vSpinner = spinnerView
    }
    
    func removeSpinner() {
        DispatchQueue.main.async {
            self.vSpinner?.removeFromSuperview()
            self.vSpinner = nil
        }
    }
    
    
    @IBAction func closeGallery(_ sender: Any) {
        UIView.animate(withDuration: 0.2, animations: {
        self.galleryView.frame.origin.y += self.galleryView.frame.height})
    }
    
    
    // TAKE A PICTURE: addPictureButtonTapped
    @IBAction func addPictureButtonTapped(_ sender: UIButton) {
        if(self.typeView == .editTreatment){
            let selectedImage:[String] = self.basicImages.filter({$0 == self.serviceToEdit.image})
            self.basicImages.removeAll(where: {$0 == self.serviceToEdit.image})
            if(selectedImage.count > 0){
                 self.basicImages.insert(selectedImage[0], at: 0)
            }
            self.basicImagesCollectionView.reloadData()
        }
        UIView.animate(withDuration: 0.2, animations: {
            self.galleryView.frame.origin.y -= self.galleryView.frame.height})
        
    }
    
    @objc func cameraTapped(button: UIButton){
        UIView.animate(withDuration: 0.2, animations: {
        self.galleryView.frame.origin.y += self.galleryView.frame.height}, completion: { (finished: Bool) in
            UIView.animate(withDuration: 0.2, animations: {
                self.showAlertCamera()
           })
        })
    }
    
    func showAlertCamera(){
        let alert = UIAlertController.init(title: "Select Image", message: nil, preferredStyle: .actionSheet)
        
        
        let cameraAction = UIAlertAction(title: "Take from Camera", style: .default) { (_) in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                self.showImagePickerController(sourceType: .camera)
                print("Camera")
            }
        }
        
        
        let photoLibraryAction = UIAlertAction(title: "Choose from Photo Library", style: .default) { (_) in
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                self.showImagePickerController(sourceType: .photoLibrary)
                print("Photo Library")
            }
        }
        
        alert.addAction(cameraAction)
        alert.addAction(photoLibraryAction)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        // POPOVER Action Sheet
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        
        self.present(alert, animated: true)
    }
    
    func updateService(treatment: Treatment?){
        self.delegate?.updateService(treatment: treatment)
    }
    
    
    func addService(treatment: Treatment){
        self.delegate?.addService(treatment: treatment)
    }
    
    func reloadServices(){
        self.delegate?.reloadServices()
    }
    
    //MARK: Images collection view
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(section == 0){
            return self.basicImages.count
        }else{
            return 1
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if(indexPath.section == 0){
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageCell", for: indexPath) as! BasicImageCollectionViewCell
           cell.image.image = UIImage(named: self.basicImages[indexPath.row])
           cell.container.layer.cornerRadius = 10.0
           cell.layer.cornerRadius = 10.0
            
            if(self.typeView == .editTreatment){
                if(self.serviceToEdit.image == self.basicImages[indexPath.row]){
                    cell.layer.borderColor = UIColor(red: 229.0/255.0, green: 73.0/255.0, blue: 44.0/255.0, alpha: 1.0).cgColor
                    cell.layer.borderWidth = 2.0
                }else{
                    cell.layer.borderWidth = 0.0
                }
            }else{
                if(self.selectedImage == self.basicImages[indexPath.row]){
                    cell.layer.borderColor = UIColor(red: 229.0/255.0, green: 73.0/255.0, blue: 44.0/255.0, alpha: 1.0).cgColor
                    cell.layer.borderWidth = 2.0
                }else{
                    cell.layer.borderWidth = 0.0
                }
            }
            
            
           return cell
        } else {
             let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cameraCell", for: indexPath) as! CameraCollectionViewCell
            cell.cameraBtn.addTarget(self, action: #selector(cameraTapped(button:)), for: .touchUpInside)
            cell.layer.cornerRadius = 10.0
            return cell
        }
       
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if(indexPath.section == 0){
            if(self.typeView == .editTreatment)
            {
                self.serviceToEdit.image = self.basicImages[indexPath.row]
            }
            self.selectedImage = self.basicImages[indexPath.row]
            self.picture.image = UIImage(named: self.selectedImage)
            UIView.animate(withDuration: 0.2, animations: {
                self.galleryView.frame.origin.y += self.galleryView.frame.height
            })
        }
    }
}


// Load ImagePictureController Extension
extension TreatmentManagementViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func showImagePickerController(sourceType: UIImagePickerController.SourceType) {
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            
            let imagePicker = UIImagePickerController()

            imagePicker.delegate = self
            imagePicker.sourceType = sourceType
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerController (_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let editedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            picture.image = editedImage
        } else if let originalImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            picture.image = originalImage
        }
        self.selectedImage = "custom"
        picker.dismiss(animated: true, completion: nil)
        
    }
}
