//
//  BarbershopViewController.swift
//  MyFigaro
//
//  Created by Luca Palmese on 20/02/2020.
//  Copyright © 2020 fernando rosa. All rights reserved.
//

import UIKit

class BarbershopViewController: UIViewController, EditBarberViewControllerDelegate, EditServiceViewControllerDelegate, AddNotificationViewControllerDelegate {
    
    
    var barbersList:[Barber] = [Barber]()
    var treatmentsList:[Treatment] = [Treatment]()
    
    
    func reloadBarbers() {
        DataBaseManager.shared.getShopBarbers(shopId: UserManager.shared.shop.shopId, completion: { res in
            UserManager.shared.shop.barbers = res
            self.barbersList = UserManager.shared.shop.barbers
            self.tableView.reloadData()
        })
    }
    
    func updateBarber(barber: Barber?) {
        for index in 0..<barbersList.count {
            if barbersList[index].name == barber?.name {
                barbersList[index].name = barber!.name
                barbersList[index].funnyDescription = barber!.funnyDescription
                barbersList[index].treatments = barber!.treatments
                break
            }
        }
        self.barberToEdit = nil
        self.tableView.reloadData()
    }
        
    func addBarber(barber: Barber) {
        barbersList.append(barber)
        self.tableView.reloadData()
    }
    
    func reloadServices() {
        DataBaseManager.shared.getShopServices(shopId: UserManager.shared.shop.shopId, completion: { res in
            UserManager.shared.shop.services = res
            self.treatmentsList = UserManager.shared.shop.services
            self.tableView.reloadData()
        })
    }
    
    func updateService(treatment: Treatment?) {
        if((treatment) != nil){
            for index in 0..<treatmentsList.count {
                if treatmentsList[index].id == treatment?.id {
                    treatmentsList[index].name = treatment!.name
                    treatmentsList[index].duration = treatment!.duration
                    treatmentsList[index].description = treatment!.description
                    break
                }
            }
            self.tableView.reloadData()
        }else{
            self.serviceToEdit = nil
        }
    }
        
    func addService(treatment: Treatment) {
        treatmentsList.append(treatment)
        self.tableView.reloadData()
    }
    
    func addNotification(notification: Notification) {
        notificationsList.append(notification)
        self.tableView.reloadData()
    }
    
    /// Function to evoke segue, called by the BarbershopCollectionViewCellSelectionDelegate protocol in BarbershopTableViewCell class
    func selectedItem(tag: Int, indexPath: IndexPath) {
        if tag == 4001 {
            serviceToEdit = treatmentsList[indexPath.item]
            performSegue(withIdentifier: "treatmentManagementSegue", sender: self)
        } else {
            barberToEdit = barbersList[indexPath.item]
            performSegue(withIdentifier: "barberManagementSegue", sender: self)
        }
    }
    
    var barberToEdit: Barber?
    var serviceToEdit: Treatment?
    
    // TableView Outlet
      @IBOutlet weak var tableView: UITableView!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.barbersList = UserManager.shared.shop.barbers
        self.treatmentsList = UserManager.shared.shop.services
        self.navigationItem.title = UserManager.shared.shop.shopName
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if(segue.identifier == "barberManagementSegue" && self.barberToEdit != nil){
            let nvc = segue.destination as! BarberManagementViewController
            nvc.typeView = .editBarber
            nvc.treatmentsList = self.treatmentsList
            nvc.barberToEdit = self.barberToEdit
            nvc.delegate = self
        } else if segue.identifier == "barberManagementSegue" && self.barberToEdit == nil {
            let nvc = segue.destination as! BarberManagementViewController
            nvc.typeView = .newBarber
            nvc.treatmentsList = self.treatmentsList
            nvc.delegate = self
        } else if(segue.identifier == "treatmentManagementSegue" && self.serviceToEdit != nil){
            let nvc = segue.destination as! TreatmentManagementViewController
            nvc.typeView = .editTreatment
            nvc.serviceToEdit = self.serviceToEdit
            nvc.delegate = self
        } else if segue.identifier == "treatmentManagementSegue" && self.serviceToEdit == nil {
            let nvc = segue.destination as! TreatmentManagementViewController
            nvc.typeView = .newTreatment
            nvc.delegate = self
        }
    }
    
    var vSpinner: UIView?
    
    func showSpinner(onView : UIView) {
       let spinnerView = UIView.init(frame: onView.frame)
       spinnerView.backgroundColor = UIColor.init(red: 229.0/255.0, green: 73.0/255.0, blue: 44.0/255.0, alpha: 0.1)
       let ai = UIActivityIndicatorView.init(style: .large)
       ai.color =  UIColor(red: 229.0/255.0, green: 73.0/255.0, blue: 44.0/255.0, alpha: 1.0)
    
       let image = UIImageView(image: UIImage(named: "Barber"))
       image.frame.size = CGSize(width: 60, height: 60)
       image.center = CGPoint(x: spinnerView.center.x, y: spinnerView.center.y - 60.0)
       ai.startAnimating()
       ai.center = spinnerView.center
       
       DispatchQueue.main.async {
           spinnerView.addSubview(ai)
           spinnerView.addSubview(image)
           onView.addSubview(spinnerView)
       }
       
       vSpinner = spinnerView
    }
    
    func removeSpinner() {
        DispatchQueue.main.async {
            self.vSpinner?.removeFromSuperview()
            self.vSpinner = nil
        }
    }

}


// Extension to manage the TableView
extension BarbershopViewController: UITableViewDelegate, UITableViewDataSource, BarbershopCollectionViewCellSelectionDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            guard let barbershopTableViewCell = tableView.dequeueReusableCell(withIdentifier: "barbershopTableViewCell", for: indexPath) as?  BarbershopTableViewCell else {
                fatalError("Unable to create home table view cell")
            }
            barbershopTableViewCell.titleLabel.text = "Services"
            
            /// To set delegate for BarbershopCollectionViewCellSelectionDelegate found in the HomeTableViewCell class
            barbershopTableViewCell.delegate = self
            barbershopTableViewCell.treatmentsList = self.treatmentsList
            barbershopTableViewCell.reload()
            
            return barbershopTableViewCell
        } else if indexPath.row == 1 {
            guard let barbershopTableViewCell2 = tableView.dequeueReusableCell(withIdentifier: "barbershopTableViewCell2", for: indexPath) as?  BarbershopTableViewCell2 else {
                fatalError("Unable to create home table view cell")
            }
            barbershopTableViewCell2.titleLabel.text = "Barbers"
            
            /// To set delegate for BarbershopCollectionViewCellSelectionDelegate found in the HomeTableViewCell class
            barbershopTableViewCell2.delegate = self
            barbershopTableViewCell2.barbersList = self.barbersList
            barbershopTableViewCell2.reload()
            return barbershopTableViewCell2
        } else {
            guard let barbershopTableViewCell3 = tableView.dequeueReusableCell(withIdentifier: "barbershopTableViewCell3", for: indexPath) as?  BarbershopTableViewCell3 else {
                fatalError("Unable to create home table view cell")
            }
            barbershopTableViewCell3.titleLabel.text = "Notifications"
            
            /// To set delegate for BarbershopCollectionViewCellSelectionDelegate found in the HomeTableViewCell class
            barbershopTableViewCell3.delegate = self
            barbershopTableViewCell3.reload()
            return barbershopTableViewCell3
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250
    }
    
}
