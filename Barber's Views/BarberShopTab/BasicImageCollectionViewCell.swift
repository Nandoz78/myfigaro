//
//  BasicImageCollectionViewCell.swift
//  MyFigaro
//
//  Created by fernando rosa on 05/03/2020.
//  Copyright © 2020 fernando rosa. All rights reserved.
//

import UIKit

class BasicImageCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var image:UIImageView!
    @IBOutlet weak var container:UIView!
}
