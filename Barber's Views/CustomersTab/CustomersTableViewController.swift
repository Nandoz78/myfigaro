//
//  CustomersTableViewController.swift
//  MyFigaro
//
//  Created by Luca Palmese on 21/02/2020.
//  Copyright © 2020 fernando rosa. All rights reserved.
//

import UIKit



enum TypeCustomersView:Int{
    case manage
    case select
    case unknown
}

protocol CustomersTableViewControllerDelegate {
    func selected(selectedCustomer:Client)
}

class CustomersTableViewController: UITableViewController, EditCustomerViewControllerDelegate {
    
    
    func update() {
        self.loadData()
    }
    
    var customers:[Client]!
    
    var customerToEdit:Client?
    
    var typeView:TypeCustomersView = .manage
    
    var delegate:CustomersTableViewControllerDelegate?
    
    // SearchBarContainer View Outlet
    @IBOutlet weak var searchBarContainer: UIView!
    
    // Customers List Handling
    var customersDictionary = [String:[String]]()
    var alphaLetters = [String]()
    
    // SEARCHBAR: Filtered Customers
    var filteredCustomers = [Client]()
    let searchController = UISearchController(searchResultsController: nil)
    
    private func filterCustomers(for searchText: String){
        filteredCustomers = customers.filter { customer in
            return ((customer.surname + " " + customer.name).lowercased().contains(searchText.lowercased()))
        }
        tableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = UserManager.shared.shop.shopName
    }
    
    func loadData(){
        self.showSpinner(onView: self.tableView!)
        self.customersDictionary = [String:[String]]()
        self.alphaLetters = [String]()
        DataBaseManager.shared.getShopClients(shopId: UserManager.shared.shop.shopId) { list in
            self.customers = list
            UserManager.shared.shop.customers = list
            
            for customer in self.customers {
                let customerKey = String((customer.surname + " " + customer.name).prefix(1))
                if var customerValues = self.customersDictionary[customerKey] { customerValues.append(customer.surname + " " + customer.name)
                    self.customersDictionary[customerKey] = customerValues
                } else {
                    self.customersDictionary[customerKey] = [customer.surname + " " + customer.name]
                }
            }
            self.alphaLetters = [String](self.customersDictionary.keys)
            self.alphaLetters = self.alphaLetters.sorted(by: {$0 < $1})
            
            // SEARCHBAR: StudentsListTableView SearchBar
            self.searchController.searchResultsUpdater = self as UISearchResultsUpdating
            self.searchController.obscuresBackgroundDuringPresentation = false
            self.searchController.hidesNavigationBarDuringPresentation = false
            self.searchController.definesPresentationContext = false
            
            self.tableView.reloadData()
            self.removeSpinner()
        }
    }
    
    override func viewWillLayoutSubviews() {
        searchController.searchBar.frame = CGRect(x: 0, y: 0, width: self.tableView.frame.width, height: 55)
        searchBarContainer.addSubview(searchController.searchBar)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.loadData()
        tableView.reloadData()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        searchController.searchBar.endEditing(true)
        searchController.isEditing = false
        searchController.isActive = false
    }
    
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        if searchController.isActive && searchController.searchBar.text != "" {
            return 1
        } else {
            return alphaLetters.count
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let alphaLetter = alphaLetters[section]
        if searchController.isActive && searchController.searchBar.text != "" {
            return filteredCustomers.count
        } else if let customers = customersDictionary[alphaLetter]{
            return customers.count
        }
        return 0
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        let alphaLetter = alphaLetters[indexPath.section]
        if searchController.isActive && (customersDictionary[alphaLetter] != nil) && searchController.searchBar.text != "" {
            let customer = filteredCustomers[indexPath.row]
            cell.textLabel?.text = customer.surname + " " + customer.name
        } else if !searchController.isActive || (customersDictionary[alphaLetter] != nil) || searchController.searchBar.text == "" {
            let customers = customersDictionary[alphaLetter]
            cell.textLabel?.text = customers?[indexPath.row]
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if searchController.isActive && searchController.searchBar.text != "" {
            return nil
        } else {
            return alphaLetters[section]
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(customersDictionary[alphaLetters[indexPath.section]]![indexPath.row])
        self.customerToEdit = self.customers.filter { $0.surname + " " + $0.name == customersDictionary[alphaLetters[indexPath.section]]![indexPath.row]}[0]
        
        if(self.typeView == .manage){
            performSegue(withIdentifier: "toCustomer", sender: self)
        }else{
            self.selected(selectedCustomer: self.customerToEdit!)
        }
        
        
    }
    
    func selected(selectedCustomer:Client){
        self.delegate?.selected(selectedCustomer: selectedCustomer)
        self.dismiss(animated: true, completion: nil)
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let selectedCustomer = self.customers.filter { $0.surname + " " + $0.name == customersDictionary[alphaLetters[indexPath.section]]![indexPath.row]}[0]
             self.showSpinner(onView: self.tableView)
             DataBaseManager.shared.deleteCustomer(shopId: UserManager.shared.shop.shopId, customer: selectedCustomer) { msg in
                self.removeSpinner()
                if msg.contains("Error") {
                    self.showErrorAlert(title: "Delete Customer", msg: msg)
                }else{
                    self.showErrorAlert(title: "Delete Customer", msg: msg)
                }
            }
        }
    }
    
    override func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return alphaLetters
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    
        if(segue.identifier == "toCustomer" && self.customerToEdit != nil){
            let nvc = segue.destination as! NewCustomerModalViewController
            nvc.typeView = .editCustomer
            nvc.customerToEdit = self.customerToEdit
            nvc.delegate = self
        }else{
            let nvc = segue.destination as! NewCustomerModalViewController
            nvc.typeView = .newCustomer
            nvc.delegate = self
        }
    }
    
    func showErrorAlert(title:String, msg:String) {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { _ in
            self.loadData()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    var vSpinner: UIView?
    
    func showSpinner(onView : UIView) {
        let spinnerView = UIView.init(frame: onView.frame)
        spinnerView.backgroundColor = UIColor.init(red: 229.0/255.0, green: 73.0/255.0, blue: 44.0/255.0, alpha: 0.1)
        let ai = UIActivityIndicatorView.init(style: .large)
        ai.color =  UIColor(red: 229.0/255.0, green: 73.0/255.0, blue: 44.0/255.0, alpha: 1.0)
     
        let image = UIImageView(image: UIImage(named: "Barber"))
        image.frame.size = CGSize(width: 60, height: 60)
        image.center = CGPoint(x: spinnerView.center.x, y: spinnerView.center.y - 60.0)
        ai.startAnimating()
        ai.center = spinnerView.center
        
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            spinnerView.addSubview(image)
            onView.addSubview(spinnerView)
        }
        
        vSpinner = spinnerView
    }
    
    func removeSpinner() {
        DispatchQueue.main.async {
            self.vSpinner?.removeFromSuperview()
            self.vSpinner = nil
        }
    }
    
}


// SEARCHBAR Extension: Update function
extension CustomersTableViewController: UISearchResultsUpdating, UISearchBarDelegate {
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.searchController.searchBar.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 55)
    }
    
    func updateSearchResults(for searchController: UISearchController){
        if self.searchController.isActive || self.searchController.isEditing {
            self.searchController.searchBar.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 55)
        } else {
            self.searchController.searchBar.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 55)
        }
        filterCustomers(for: searchController.searchBar.text ?? "")}
}
