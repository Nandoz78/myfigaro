//
//  NewCostumerModalViewController.swift
//  MyFigaro
//
//  Created by Luca Palmese on 25/02/2020.
//  Copyright © 2020 fernando rosa. All rights reserved.
//

import UIKit

enum TypeCustomerView:Int{
    case editCustomer
    case newCustomer
    case unknown
}

protocol EditCustomerViewControllerDelegate {
    func update()
}

class NewCustomerModalViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    
    //First name field
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var firstNameView: UIView!
    @IBOutlet weak var txtFirstName: UITextField!
    
    //Last name field
    @IBOutlet weak var lblLastName: UILabel!
    @IBOutlet weak var lastNameView: UIView!
    @IBOutlet weak var txtLastName: UITextField!
    
    //Phone number field
    @IBOutlet weak var lblPhoneNumber: UILabel!
    @IBOutlet weak var phoneNumberView: UIView!
    @IBOutlet weak var txtPhoneNumber: UITextField!
    
    //Email field
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var txtEmail: UITextField!
    
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var scrollView:UIScrollView!
    var activeField: UITextField?
    var typeView:TypeCustomerView = .newCustomer
    var customerToEdit:Client!
    var delegate:EditCustomerViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.txtFirstName.delegate = self
        self.txtLastName.delegate = self
        self.txtPhoneNumber.delegate = self
        self.txtEmail.delegate = self
        
        self.txtFirstName.tag = 1001
        self.txtLastName.tag = 1002
        self.txtPhoneNumber.tag = 1003
        self.txtEmail.tag = 1004
        
        self.txtFirstName.isEnabled = true
        self.txtLastName.isEnabled = true
        self.txtPhoneNumber.isEnabled = true
        self.txtEmail.isEnabled = true
        
        self.firstNameView.layer.cornerRadius = 5.0
        self.lastNameView.layer.cornerRadius = 5.0
        self.phoneNumberView.layer.cornerRadius = 5.0
        self.emailView.layer.cornerRadius = 5.0
        
        
        self.lblTitle.text = (self.typeView == .newCustomer) ? "New Customer" : "Edit Customer"
        if(self.typeView == .editCustomer){
            self.txtFirstName.text = self.customerToEdit.name
            self.txtLastName.text = self.customerToEdit.surname
            self.txtPhoneNumber.text = self.customerToEdit.cell
            self.txtEmail.text = self.customerToEdit.email
            
        }else{
            self.customerToEdit = Client()
        }
        
        
        self.saveButton.isUserInteractionEnabled = true
        
    }
    
    @objc func keyboardWillBeHidden(aNotification: NSNotification) {

         let contentInsets: UIEdgeInsets = .zero
         self.scrollView.contentInset = contentInsets
         self.scrollView.scrollIndicatorInsets = contentInsets

    }
    
    @objc func keyboardWillShow(aNotification: NSNotification) {
        
        let info = aNotification.userInfo!
        let kbSize: CGSize = ((info["UIKeyboardFrameEndUserInfoKey"] as? CGRect)?.size)!
        print("kbSize = \(kbSize)")
        let contentInsets: UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: kbSize.height, right: 0.0)
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
        var aRect: CGRect = self.view.frame
        aRect.size.height -= kbSize.height
        if !aRect.contains(activeField!.frame.origin) {
            self.scrollView.scrollRectToVisible(activeField!.frame, animated: true)
        }
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.activeField = textField
        switch textField.tag {
        case 1001:
            self.firstNameView.layer.borderColor = UIColor(red: 229.0/255.0, green: 73.0/255.0, blue: 44.0/255.0, alpha: 1.0).cgColor
            self.firstNameView.layer.borderWidth = 2.0
        case 1002:
            self.lastNameView.layer.borderColor = UIColor(red: 229.0/255.0, green: 73.0/255.0, blue: 44.0/255.0, alpha: 1.0).cgColor
            self.lastNameView.layer.borderWidth = 2.0
        case 1003:
            self.phoneNumberView.layer.borderColor = UIColor(red: 229.0/255.0, green: 73.0/255.0, blue: 44.0/255.0, alpha: 1.0).cgColor
            self.phoneNumberView.layer.borderWidth = 2.0
        case 1004:
              self.emailView.layer.borderColor = UIColor(red: 229.0/255.0, green: 73.0/255.0, blue: 44.0/255.0, alpha: 1.0).cgColor
              self.emailView.layer.borderWidth = 2.0
        default:
            print()
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField.tag {
        case 1001:
            self.firstNameView.layer.borderWidth = 0.0
        case 1002:
            self.lastNameView.layer.borderWidth = 0.0
        case 1003:
            self.phoneNumberView.layer.borderWidth = 0.0
        case 1004:
            self.emailView.layer.borderWidth = 0.0
        default:
            print()
        }
        self.activeField = nil
        //self.checkTxtFields()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTag = textField.tag + 1

        if let nextResponder = self.view.viewWithTag(nextTag) {
            nextResponder.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return true
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    // CANCEL BUTTON ACTION
    @IBAction func cancelButtonTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addButton(_ sender: Any) {
        self.view.endEditing(true)
        if(self.checkData()){
            self.showSpinner(onView: self.view)
            
            if(self.typeView == .newCustomer){
                let newCustomer:Client = Client(name: self.txtFirstName.text!, surname: self.txtLastName.text!, cell: self.txtPhoneNumber.text!, email: self.txtEmail.text!, isActive: true)
                DataBaseManager.shared.addCustomerToShop(shopId: UserManager.shared.shop.shopId, customer: newCustomer) { msg in
                    self.removeSpinner()
                    if msg.contains("Error") {
                        self.showErrorAlert(title: "New Customer", msg: msg)
                    }else{
                        self.showErrorAlert(title: "New Customer", msg: "The new customer has been successfully added!")
                    }
                    self.update()
                }
            } else {
                self.customerToEdit.name = self.txtFirstName.text!
                self.customerToEdit.surname = self.txtLastName.text!
                self.customerToEdit.email = self.txtEmail.text!
                self.customerToEdit.cell = self.txtPhoneNumber.text!
                
                DataBaseManager.shared.updateCustomer(customer: self.customerToEdit, completion: { msg in
                        self.removeSpinner()
                        if msg.contains("Error") {
                            self.showErrorAlert(title: "Edit Customer", msg: msg)
                        }else{
                            self.showErrorAlert(title: "Edit Customer", msg: msg)
                        }
                        self.update()
                })
            }
        }
    }
    
    
    
    
    func checkData() -> Bool{
        var res = (self.txtFirstName.text != "" && self.txtLastName.text != "" && self.txtEmail.text != "")
        return res
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(aNotification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(aNotification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        
    }
   
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
    }

    func update(){
        self.delegate?.update()
    }
    
    func showErrorAlert(title:String, msg:String) {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { _ in
            self.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    var vSpinner: UIView?
    
    func showSpinner(onView : UIView) {
       let spinnerView = UIView.init(frame: onView.frame)
       spinnerView.backgroundColor = UIColor.init(red: 229.0/255.0, green: 73.0/255.0, blue: 44.0/255.0, alpha: 0.1)
       let ai = UIActivityIndicatorView.init(style: .large)
       ai.color =  UIColor(red: 229.0/255.0, green: 73.0/255.0, blue: 44.0/255.0, alpha: 1.0)
    
       let image = UIImageView(image: UIImage(named: "Barber"))
       image.frame.size = CGSize(width: 60, height: 60)
       image.center = CGPoint(x: spinnerView.center.x, y: spinnerView.center.y - 60.0)
       ai.startAnimating()
       ai.center = spinnerView.center
       
       DispatchQueue.main.async {
           spinnerView.addSubview(ai)
           spinnerView.addSubview(image)
           onView.addSubview(spinnerView)
       }
       
       vSpinner = spinnerView
    }
    
    func removeSpinner() {
        DispatchQueue.main.async {
            self.vSpinner?.removeFromSuperview()
            self.vSpinner = nil
        }
    }

}
