//
//  NewCostumerModalTableViewCell.swift
//  MyFigaro
//
//  Created by Luca Palmese on 25/02/2020.
//  Copyright © 2020 fernando rosa. All rights reserved.
//

import UIKit

class NewCustomerModalTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textfield: UITextField!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
