//
//  NewAppointmentModalViewController.swift
//  MyFigaro
//
//  Created by Luca Palmese on 22/02/2020.
//  Copyright © 2020 fernando rosa. All rights reserved.
//

import Foundation
import Firebase

class NewAppointmentViewController: UIViewController, UITextFieldDelegate, CustomersTableViewControllerDelegate, ServicesSelectionDelegate, BarberSelectionDelegate, CalendarDelegate{
    
    var selectedDayAndTime:Date!
    func setDay(dayAndTime: Date) {
        print(dayAndTime)
        self.selectedDayAndTime = dayAndTime
        
        let calendar = Calendar.current
        let year = calendar.component(.year, from: dayAndTime)
        let month = calendar.component(.month, from: dayAndTime)
        let day = calendar.component(.day, from: dayAndTime)
        let hour = calendar.component(.hour, from: dayAndTime)
        let min = calendar.component(.minute, from: dayAndTime)
        let hourToDisplay = hour < 10 ? "0\(hour)" : "\(hour)"
        let minToDisplay = min < 10 ? "0\(min)" : "\(min)"
        self.refDayAndTime.text = "\(day)-\(month)-\(year) at \(hourToDisplay):\(minToDisplay)"
    }
    
    
    lazy var slideInTransitionDelegate = CustomInPresentationManager()
    
    func loadService(services: [Treatment]) {
        self.selectedServices = services
    }
    
    func loadBarber(barber: Barber) {
        self.selectedBarber = barber
    }
    
    
    var refCustomerName:UITextField!
    var refDayAndTime:UITextField!
    
    // Next BarButtonItem Outlet
    @IBOutlet weak var nextButton: UIBarButtonItem!
    
    // TableView Outlet
    @IBOutlet weak var tableView: UITableView!
    
    var selectedCutomer:Client!
    var customerName:String = ""
    
    var selectedServices:[Treatment]!
    var selectedBarber:Barber!
    
    var servicesCollection:UICollectionView!
    var barbersCollection:UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    func selected(selectedCustomer: Client) {
        print("\(selectedCustomer.surname + " " + selectedCustomer.name)")
        self.refCustomerName.text = "\(selectedCustomer.surname + " " + selectedCustomer.name)"
        self.customerName =  self.refCustomerName.text!
        self.selectedCutomer = selectedCustomer
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }
    
    func showErrorAlert(title:String, msg:String) {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { _ in
           self.navigationController?.popToRootViewController(animated: false)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    var vSpinner: UIView?
    
    func showSpinner(onView : UIView) {
        let spinnerView = UIView.init(frame: onView.frame)
        spinnerView.backgroundColor = UIColor.init(red: 229.0/255.0, green: 73.0/255.0, blue: 44.0/255.0, alpha: 0.1)
        let ai = UIActivityIndicatorView.init(style: .large)
        ai.color =  UIColor(red: 229.0/255.0, green: 73.0/255.0, blue: 44.0/255.0, alpha: 1.0)
     
        let image = UIImageView(image: UIImage(named: "Barber"))
        image.frame.size = CGSize(width: 60, height: 60)
        image.center = CGPoint(x: spinnerView.center.x, y: spinnerView.center.y - 60.0)
        ai.startAnimating()
        ai.center = spinnerView.center
        
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            spinnerView.addSubview(image)
            onView.addSubview(spinnerView)
        }
        
        vSpinner = spinnerView
    }
    
    func removeSpinner() {
        DispatchQueue.main.async {
            self.vSpinner?.removeFromSuperview()
            self.vSpinner = nil
        }
    }
    
}

extension NewAppointmentViewController: UITableViewDelegate, UITableViewDataSource {
    
   
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            guard let newAppointmentTableViewCell = tableView.dequeueReusableCell(withIdentifier: "newAppointmentTableViewCell", for: indexPath) as? NewAppointmentTableViewCell else {
                fatalError("Unable to create barber calendar table view cell")
            }
            newAppointmentTableViewCell.btnAddCustomerName.addTarget(self, action: #selector(openCustomersView(button:)), for: .touchUpInside)
            newAppointmentTableViewCell.customerName.delegate = self
            self.refCustomerName = newAppointmentTableViewCell.customerName
            return newAppointmentTableViewCell
        } else if indexPath.row == 1 {
            
            guard let servicesSelectionModalTableViewCell = tableView.dequeueReusableCell(withIdentifier: "servicesSelectionModalTableViewCell", for: indexPath) as? ServicesSelectionModalTableViewCell else {
                fatalError("Unable to create barber calendar table view cell")
            }
            servicesSelectionModalTableViewCell.treatmentsList = UserManager.shared.shop.services
            self.servicesCollection = servicesSelectionModalTableViewCell.collectionView2
            servicesSelectionModalTableViewCell.selectionDelegate = self
            return servicesSelectionModalTableViewCell
            
        } else if indexPath.row == 2 {
            guard let barberSelectionModalTableViewCell = tableView.dequeueReusableCell(withIdentifier: "barberSelectionModalTableViewCell", for: indexPath) as? BarberSelectionModalTableViewCell else {
                fatalError("Unable to create barber calendar table view cell")
            }
            barberSelectionModalTableViewCell.barbersList = UserManager.shared.shop.barbers
            self.barbersCollection = barberSelectionModalTableViewCell.collectionView
            barberSelectionModalTableViewCell.selectionDelegate = self
            return barberSelectionModalTableViewCell
        } else {
            let dayTableViewCell = tableView.dequeueReusableCell(withIdentifier: "dayAndTimeCell", for: indexPath) as! AppointmentDayTableViewCell
            dayTableViewCell.btnSet.addTarget(self, action: #selector(setDayAndTime(button:)), for: .touchUpInside)
            dayTableViewCell.btnConfirmAppointment.addTarget(self, action: #selector(confirmAppointment(button:)), for: .touchUpInside)
            self.refDayAndTime = dayTableViewCell.txtSelectDay
            return dayTableViewCell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
            case 0:
                return 120
            case 1:
                return 240
            case 2:
                return 240
            case 3:
                return 250
            default:
                fatalError()
        }
    }
    
    @objc func openCustomersView(button: UIButton){
       let storyBoard: UIStoryboard = UIStoryboard(name: "Barber", bundle: nil)
             let customersViewController = storyBoard.instantiateViewController(withIdentifier: "customersView") as! CustomersTableViewController
        customersViewController.modalPresentationStyle = .automatic
        customersViewController.delegate = self
        customersViewController.typeView = .select
             self.present(customersViewController, animated: true, completion: nil)
    }
    
    @objc func confirmAppointment(button: UIButton){
        
        let newAppointment:Appointment = Appointment()
        
        newAppointment.treatments = self.selectedServices
        newAppointment.barber = self.selectedBarber
        newAppointment.customer = self.selectedCutomer
        newAppointment.time = selectedDayAndTime
        newAppointment.duration = newAppointment.getDuration()
        
        self.showSpinner(onView: self.view)
        DataBaseManager.shared.addAppointment(idShop: UserManager.shared.shop.shopId, appointment: newAppointment) { msg in
            self.removeSpinner()
            if msg.contains("Error") {
                self.showErrorAlert(title: "New Appointment", msg: msg)
            }else{
                self.showErrorAlert(title: "New Appointment", msg: "The new appointment has been successfully added!")
            }
        }
    }
    
    @objc func setDayAndTime(button: UIButton){
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Barber", bundle: nil)
        let nvc = storyBoard.instantiateViewController(withIdentifier: "calendarView") as! BarberCalendarMonthViewController
        nvc.typeView = .appointment
        nvc.delegate = self
        nvc.delegate = self
        nvc.transitioningDelegate = self.slideInTransitionDelegate
        self.slideInTransitionDelegate.direction = .bottom
        nvc.modalPresentationStyle = .custom
        
        self.present(nvc, animated: true, completion: nil)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.view.endEditing(true)
    }
}
