//
//  NewAppointmentTableViewCell.swift
//  MyFigaro
//
//  Created by Luca Palmese on 24/02/2020.
//  Copyright © 2020 fernando rosa. All rights reserved.
//

import UIKit

class NewAppointmentTableViewCell: UITableViewCell {
    
    @IBOutlet weak var btnAddCustomerName: UIButton!
    
    // Customer's FullName TextField Outlet
    @IBOutlet weak var customerName: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.btnAddCustomerName.layer.cornerRadius = 5.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
