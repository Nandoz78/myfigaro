//
//  BarberCalendarMonthViewController.swift
//  MyFigaro
//
//  Created by Luca Palmese on 23/02/2020.
//  Copyright © 2020 fernando rosa. All rights reserved.
//

import UIKit

// Date Management
let dateFormatter5 = DateFormatter()

enum TypeCalendarView:Int{
    case standard
    case appointment
    case unknown
}

protocol CalendarDelegate {
    func setDay(dayAndTime:Date)
}

class BarberCalendarMonthViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    
    @IBOutlet weak var selectedTime: UITextField!
    @IBOutlet weak var rangeView: UIView!
    @IBOutlet weak var btnOk: UIButton!
    var selectedTimeIsVisible:Bool = false
    var pickerIsVisible:Bool = false
    var delegate:CalendarDelegate?
    var selectedTimeAndDay:Date!

    @IBAction func setDateAndTime(_ sender: Any) {
        print(self.selectedTimeAndDay)
        self.delegate?.setDay(dayAndTime: self.selectedTimeAndDay)
        self.dismiss(animated: true, completion: nil)
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        self.ranges.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.ranges[row].stringToDisplay
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        UIView.animate(withDuration: 0.2, animations: {
            self.timeRanges.frame.origin.y += (self.timeRanges.frame.height)
        })
        self.selectedTime.text = "\(month+1)/\(self.selectedDay)/\(year) at " + self.ranges[row].stringToDisplay
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = Locale.current
        self.selectedTimeAndDay = dateFormatter.date(from: "\(year)-\(month+1)-\(self.selectedDay)T\(self.ranges[row].stringToDisplay):00")
        print(self.selectedTimeAndDay)
        self.pickerIsVisible = false
    }
    
    
    @IBOutlet weak var timeRanges: UIPickerView!
    let ranges:[TimeRange] = [TimeRange(date: Date(), stringToDisplay: "09:00", barbers: UserManager.shared.shop.barbers),TimeRange(date: Date(), stringToDisplay: "09:15", barbers: UserManager.shared.shop.barbers),TimeRange(date: Date(), stringToDisplay: "09:30", barbers: UserManager.shared.shop.barbers)]
    
    let Months = ["January","February","March","April","May","June","July","August","September","October","November","December"]
    let DaysOfMonths = ["monday","tuesday","wednsday","thursday","friday","saturday","sunday"]
    var DaysInMonths = [31,29,31,30,31,30,31,31,30,31,30,31]
    var currentMonth = String()
    var numberOfEmptyBox = Int()
    var nextNumberOfEmptyBox = Int()
    var previousNumberOfEmptyBox = 0
    var direction = 0
    var positionIndex = 0
    let actualCalendar = Calendar.current
    var dayCounter = 0
    var selectedDay = Int()
    
    // Calendar Outlets
    @IBOutlet weak var calendar: UICollectionView!
    @IBOutlet weak var monthsLabel: UILabel!
    
    var typeView:TypeCalendarView = .standard
    
    override func viewWillLayoutSubviews() {
        self.view.layer.cornerRadius = 20.0
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.layer.cornerRadius = 20.0
        self.timeRanges.frame.origin.y += self.timeRanges.frame.height
        self.timeRanges.dataSource = self
        self.timeRanges.delegate = self
        
        dateFormatter5.dateFormat = "MMM"
        dateFormatter5.dateStyle = .short
        
        currentMonth = Months[month]
        monthsLabel.text = "\(currentMonth) \(year)"
        if weekday == 0 {
            weekday = 7
        }
        getStartDateDayPosition()
        
        if(self.typeView == .appointment)
        {
            self.rangeView.isHidden = false
            self.rangeView.layer.cornerRadius = 5.0
            self.btnOk.layer.cornerRadius = 5.0
        }else{
            self.rangeView.isHidden = true
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = false
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        // Manage comeback to Calendar
    }
    
    func getStartDateDayPosition() {
        switch direction {
            
            case 0:
                numberOfEmptyBox = weekday
                dayCounter = day
                while dayCounter>0 {
                    numberOfEmptyBox = numberOfEmptyBox - 1
                    dayCounter = dayCounter - 1
                    if numberOfEmptyBox == 0 {
                        numberOfEmptyBox = 7
                    }
                }
                if numberOfEmptyBox == 7 {
                    numberOfEmptyBox = 0
                }
                positionIndex = numberOfEmptyBox
            
            case 1...:
                nextNumberOfEmptyBox = ( positionIndex + DaysInMonths[month] ) % 7
                positionIndex = nextNumberOfEmptyBox
            
            case -1:
                previousNumberOfEmptyBox = (7 - (DaysInMonths[month] - positionIndex) % 7 )
                if previousNumberOfEmptyBox == 7 {
                    previousNumberOfEmptyBox = 0
                }
                positionIndex = previousNumberOfEmptyBox
        
            default: fatalError()
        }
    }
    
    @IBAction func next(_ sender: Any) {
        switch currentMonth {
        case "December":
            month = 0
            year += 1
            direction = 1
            if year % 4 == 0 {
                DaysInMonths[1] = 29
            } else {
                DaysInMonths[1] = 28
            }
            getStartDateDayPosition()
            currentMonth = Months[month]
            monthsLabel.text = "\(currentMonth) \(year)"
            calendar.reloadData()
            
        default:
            direction = 1
            getStartDateDayPosition()
            month += 1
            currentMonth = Months[month]
            monthsLabel.text = "\(currentMonth) \(year)"
            calendar.reloadData()
        }
    }
    
    @IBAction func back(_ sender: Any) {
        switch currentMonth {
        case "January":
            direction = -1
            month = 11
            year -= 1
            if year % 4 == 0 {
                DaysInMonths[1] = 29
            } else {
                DaysInMonths[1] = 28
            }
            getStartDateDayPosition()
            currentMonth = Months[month]
            monthsLabel.text = "\(currentMonth) \(year)"
            calendar.reloadData()
            
        default:
            direction = -1
            month -= 1
            getStartDateDayPosition()
            currentMonth = Months[month]
            monthsLabel.text = "\(currentMonth) \(year)"
            calendar.reloadData()
        }
    }
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    // Get the new view controller using segue.destination.
    // Pass the selected object to the new view controller.
    }
    */
    
}


// BarbershopTableViewCell2 extension to support the CollectionView
extension BarberCalendarMonthViewController: UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch direction {
            case 0:
                return DaysInMonths[month] + numberOfEmptyBox
            case 1...:
                return DaysInMonths[month] + nextNumberOfEmptyBox
            case -1:
                return DaysInMonths[month] + previousNumberOfEmptyBox
            default:
                fatalError()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let barberCalendarMonthCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "barberCalendarMonthCollectionViewCell", for: indexPath) as? BarberCalendarMonthCollectionViewCell else {fatalError("Unable to create a CollectionView cell")}
        
        barberCalendarMonthCollectionViewCell.backgroundColor = UIColor.clear
        barberCalendarMonthCollectionViewCell.dateLabel.textColor = UIColor.black
        barberCalendarMonthCollectionViewCell.circle.isHidden = true
        
        if barberCalendarMonthCollectionViewCell.isHidden {
            barberCalendarMonthCollectionViewCell.isHidden = false
        }
        switch direction {
            case 0:
                barberCalendarMonthCollectionViewCell.dateLabel.text = "\(indexPath.row + 1 - numberOfEmptyBox)"
            case 1:
                barberCalendarMonthCollectionViewCell.dateLabel.text = "\(indexPath.row + 1 - nextNumberOfEmptyBox)"
            case -1:
                barberCalendarMonthCollectionViewCell.dateLabel.text = "\(indexPath.row + 1 - previousNumberOfEmptyBox)"
            default:
                fatalError()
        }
        if Int(barberCalendarMonthCollectionViewCell.dateLabel.text!)! < 1 {
            barberCalendarMonthCollectionViewCell.isHidden = true
        }
        switch indexPath.row {
            case 5,6,12,13,19,20,26,27,33,34:
                if Int(barberCalendarMonthCollectionViewCell.dateLabel.text!)! > 0 {
                    barberCalendarMonthCollectionViewCell.dateLabel.textColor = UIColor.lightGray
                }
            default:
                break
            }
        if currentMonth == Months[actualCalendar.component(.month, from: date) - 1] && year == actualCalendar.component(.year, from: date) && indexPath.row + 1 - numberOfEmptyBox == day {
            barberCalendarMonthCollectionViewCell.circle.isHidden = false
            barberCalendarMonthCollectionViewCell.drawCircle()
        }
        
        return barberCalendarMonthCollectionViewCell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        switch direction {
            case 0:
                selectedDay = indexPath.row + 1 - numberOfEmptyBox
            case 1:
                selectedDay = indexPath.row + 1 - nextNumberOfEmptyBox
            case -1:
                selectedDay = indexPath.row + 1 - previousNumberOfEmptyBox
            default:
                fatalError()
        }
        
        self.selectedItem()
    }
    
    /// Function to evoke segue, called by the BarberCalendarMonthCellSelectionDelegate protocol in BarberCalendarMonthViewController class
    func selectedItem() {
        if(self.typeView == .standard){
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            dateFormatter.timeZone = TimeZone.current
            dateFormatter.locale = Locale.current
            print(dateFormatter.date(from: "\(year)-\(month+1)-\(self.selectedDay)T00:00:00"))
            self.delegate?.setDay(dayAndTime: dateFormatter.date(from: "\(year)-\(month+1)-\(self.selectedDay)T00:00:00")!)
            self.dismiss(animated: true, completion: nil)
        } else {
            print("\(month+1)/\(self.selectedDay)/\(year)")
            if(!self.pickerIsVisible){
                UIView.animate(withDuration: 0.2, animations: {
                    self.timeRanges.frame.origin.y -= (self.timeRanges.frame.height)
                })
                self.pickerIsVisible = true
            }
            if(!self.selectedTimeIsVisible){
                UIView.animate(withDuration: 0.2, animations: {
                    self.rangeView.frame.origin.y += (self.rangeView.frame.height + 10.0)
                })
                self.selectedTimeIsVisible = true
            }
        }
        
       
    }
    
}
