//
//  BarberCalendarViewController.swift
//  MyFigaro
//
//  Created by Luca Palmese on 18/02/2020.
//  Copyright © 2020 fernando rosa. All rights reserved.
//

import UIKit
import Firebase

// Date Management
let dateFormatter = DateFormatter()
let dateFormatter2 = DateFormatter()
let dateFormatter3 = DateFormatter()
let dateFormatter4 = DateFormatter()

class BarberCalendarViewController: UIViewController, CalendarDelegate {
    
    func setDay(dayAndTime: Date) {
        date = dayAndTime
        self.showSpinner(onView: self.view)
        DataBaseManager.shared.getShopAppointments(shopId: UserManager.shared.shop.shopId, dateRef: date, completion: { list in
            self.removeSpinner()
                   self.appointmentsList = list
                   self.tableView.reloadData()
                   
               })
    }
    
    
    lazy var slideInTransitionDelegate = CustomInPresentationManager()
    
    // To help put every appointment into its own slot
    var duration = 0
    var appointment = Appointment()
    
    // NavigationBar's Left BarButtonItem
    var leftButtonView = UIView()
    var leftButton = UIButton.init(type: .system)
    var leftBarButton = UIBarButtonItem()
    
    var appointmentsList:[Appointment] = [Appointment]()
    
    // TableView Outlet
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        dateFormatter.dateFormat = "E, d MMM y"
        dateFormatter2.dateFormat = "HH:mm"
        dateFormatter3.dateFormat = "MMM"
        dateFormatter3.dateStyle = .medium
        dateFormatter4.dateStyle = .short

        self.navigationItem.title = UserManager.shared.shop.shopName
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        leftButtonView = UIView.init(frame: CGRect(x: -16, y: 0, width: 50, height: 50))
        leftButton.backgroundColor = .clear
        leftButton.frame = leftButtonView.frame
        leftButton.setImage(UIImage(systemName: "calendar"), for: .normal)
        leftButton.autoresizesSubviews = true
        leftButton.autoresizingMask = [.flexibleWidth , .flexibleHeight]
        leftButton.addTarget(self, action: #selector(calendarButtonTapped), for: .touchUpInside)
        leftButtonView.addSubview(leftButton)

        self.leftBarButton = UIBarButtonItem.init(customView: leftButtonView)
        navigationItem.leftBarButtonItem = leftBarButton
        
        // Always display NavigationBar
        self.navigationController?.navigationBar.isHidden = false
        
        print(dateFormatter.date(from: ResourcesManager.shared.selectedDay) ?? date)
       
        DataBaseManager.shared.getShopAppointments(shopId: UserManager.shared.shop.shopId, dateRef: date, completion: { list in
            
            self.appointmentsList = list
            self.tableView.reloadData()
            
        })
        
        
    }
    

    @IBAction func addNewAppointmentButtonTapped(_ sender: UIBarButtonItem) {
    }
    
    @objc func calendarButtonTapped() {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Barber", bundle: nil)
        let nvc = storyBoard.instantiateViewController(withIdentifier: "calendarView") as! BarberCalendarMonthViewController
        nvc.typeView = .standard
        nvc.transitioningDelegate = self.slideInTransitionDelegate
        self.slideInTransitionDelegate.direction = .bottom
        nvc.modalPresentationStyle = .custom
        nvc.delegate = self
        self.present(nvc, animated: true, completion: nil)
    
    }
    
    func showErrorAlert(title:String, msg:String) {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { _ in
        
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    var vSpinner: UIView?
    
    func showSpinner(onView : UIView) {
        let spinnerView = UIView.init(frame: onView.frame)
        spinnerView.backgroundColor = UIColor.init(red: 229.0/255.0, green: 73.0/255.0, blue: 44.0/255.0, alpha: 0.1)
        let ai = UIActivityIndicatorView.init(style: .large)
        ai.color =  UIColor(red: 229.0/255.0, green: 73.0/255.0, blue: 44.0/255.0, alpha: 1.0)
     
        let image = UIImageView(image: UIImage(named: "Barber"))
        image.frame.size = CGSize(width: 60, height: 60)
        image.center = CGPoint(x: spinnerView.center.x, y: spinnerView.center.y - 60.0)
        ai.startAnimating()
        ai.center = spinnerView.center
        
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            spinnerView.addSubview(image)
            onView.addSubview(spinnerView)
        }
        
        vSpinner = spinnerView
    }
    
    func removeSpinner() {
        DispatchQueue.main.async {
            self.vSpinner?.removeFromSuperview()
            self.vSpinner = nil
        }
    }

}

// Extension to manage the TableView
extension BarberCalendarViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return dateFormatter.string(from: date)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.appointmentsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let barberCalendarTableViewCell = tableView.dequeueReusableCell(withIdentifier: "barberCalendarTableViewCell", for: indexPath) as? BarberCalendarTableViewCell else {
            fatalError("Unable to create barber calendar table view cell")
        }
        barberCalendarTableViewCell.timeSlot.text = self.appointmentsList[indexPath.row].getTime()
        barberCalendarTableViewCell.scheduledCustomer.text = self.appointmentsList[indexPath.row].customer.name + " " + self.appointmentsList[indexPath.row].customer.surname
        barberCalendarTableViewCell.chosenBarber.adjustsFontSizeToFitWidth = true
        barberCalendarTableViewCell.chosenBarber.minimumScaleFactor = 0.5
        barberCalendarTableViewCell.chosenBarber.text = self.appointmentsList[indexPath.row].barber.name
        var treatmentsList: String = ""
        for item in self.appointmentsList[indexPath.row].treatments {
            treatmentsList.append(" \(item.name),")
        }
        if !treatmentsList.isEmpty {
            treatmentsList.removeLast()
        }
        barberCalendarTableViewCell.services.adjustsFontSizeToFitWidth = true
        barberCalendarTableViewCell.services.minimumScaleFactor = 0.5
        barberCalendarTableViewCell.services.text = treatmentsList
        
       
        return barberCalendarTableViewCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let check = checkAction(tableView, at: indexPath)
        let edit = editAction(at: indexPath)
        let delete = deleteAction(tableView, at: indexPath)
        if appointmentsList[indexPath.row].status != .unknown {
            let swipeAction = UISwipeActionsConfiguration(actions: [delete, edit, check])
            swipeAction.performsFirstActionWithFullSwipe = false
            return swipeAction
        } else {
            let swipeAction = UISwipeActionsConfiguration(actions: [])
            swipeAction.performsFirstActionWithFullSwipe = false
            return swipeAction
        }
    }
    
    func checkAction(_ tableView: UITableView, at indexPath: IndexPath) -> UIContextualAction {
        let cell = tableView.cellForRow(at: indexPath) as? BarberCalendarTableViewCell
        let action = UIContextualAction(style: .normal, title: "Check") {
            (action, view, completion) in
            if self.appointmentsList[indexPath.row].status == .pending {
                self.appointmentsList[indexPath.row].status = .done
                cell?.backgroundColor = #colorLiteral(red: 1, green: 0.9254901961, blue: 0.6039215686, alpha: 1)
            } else if self.appointmentsList[indexPath.row].status == .done {
                self.appointmentsList[indexPath.row].status = .pending
                cell?.backgroundColor = .systemBackground
            }
            completion(true)
        }
        if appointmentsList[indexPath.row].status == .done {
            action.image = UIImage(systemName: "xmark")
            action.backgroundColor = #colorLiteral(red: 0.8980392157, green: 0.2862745098, blue: 0.1725490196, alpha: 1)
        } else if appointmentsList[indexPath.row].status == .pending {
            action.image = UIImage(systemName: "checkmark")
            action.backgroundColor = #colorLiteral(red: 0.2509803922, green: 0.431372549, blue: 0.7647058824, alpha: 1)
        }
        
        return action
    }
    
    func editAction(at indexPath: IndexPath) -> UIContextualAction {
        if self.appointmentsList[indexPath.row].status == .unknown {
            let action = UIContextualAction(style: .normal, title: "Add") {
                (action, view, completion) in
                self.performSegue(withIdentifier: "appointmentEditingSegue", sender: self)
                completion(true)
            }
            action.backgroundColor = .gray
            
            return action
        } else {
            let action = UIContextualAction(style: .normal, title: "Edit") {
                (action, view, completion) in
                self.performSegue(withIdentifier: "appointmentEditingSegue", sender: self)
                completion(true)
            }
            action.backgroundColor = .gray
            
            return action
        }
    }
    
    func deleteAction(_ tableView: UITableView, at indexPath: IndexPath) -> UIContextualAction {
        
        let action = UIContextualAction(style: .destructive, title: "Delete") {
             (action, view, completion) in
            
            self.showSpinner(onView: self.view)
            self.tableView.beginUpdates()
            
            DataBaseManager.shared.deleteAppointment(appointment: self.appointmentsList[indexPath.row], completion: { msg in
                self.removeSpinner()
                self.appointmentsList.remove(at: indexPath.row)
                self.tableView.deleteRows(at: [indexPath], with: .automatic)
                self.tableView.endUpdates()
            })
        }
        action.image = UIImage(systemName: "trash")
        action.backgroundColor = .systemRed
        
        return action
    }
    
}
