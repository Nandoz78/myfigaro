//
//  BarberCalendarTableViewCell.swift
//  MyFigaro
//
//  Created by Luca Palmese on 18/02/2020.
//  Copyright © 2020 fernando rosa. All rights reserved.
//

import UIKit

class BarberCalendarTableViewCell: UITableViewCell {
    
    @IBOutlet weak var timeSlot: UILabel!
    @IBOutlet weak var chosenBarber: UILabel!
    @IBOutlet weak var scheduledCustomer: UILabel!
    @IBOutlet weak var services: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
