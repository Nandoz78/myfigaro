//
//  AppointmentEditingModalTableViewCell3.swift
//  MyFigaro
//
//  Created by Luca Palmese on 22/02/2020.
//  Copyright © 2020 fernando rosa. All rights reserved.
//

import UIKit

protocol BarberSelectionDelegate {
    func loadBarber(barber:Barber)
}

class BarberSelectionModalTableViewCell: UITableViewCell {
    
    var barbersList:[Barber] = [Barber]()
    var selectedBarber:Barber!
    var selectionDelegate:BarberSelectionDelegate?
    
    // CollectionView Outlet
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        collectionView.delegate = self
        collectionView.dataSource = self
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}


// BarberSelectionModalTableViewCell extension to support the CollectionView
extension BarberSelectionModalTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return barbersList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
                
        guard let barberSelectionModalCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "barberSelectionModalCollectionViewCell", for: indexPath) as? BarberSelectionModalCollectionViewCell else {fatalError("Unable to create a CollectionView cell")}
        
        barberSelectionModalCollectionViewCell.barberImage.clipsToBounds = false
        barberSelectionModalCollectionViewCell.barberImage.layer.cornerRadius = 25
        barberSelectionModalCollectionViewCell.barberImage.layer.shadowColor = UIColor.gray.cgColor
        barberSelectionModalCollectionViewCell.barberImage.layer.shadowOpacity = 0.5
        barberSelectionModalCollectionViewCell.barberImage.layer.shadowOffset = CGSize(width: 0, height: 3.5)
        barberSelectionModalCollectionViewCell.barberImage.layer.shadowRadius = 2.5
        barberSelectionModalCollectionViewCell.barberImage.image = UIImage(named: "Barber")
        barberSelectionModalCollectionViewCell.barberName.adjustsFontSizeToFitWidth = true
        barberSelectionModalCollectionViewCell.barberName.minimumScaleFactor = 0.5
        barberSelectionModalCollectionViewCell.barberName.text = barbersList[indexPath.row].name
        
        if(self.selectedBarber != nil && self.barbersList[indexPath.row].id == self.selectedBarber!.id){
            barberSelectionModalCollectionViewCell.setSelected()
        }else{
            barberSelectionModalCollectionViewCell.setUnselected()
        }
        
        return barberSelectionModalCollectionViewCell
    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        return CGSize(width: 120, height: 159)
//    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if(self.selectedBarber != nil){
            if(self.selectedBarber.id != self.barbersList[indexPath.row].id){
                self.selectedBarber = self.barbersList[indexPath.row]
            }else{
                self.selectedBarber = Barber()
            }
        }else{
            self.selectedBarber = self.barbersList[indexPath.row]
        }
        
        self.selectionDelegate?.loadBarber(barber: self.selectedBarber)
        self.collectionView.reloadData()
    }
    
}
