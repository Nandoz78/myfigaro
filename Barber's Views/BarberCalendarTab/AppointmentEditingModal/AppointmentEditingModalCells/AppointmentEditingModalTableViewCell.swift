//
//  AppointmentEditingModalTableViewCell.swift
//  MyFigaro
//
//  Created by Luca Palmese on 22/02/2020.
//  Copyright © 2020 fernando rosa. All rights reserved.
//

import UIKit

class AppointmentEditingModalTableViewCell: UITableViewCell {
    
    // Customer's Name Outlet
    @IBOutlet weak var customerName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
