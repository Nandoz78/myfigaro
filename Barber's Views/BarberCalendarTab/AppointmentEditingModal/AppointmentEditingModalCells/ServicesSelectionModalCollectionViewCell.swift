//
//  ServicesSelectionCollectionViewCell.swift
//  MyFigaro
//
//  Created by Luca Palmese on 22/02/2020.
//  Copyright © 2020 fernando rosa. All rights reserved.
//

import UIKit

class ServicesSelectionModalCollectionViewCell: UICollectionViewCell {
    
    // Service Image
    @IBOutlet weak var serviceImage: UIImageView!
    
    // Service Name Label
    @IBOutlet weak var serviceName: UILabel!
    
    // Service duration Label
    @IBOutlet weak var serviceDuration: UILabel!
    
    //Service description string
    var serviceDescription:String?
    
    var serviceSelected:Bool = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
