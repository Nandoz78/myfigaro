//
//  AppointmentDayTableViewCell.swift
//  MyFigaro
//
//  Created by fernando rosa on 10/03/2020.
//  Copyright © 2020 fernando rosa. All rights reserved.
//

import UIKit

class AppointmentDayTableViewCell: UITableViewCell {

    @IBOutlet weak var lblSelectDay:UILabel!
    @IBOutlet weak var txtSelectDay:UITextField!
    @IBOutlet weak var dayView:UIView!
    @IBOutlet weak var btnSet:UIButton!
    @IBOutlet weak var btnConfirmAppointment:UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.dayView.layer.cornerRadius = 5.0
        self.btnSet.layer.cornerRadius = 5.0
        self.btnConfirmAppointment.layer.cornerRadius = 5.0
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
