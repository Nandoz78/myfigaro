//
//  AppointmentEditingModalTableViewCell4.swift
//  MyFigaro
//
//  Created by Luca Palmese on 22/02/2020.
//  Copyright © 2020 fernando rosa. All rights reserved.
//

import UIKit

protocol ServicesSelectionDelegate {
    func loadService(services:[Treatment])
}

class ServicesSelectionModalTableViewCell: UITableViewCell {

    var treatmentsList:[Treatment] = [Treatment]()
    var selectedtreatments:[Treatment] = [Treatment]()
    var selectionDelegate:ServicesSelectionDelegate?
    
    // CollectionView Outlet
    @IBOutlet weak var collectionView2: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        collectionView2.delegate = self
        collectionView2.dataSource = self
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

// ServicesSelectionModalTableViewCell extension to support the CollectionView
extension ServicesSelectionModalTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return treatmentsList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
                
        guard let servicesSelectionModalCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "servicesSelectionModalCollectionViewCell", for: indexPath) as? ServicesSelectionModalCollectionViewCell else {fatalError("Unable to create a CollectionView cell")}
        
        servicesSelectionModalCollectionViewCell.serviceImage.clipsToBounds = false
        servicesSelectionModalCollectionViewCell.serviceImage.layer.cornerRadius = 25
        servicesSelectionModalCollectionViewCell.serviceImage.layer.shadowColor = UIColor.gray.cgColor
        servicesSelectionModalCollectionViewCell.serviceImage.layer.shadowOpacity = 0.5
        servicesSelectionModalCollectionViewCell.serviceImage.layer.shadowOffset = CGSize(width: 0, height: 3.5)
        servicesSelectionModalCollectionViewCell.serviceImage.layer.shadowRadius = 2.5
        servicesSelectionModalCollectionViewCell.serviceImage.image = UIImage(named: treatmentsList[indexPath.row].image)
        servicesSelectionModalCollectionViewCell.serviceName.adjustsFontSizeToFitWidth = true
        servicesSelectionModalCollectionViewCell.serviceName.minimumScaleFactor = 0.5
        servicesSelectionModalCollectionViewCell.serviceName.text = treatmentsList[indexPath.row].name
        servicesSelectionModalCollectionViewCell.serviceDuration.adjustsFontSizeToFitWidth = true
        servicesSelectionModalCollectionViewCell.serviceDuration.minimumScaleFactor = 0.5
        servicesSelectionModalCollectionViewCell.serviceDuration.text = "\(treatmentsList[indexPath.row].duration) min"
        
        if(self.selectedtreatments.filter({$0.id == treatmentsList[indexPath.row].id}).count > 0){
            servicesSelectionModalCollectionViewCell.serviceImage.alpha = 1.0
        }else {
            servicesSelectionModalCollectionViewCell.serviceImage.alpha = 0.5
        }
        
        return servicesSelectionModalCollectionViewCell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if(self.selectedtreatments.filter({$0.id == treatmentsList[indexPath.row].id}).count > 0){
            self.selectedtreatments = self.selectedtreatments.filter({$0.id != treatmentsList[indexPath.row].id})
        } else {
            self.selectedtreatments.append(self.treatmentsList.filter({$0.id == treatmentsList[indexPath.row].id})[0])
        }
        self.selectionDelegate?.loadService(services: self.selectedtreatments)
        self.collectionView2.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 120, height: 169)
    }
    
}
