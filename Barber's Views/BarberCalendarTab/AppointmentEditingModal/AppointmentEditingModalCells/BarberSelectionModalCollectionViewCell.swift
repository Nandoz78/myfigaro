//
//  BarberSelectionCollectionViewCell.swift
//  MyFigaro
//
//  Created by Luca Palmese on 22/02/2020.
//  Copyright © 2020 fernando rosa. All rights reserved.
//

import UIKit

class BarberSelectionModalCollectionViewCell: UICollectionViewCell {
    
    // Barber's Image
    @IBOutlet weak var barberImage: UIImageView!
    
    // Barber's Name Label
    @IBOutlet weak var barberName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setSelected(){
        self.barberImage.alpha = 1.0
        self.barberName.alpha = 1.0
    }
    
    func setUnselected(){
        self.barberImage.alpha = 0.5
        self.barberName.alpha = 0.5
    }
}
