//
//  AppointmentEditingModalViewController.swift
//  MyFigaro
//
//  Created by Luca Palmese on 19/02/2020.
//  Copyright © 2020 fernando rosa. All rights reserved.
//

import UIKit
import Firebase

class AppointmentEditingModalViewController: UIViewController, UITextFieldDelegate{
    
    // Outlets
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    // Cancel Button Action
    @IBAction func cancelButtonTapped(_ sender: UIButton) {
        // Raises the FLAG 'NewStudentModalIsDismissed to let the Observer notice and react
        dismiss(animated: true) {
            ///                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ViewProfileModalIsDismissed"), object: nil)
        }
    }
    
    // Edit Button Action
    @IBAction func saveButtonTapped(_ sender: UIButton) {
        dismiss(animated: true) {
            /// Save onto the Database
            ///                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ViewProfileModalIsDismissed"), object: nil)
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension AppointmentEditingModalViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            guard let appointmentEditingModalTableViewCell = tableView.dequeueReusableCell(withIdentifier: "appointmentEditingModalTableViewCell", for: indexPath) as? AppointmentEditingModalTableViewCell else {
                fatalError("Unable to create barber calendar table view cell")
            }
            
            appointmentEditingModalTableViewCell.customerName.text = "Topo Gigio"
            
            return appointmentEditingModalTableViewCell
        } else if indexPath.row == 1 {
            guard let appointmentEditingModalTableViewCell2 = tableView.dequeueReusableCell(withIdentifier: "appointmentEditingModalTableViewCell2", for: indexPath) as? AppointmentEditingModalTableViewCell2 else {
                fatalError("Unable to create barber calendar table view cell")
            }
            
            appointmentEditingModalTableViewCell2.time.text = "12:00"
            
            return appointmentEditingModalTableViewCell2
        } else if indexPath.row == 2 {
            guard let barberSelectionModalTableViewCell = tableView.dequeueReusableCell(withIdentifier: "barberSelectionModalTableViewCell", for: indexPath) as? BarberSelectionModalTableViewCell else {
                fatalError("Unable to create barber calendar table view cell")
            }
            
            return barberSelectionModalTableViewCell
        } else {
            guard let servicesSelectionModalTableViewCell = tableView.dequeueReusableCell(withIdentifier: "servicesSelectionModalTableViewCell", for: indexPath) as? ServicesSelectionModalTableViewCell else {
                fatalError("Unable to create barber calendar table view cell")
            }
            
            return servicesSelectionModalTableViewCell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 2 {
            return 240
        } else if indexPath.row == 3 {
            return 250
        } else {
            return 63
        }
    }
    
}
